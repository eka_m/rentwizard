<?php

namespace App\Console\Commands;

use App\Jobs\ActivateDeal;
use App\Jobs\DeactivateDeal;

use App\Models\DealJob;

use Illuminate\Console\Command;


class CheckDealExpiration extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'deals:check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Checking deals expiration';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle ()
	{
		$jobs = DealJob::orderBy( 'start', 'ASC' )->with( 'website' )->get();
		if ( !$jobs->isEmpty() ) {
			foreach ( $jobs as $job ) {
				$this->activate( $job );
				$this->deactivate( $job );
			}
			$this->info( 'Success' );
			return;
		}
		$this->info( 'No deals' );
		return;
	}

	public function activate ( $job )
	{
		if ( $job->activated == 0 ) {
			ActivateDeal::dispatch( $job );
		}
	}

	public function deactivate ( $job )
	{
		if ( $job->activated == 1 ) {
			$this->info( date_default_timezone_get() . ' - ' . $job->deal_id );
			DeactivateDeal::dispatch( $job );
		}
	}

}
