<?php

namespace App\Console\Commands;

use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Illuminate\Console\Command;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;

class CreateTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:create';

    /**
     * The console command description.
     *
     * @var string
     */
	protected $description = 'Creates a tenant with the provided name and email address e.g. php artisan tenant:create easy user@example.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $website = new Website;
	    app( WebsiteRepository::class )->create( $website );

	    $hostname = new Hostname;
	    $hostname->fqdn = "popa";
	    $hostname = app( HostnameRepository::class )->create( $hostname );
	    app( HostnameRepository::class )->attach( $hostname, $website );
	    return $hostname;
    }
}
