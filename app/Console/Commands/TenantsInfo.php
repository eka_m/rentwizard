<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\Deal;
use App\Models\Inventory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class TenantsInfo extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'get:info';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get information for tenants';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle ()
	{
		$website = website();

		if ( $website->id != 1 ) {

			$count_of_deals = Deal::count();
			$count_of_items = Inventory::count();
			$count_of_clients = Client::count();
			$info = json_decode( Cache::get( 'tenantsinfo', '{}' ), true );
			$info[ $website->pin ][ 'count_of_deals' ] = $count_of_deals;
			$info[ $website->pin ][ 'count_of_items' ] = $count_of_items;
			$info[ $website->pin ][ 'count_of_clients' ] = $count_of_clients;
			Cache::forget( "tenantsinfo" );
			Cache::forever( "tenantsinfo", json_encode( $info ) );
			$this->info( Cache::get( 'tenantsinfo' ) );
		}

	}
}
