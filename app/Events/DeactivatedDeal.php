<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DeactivatedDeal
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $deal;
    public $website;

    public function __construct($deal, $website)
    {
        $this->deal = $deal;
        $this->website = $website;
    }

}
