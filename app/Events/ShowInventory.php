<?php

namespace App\Events;

use App\Models\Inventory;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ShowInventory implements ShouldBroadcast
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $inventory;

	/**
	 * Create a new event instance.
	 *
	 * @param Inventory $inventory
	 */
	public function __construct ( $inventory )
	{
		$this->inventory = $inventory;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn ()
	{
		$user = auth( 'employee' )->user()->id;
		$tenant = website()->id;
		return new PrivateChannel( "inventory.$user.$tenant" );
	}

	public function broadcastWith ()
	{
		return [ $this->inventory ];
	}
}
