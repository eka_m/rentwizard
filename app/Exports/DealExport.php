<?php

namespace App\Exports;

use App\Models\Deal;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DealExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{

	use Exportable;

	public function query ()
	{
		return Deal::query()->with( "client" )->with( 'items' )->with( "manager" );
	}

	public function map ( $item ): array
	{
		$items = "";
		foreach ( $item->items as $inv ) {
			$items .= " (#" . $inv->id . " " . $inv->name . "), ";
		}
		$items = rtrim( $items, ", " );
		$map = [
			$item->id,
			$item->hash,
			"#" . $item->client->id . ' ' . $item->client->name,
			$items,
			$item->created_at,
			$item->start,
			$item->end,
			$item->paid_at,
			$item->realprice,
			$item->sale . " %",
			$item->price,
			__( $item->status ),
			htmlspecialchars(html_entity_decode($item->description)),
			$item->manager->name,
		];

		$profit = $item->manager_profit ? $item->manager_profit : 0;

		$type = "";
		switch ( $item->manager_profit_type ) {
			case "fix":
				$type .= " " . __( 'Amount from the deal' );
				break;
			case "fixfromdeal":
				$type .= " " . __( 'Additional income' );
				break;
			default:
				$type .= "% " . __( 'Percentage from the deal' );
		}

		$map[] = $profit . " " . $type;

		return $map;
	}

	public function headings (): array
	{
		return [
			'#',
			__( "Name" ),
			__( "Customer" ),
			__( "Items" ),
			__( "Date of adding" ),
			__( "Start date" ),
			__( "End date" ),
			__( "Payment date" ),
			__( "Price" ),
			__( "Sale" ),
			__( "Final price" ),
			__( "Status" ),
			__( "Description" ),
			__( "Manager" ),
			__( "Manager profit" ),
		];
	}

}
