<?php

namespace App\Exports;

use App\Models\Attribute;
use App\Models\Inventory;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class InventoryExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{

	use Exportable;

	protected $attributes;

	public function __construct ()
	{
		$this->attributes = Attribute::all();
	}

	public function query ()
	{
		return Inventory::query();
	}

	public function map ( $item ): array
	{
		$map = [
			$item->id,
			$item->name,
			$item->created_at,
			$item->purchase_date,
			$item->purchase_price
		];

		foreach ( $this->attributes as $attribute ) {
			$map[] = $item->attributes[ $attribute->slug ];
		}

		$map[] = __( $item->state );
		$map[] = $item->rent_price . "/" . __( $item->rent_per );


		return $map;
	}

	public function headings (): array
	{
		$headings = [
			'#',
			__( "Name" ),
			__( "Date of adding" ),
			__( "Purchase date" ),
			__( "Purchase price" ),
		];

		foreach ( $this->attributes as $attribute ) {
			$headings[] = $attribute->name;
		}

		$headings[] = __( "Сondition" );
		$headings[] = __( "Rent price" );

		return $headings;
	}

}
