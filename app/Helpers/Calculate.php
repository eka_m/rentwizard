<?php

namespace App\Helpers;

use Carbon\Carbon;

class Calculate
{
	public static function managerProfit ( $deals )
	{
		foreach ( $deals as $key => $item ) {
			if ( $item[ 'manager_profit_type' ] == "percent" ) {
				$deals[$key][ 'manager_profit' ] = round( $item[ 'price' ] * $item[ 'manager_profit' ] / 100, 1 );
			}
		}
		return $deals;
	}

	public static function groupByMonth ( $data, $field )
	{
		return $data->groupBy( function ( $q ) use ( $field ) {
			return Carbon::parse( $q->{$field} )->format( "n" );
		} );
	}

	public static function calculateSums ( $data, $field, $precision = 2 )
	{
		return $data->map( function ( $item ) use ( $field, $precision ) {
			return round( $item->sum( $field ), 2 );
		} );
	}

	public static function createYearData ( $data )
	{
		$result = collect( [ 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0 ] );
		foreach ( $data as $key => $value ) {
			$result->put( $key, $value );
		}
		return $result;
	}
}