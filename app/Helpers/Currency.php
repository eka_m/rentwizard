<?php
/**
 * Created by PhpStorm.
 * User: eka
 * Date: 2019-03-11
 * Time: 16:04
 */

namespace App\Helpers;


class Currency
{
	private static $currencies =
		[
			[
				"currency" => "Albania Lek",
				"abbreviation" => "ALL",
				"symbol" => "&#76;&#101;&#107;"
			],
			[
				"currency" => "Afghanistan Afghani",
				"abbreviation" => "AFN",
				"symbol" => "&#1547;"
			],
			[
				"currency" => "Argentina Peso",
				"abbreviation" => "ARS",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Aruba Guilder",
				"abbreviation" => "AWG",
				"symbol" => "&#402;"
			],
			[
				"currency" => "Australia Dollar",
				"abbreviation" => "AUD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Azerbaijan New Manat",
				"abbreviation" => "AZN",
				"symbol" => "&#8380"
			],
			[
				"currency" => "Bahamas Dollar",
				"abbreviation" => "BSD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Barbados Dollar",
				"abbreviation" => "BBD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Belarus Ruble",
				"abbreviation" => "BYR",
				"symbol" => "&#112;&#46;"
			],
			[
				"currency" => "Belize Dollar",
				"abbreviation" => "BZD",
				"symbol" => "&#66;&#90;&#36;"
			],
			[
				"currency" => "Bermuda Dollar",
				"abbreviation" => "BMD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Bolivia Boliviano",
				"abbreviation" => "BOB",
				"symbol" => "&#36;&#98;"
			],
			[
				"currency" => "Bosnia and Herzegovina Convertible Marka",
				"abbreviation" => "BAM",
				"symbol" => "&#75;&#77;"
			],
			[
				"currency" => "Botswana Pula",
				"abbreviation" => "BWP",
				"symbol" => "&#80;"
			],
			[
				"currency" => "Bulgaria Lev",
				"abbreviation" => "BGN",
				"symbol" => "&#1083;&#1074;"
			],
			[
				"currency" => "Brazil Real",
				"abbreviation" => "BRL",
				"symbol" => "&#82;&#36;"
			],
			[
				"currency" => "Brunei Darussalam Dollar",
				"abbreviation" => "BND",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Cambodia Riel",
				"abbreviation" => "KHR",
				"symbol" => "&#6107;"
			],
			[
				"currency" => "Canada Dollar",
				"abbreviation" => "CAD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Cayman Islands Dollar",
				"abbreviation" => "KYD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Chile Peso",
				"abbreviation" => "CLP",
				"symbol" => "&#36;"
			],
			[
				"currency" => "China Yuan Renminbi",
				"abbreviation" => "CNY",
				"symbol" => "&#165;"
			],
			[
				"currency" => "Colombia Peso",
				"abbreviation" => "COP",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Costa Rica Colon",
				"abbreviation" => "CRC",
				"symbol" => "&#8353;"
			],
			[
				"currency" => "Croatia Kuna",
				"abbreviation" => "HRK",
				"symbol" => "&#107;&#110;"
			],
			[
				"currency" => "Cuba Peso",
				"abbreviation" => "CUP",
				"symbol" => "&#8369;"
			],
			[
				"currency" => "Czech Republic Koruna",
				"abbreviation" => "CZK",
				"symbol" => "&#75;&#269;"
			],
			[
				"currency" => "Denmark Krone",
				"abbreviation" => "DKK",
				"symbol" => "&#107;&#114;"
			],
			[
				"currency" => "Dominican Republic Peso",
				"abbreviation" => "DOP",
				"symbol" => "&#82;&#68;&#36;"
			],
			[
				"currency" => "East Caribbean Dollar",
				"abbreviation" => "XCD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Egypt Pound",
				"abbreviation" => "EGP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "El Salvador Colon",
				"abbreviation" => "SVC",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Estonia Kroon",
				"abbreviation" => "EEK",
				"symbol" => "&#107;&#114;"
			],
			[
				"currency" => "Euro Member Countries",
				"abbreviation" => "EUR",
				"symbol" => "&#8364;"
			],
			[
				"currency" => "Falkland Islands (Malvinas] Pound",
				"abbreviation" => "FKP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Fiji Dollar",
				"abbreviation" => "FJD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Ghana Cedis",
				"abbreviation" => "GHC",
				"symbol" => "&#162;"
			],
			[
				"currency" => "Gibraltar Pound",
				"abbreviation" => "GIP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Guatemala Quetzal",
				"abbreviation" => "GTQ",
				"symbol" => "&#81;"
			],
			[
				"currency" => "Guernsey Pound",
				"abbreviation" => "GGP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Guyana Dollar",
				"abbreviation" => "GYD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Honduras Lempira",
				"abbreviation" => "HNL",
				"symbol" => "&#76;"
			],
			[
				"currency" => "Hong Kong Dollar",
				"abbreviation" => "HKD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Hungary Forint",
				"abbreviation" => "HUF",
				"symbol" => "&#70;&#116;"
			],
			[
				"currency" => "Iceland Krona",
				"abbreviation" => "ISK",
				"symbol" => "&#107;&#114;"
			],
			[
				"currency" => "India Rupee",
				"abbreviation" => "INR",
				"symbol" => null
			],
			[
				"currency" => "Indonesia Rupiah",
				"abbreviation" => "IDR",
				"symbol" => "&#82;&#112;"
			],
			[
				"currency" => "Iran Rial",
				"abbreviation" => "IRR",
				"symbol" => "&#65020;"
			],
			[
				"currency" => "Isle of Man Pound",
				"abbreviation" => "IMP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Israel Shekel",
				"abbreviation" => "ILS",
				"symbol" => "&#8362;"
			],
			[
				"currency" => "Jamaica Dollar",
				"abbreviation" => "JMD",
				"symbol" => "&#74;&#36;"
			],
			[
				"currency" => "Japan Yen",
				"abbreviation" => "JPY",
				"symbol" => "&#165;"
			],
			[
				"currency" => "Jersey Pound",
				"abbreviation" => "JEP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Kazakhstan Tenge",
				"abbreviation" => "KZT",
				"symbol" => "&#8376;"
			],
			[
				"currency" => "Korea (North] Won",
				"abbreviation" => "KPW",
				"symbol" => "&#8361;"
			],
			[
				"currency" => "Korea (South] Won",
				"abbreviation" => "KRW",
				"symbol" => "&#8361;"
			],
			[
				"currency" => "Kyrgyzstan Som",
				"abbreviation" => "KGS",
				"symbol" => "&#1083;&#1074;"
			],
			[
				"currency" => "Laos Kip",
				"abbreviation" => "LAK",
				"symbol" => "&#8365;"
			],
			[
				"currency" => "Latvia Lat",
				"abbreviation" => "LVL",
				"symbol" => "&#76;&#115;"
			],
			[
				"currency" => "Lebanon Pound",
				"abbreviation" => "LBP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Liberia Dollar",
				"abbreviation" => "LRD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Lithuania Litas",
				"abbreviation" => "LTL",
				"symbol" => "&#76;&#116;"
			],
			[
				"currency" => "Macedonia Denar",
				"abbreviation" => "MKD",
				"symbol" => "&#1076;&#1077;&#1085;"
			],
			[
				"currency" => "Malaysia Ringgit",
				"abbreviation" => "MYR",
				"symbol" => "&#82;&#77;"
			],
			[
				"currency" => "Mauritius Rupee",
				"abbreviation" => "MUR",
				"symbol" => "&#8360;"
			],
			[
				"currency" => "Mexico Peso",
				"abbreviation" => "MXN",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Mongolia Tughrik",
				"abbreviation" => "MNT",
				"symbol" => "&#8366;"
			],
			[
				"currency" => "Mozambique Metical",
				"abbreviation" => "MZN",
				"symbol" => "&#77;&#84;"
			],
			[
				"currency" => "Namibia Dollar",
				"abbreviation" => "NAD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Nepal Rupee",
				"abbreviation" => "NPR",
				"symbol" => "&#8360;"
			],
			[
				"currency" => "Netherlands Antilles Guilder",
				"abbreviation" => "ANG",
				"symbol" => "&#402;"
			],
			[
				"currency" => "New Zealand Dollar",
				"abbreviation" => "NZD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Nicaragua Cordoba",
				"abbreviation" => "NIO",
				"symbol" => "&#67;&#36;"
			],
			[
				"currency" => "Nigeria Naira",
				"abbreviation" => "NGN",
				"symbol" => "&#8358;"
			],
			[
				"currency" => "Korea (North] Won",
				"abbreviation" => "KPW",
				"symbol" => "&#8361;"
			],
			[
				"currency" => "Norway Krone",
				"abbreviation" => "NOK",
				"symbol" => "&#107;&#114;"
			],
			[
				"currency" => "Oman Rial",
				"abbreviation" => "OMR",
				"symbol" => "&#65020;"
			],
			[
				"currency" => "Pakistan Rupee",
				"abbreviation" => "PKR",
				"symbol" => "&#8360;"
			],
			[
				"currency" => "Panama Balboa",
				"abbreviation" => "PAB",
				"symbol" => "&#66;&#47;&#46;"
			],
			[
				"currency" => "Paraguay Guarani",
				"abbreviation" => "PYG",
				"symbol" => "&#71;&#115;"
			],
			[
				"currency" => "Peru Nuevo Sol",
				"abbreviation" => "PEN",
				"symbol" => "&#83;&#47;&#46;"
			],
			[
				"currency" => "Philippines Peso",
				"abbreviation" => "PHP",
				"symbol" => "&#8369;"
			],
			[
				"currency" => "Poland Zloty",
				"abbreviation" => "PLN",
				"symbol" => "&#122;&#322;"
			],
			[
				"currency" => "Qatar Riyal",
				"abbreviation" => "QAR",
				"symbol" => "&#65020;"
			],
			[
				"currency" => "Romania New Leu",
				"abbreviation" => "RON",
				"symbol" => "&#108;&#101;&#105;"
			],
			[
				"currency" => "Russia Ruble",
				"abbreviation" => "RUB",
				"symbol" => "&#8381;"
			],
			[
				"currency" => "Saint Helena Pound",
				"abbreviation" => "SHP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Saudi Arabia Riyal",
				"abbreviation" => "SAR",
				"symbol" => "&#65020;"
			],
			[
				"currency" => "Serbia Dinar",
				"abbreviation" => "RSD",
				"symbol" => "&#1044;&#1080;&#1085;&#46;"
			],
			[
				"currency" => "Seychelles Rupee",
				"abbreviation" => "SCR",
				"symbol" => "&#8360;"
			],
			[
				"currency" => "Singapore Dollar",
				"abbreviation" => "SGD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Solomon Islands Dollar",
				"abbreviation" => "SBD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Somalia Shilling",
				"abbreviation" => "SOS",
				"symbol" => "&#83;"
			],
			[
				"currency" => "South Africa Rand",
				"abbreviation" => "ZAR",
				"symbol" => "&#82;"
			],
			[
				"currency" => "Korea (South] Won",
				"abbreviation" => "KRW",
				"symbol" => "&#8361;"
			],
			[
				"currency" => "Sri Lanka Rupee",
				"abbreviation" => "LKR",
				"symbol" => "&#8360;"
			],
			[
				"currency" => "Sweden Krona",
				"abbreviation" => "SEK",
				"symbol" => "&#107;&#114;"
			],
			[
				"currency" => "Switzerland Franc",
				"abbreviation" => "CHF",
				"symbol" => "&#67;&#72;&#70;"
			],
			[
				"currency" => "Suriname Dollar",
				"abbreviation" => "SRD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Syria Pound",
				"abbreviation" => "SYP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "Taiwan New Dollar",
				"abbreviation" => "TWD",
				"symbol" => "&#78;&#84;&#36;"
			],
			[
				"currency" => "Thailand Baht",
				"abbreviation" => "THB",
				"symbol" => "&#3647;"
			],
			[
				"currency" => "Trinidad and Tobago Dollar",
				"abbreviation" => "TTD",
				"symbol" => "&#84;&#84;&#36;"
			],
			[
				"currency" => "Turkey Lira",
				"abbreviation" => "TRY",
				"symbol" => null
			],
			[
				"currency" => "Turkey Lira",
				"abbreviation" => "TRL",
				"symbol" => "&#8356;"
			],
			[
				"currency" => "Tuvalu Dollar",
				"abbreviation" => "TVD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Ukraine Hryvna",
				"abbreviation" => "UAH",
				"symbol" => "&#8372;"
			],
			[
				"currency" => "United Kingdom Pound",
				"abbreviation" => "GBP",
				"symbol" => "&#163;"
			],
			[
				"currency" => "United States Dollar",
				"abbreviation" => "USD",
				"symbol" => "&#36;"
			],
			[
				"currency" => "Uruguay Peso",
				"abbreviation" => "UYU",
				"symbol" => "&#36;&#85;"
			],
			[
				"currency" => "Uzbekistan Som",
				"abbreviation" => "UZS",
				"symbol" => "&#1083;&#1074;"
			],
			[
				"currency" => "Venezuela Bolivar",
				"abbreviation" => "VEF",
				"symbol" => "&#66;&#115;"
			],
			[
				"currency" => "Viet Nam Dong",
				"abbreviation" => "VND",
				"symbol" => "&#8363;"
			],
			[
				"currency" => "Yemen Rial",
				"abbreviation" => "YER",
				"symbol" => "&#65020;"
			],
			[
				"currency" => "Zimbabwe Dollar",
				"abbreviation" => "ZWD",
				"symbol" => "&#90;&#36;"
			]
		];

	private static $currenciesAndCodes = [
		'AFN' => [ 'AF' ],
		'ALL' => [ 'AL' ],
		'DZD' => [ 'DZ' ],
		'USD' => [ 'AS', 'IO', 'GU', 'MH', 'FM', 'MP', 'PW', 'PR', 'TC', 'US', 'UM', 'VI' ],
		'EUR' => [ 'AD', 'AT', 'BE', 'CY', 'EE', 'FI', 'FR', 'GF', 'TF', 'DE', 'GR', 'GP', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'MQ', 'YT', 'MC', 'ME', 'NL', 'PT', 'RE', 'PM', 'SM', 'SK', 'SI', 'ES' ],
		'AOA' => [ 'AO' ],
		'XCD' => [ 'AI', 'AQ', 'AG', 'DM', 'GD', 'MS', 'KN', 'LC', 'VC' ],
		'ARS' => [ 'AR' ],
		'AMD' => [ 'AM' ],
		'AWG' => [ 'AW' ],
		'AUD' => [ 'AU', 'CX', 'CC', 'HM', 'KI', 'NR', 'NF', 'TV' ],
		'AZN' => [ 'AZ' ],
		'BSD' => [ 'BS' ],
		'BHD' => [ 'BH' ],
		'BDT' => [ 'BD' ],
		'BBD' => [ 'BB' ],
		'BYR' => [ 'BY' ],
		'BZD' => [ 'BZ' ],
		'XOF' => [ 'BJ', 'BF', 'ML', 'NE', 'SN', 'TG' ],
		'BMD' => [ 'BM' ],
		'BTN' => [ 'BT' ],
		'BOB' => [ 'BO' ],
		'BAM' => [ 'BA' ],
		'BWP' => [ 'BW' ],
		'NOK' => [ 'BV', 'NO', 'SJ' ],
		'BRL' => [ 'BR' ],
		'BND' => [ 'BN' ],
		'BGN' => [ 'BG' ],
		'BIF' => [ 'BI' ],
		'KHR' => [ 'KH' ],
		'XAF' => [ 'CM', 'CF', 'TD', 'CG', 'GQ', 'GA' ],
		'CAD' => [ 'CA' ],
		'CVE' => [ 'CV' ],
		'KYD' => [ 'KY' ],
		'CLP' => [ 'CL' ],
		'CNY' => [ 'CN' ],
		'HKD' => [ 'HK' ],
		'COP' => [ 'CO' ],
		'KMF' => [ 'KM' ],
		'CDF' => [ 'CD' ],
		'NZD' => [ 'CK', 'NZ', 'NU', 'PN', 'TK' ],
		'CRC' => [ 'CR' ],
		'HRK' => [ 'HR' ],
		'CUP' => [ 'CU' ],
		'CZK' => [ 'CZ' ],
		'DKK' => [ 'DK', 'FO', 'GL' ],
		'DJF' => [ 'DJ' ],
		'DOP' => [ 'DO' ],
		'ECS' => [ 'EC' ],
		'EGP' => [ 'EG' ],
		'SVC' => [ 'SV' ],
		'ERN' => [ 'ER' ],
		'ETB' => [ 'ET' ],
		'FKP' => [ 'FK' ],
		'FJD' => [ 'FJ' ],
		'GMD' => [ 'GM' ],
		'GEL' => [ 'GE' ],
		'GHS' => [ 'GH' ],
		'GIP' => [ 'GI' ],
		'QTQ' => [ 'GT' ],
		'GGP' => [ 'GG' ],
		'GNF' => [ 'GN' ],
		'GWP' => [ 'GW' ],
		'GYD' => [ 'GY' ],
		'HTG' => [ 'HT' ],
		'HNL' => [ 'HN' ],
		'HUF' => [ 'HU' ],
		'ISK' => [ 'IS' ],
		'INR' => [ 'IN' ],
		'IDR' => [ 'ID' ],
		'IRR' => [ 'IR' ],
		'IQD' => [ 'IQ' ],
		'GBP' => [ 'IM', 'JE', 'GS', 'GB' ],
		'ILS' => [ 'IL' ],
		'JMD' => [ 'JM' ],
		'JPY' => [ 'JP' ],
		'JOD' => [ 'JO' ],
		'KZT' => [ 'KZ' ],
		'KES' => [ 'KE' ],
		'KPW' => [ 'KP' ],
		'KRW' => [ 'KR' ],
		'KWD' => [ 'KW' ],
		'KGS' => [ 'KG' ],
		'LAK' => [ 'LA' ],
		'LBP' => [ 'LB' ],
		'LSL' => [ 'LS' ],
		'LRD' => [ 'LR' ],
		'LYD' => [ 'LY' ],
		'CHF' => [ 'LI', 'CH' ],
		'MKD' => [ 'MK' ],
		'MGF' => [ 'MG' ],
		'MWK' => [ 'MW' ],
		'MYR' => [ 'MY' ],
		'MVR' => [ 'MV' ],
		'MRO' => [ 'MR' ],
		'MUR' => [ 'MU' ],
		'MXN' => [ 'MX' ],
		'MDL' => [ 'MD' ],
		'MNT' => [ 'MN' ],
		'MAD' => [ 'MA', 'EH' ],
		'MZN' => [ 'MZ' ],
		'MMK' => [ 'MM' ],
		'NAD' => [ 'NA' ],
		'NPR' => [ 'NP' ],
		'ANG' => [ 'AN' ],
		'XPF' => [ 'NC', 'WF' ],
		'NIO' => [ 'NI' ],
		'NGN' => [ 'NG' ],
		'OMR' => [ 'OM' ],
		'PKR' => [ 'PK' ],
		'PAB' => [ 'PA' ],
		'PGK' => [ 'PG' ],
		'PYG' => [ 'PY' ],
		'PEN' => [ 'PE' ],
		'PHP' => [ 'PH' ],
		'PLN' => [ 'PL' ],
		'QAR' => [ 'QA' ],
		'RON' => [ 'RO' ],
		'RUB' => [ 'RU' ],
		'RWF' => [ 'RW' ],
		'SHP' => [ 'SH' ],
		'WST' => [ 'WS' ],
		'STD' => [ 'ST' ],
		'SAR' => [ 'SA' ],
		'RSD' => [ 'RS' ],
		'SCR' => [ 'SC' ],
		'SLL' => [ 'SL' ],
		'SGD' => [ 'SG' ],
		'SBD' => [ 'SB' ],
		'SOS' => [ 'SO' ],
		'ZAR' => [ 'ZA' ],
		'SSP' => [ 'SS' ],
		'LKR' => [ 'LK' ],
		'SDG' => [ 'SD' ],
		'SRD' => [ 'SR' ],
		'SZL' => [ 'SZ' ],
		'SEK' => [ 'SE' ],
		'SYP' => [ 'SY' ],
		'TWD' => [ 'TW' ],
		'TJS' => [ 'TJ' ],
		'TZS' => [ 'TZ' ],
		'THB' => [ 'TH' ],
		'TOP' => [ 'TO' ],
		'TTD' => [ 'TT' ],
		'TND' => [ 'TN' ],
		'TRY' => [ 'TR' ],
		'TMT' => [ 'TM' ],
		'UGX' => [ 'UG' ],
		'UAH' => [ 'UA' ],
		'AED' => [ 'AE' ],
		'UYU' => [ 'UY' ],
		'UZS' => [ 'UZ' ],
		'VUV' => [ 'VU' ],
		'VEF' => [ 'VE' ],
		'VND' => [ 'VN' ],
		'YER' => [ 'YE' ],
		'ZMW' => [ 'ZM' ],
		'ZWD' => [ 'ZW' ],
	];

	public static function getSymbol ( $code )
	{
		$currency = collect( self::$currencies )->firstWhere( "abbreviation", $code );
		return $currency ? $currency[ 'symbol' ] : $code;
	}

	public static function getCurrencyByCoutryCode ( $code )
	{
		$currency = null;
		foreach ( self::$currenciesAndCodes as $cur => $currencies ) {
			if ( in_array( strtoupper( $code ), $currencies ) ) {
				$currency = $cur;
			}
		}

		$currency = $currency ? $currency : "USD";

		return [
			'code' => $currency,
			'symbol' => self::getSymbol( $currency )
		];
	}
}

