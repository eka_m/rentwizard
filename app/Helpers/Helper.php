<?php
function makeCollection ( $arr )
{
	$result = collect( [] );
	if ( !empty( $arr ) ) {
		foreach ( $arr as $key => $value ) {
			$result->put( $key, $value );
		}
	}
	return $result;
}

function tmp_path ( $file )
{
	return storage_path( 'app/temp/' . ltrim( $file, '/' ) );
}

function setActive ( $path, $class = "active" )
{
	return \Request::is( $path . '*' ) ? $class : '';
}

function is_exists ( $item, $alter = 'Not specified' )
{
	$alter = __( $alter );
	if ( is_array( $item ) ) {
		return !empty( $item ) ? $item : $alter;
	}
	switch ( trim( $item ) ) {
		case null:
		case '':
			return $alter;
			break;
		default:
			return $item;
	}
}

function is_exists_arr ( $item, $key = false, $alter = 'not_specified' )
{
	$alter = __( $alter );
	if ( is_array( $item ) ) {
		return !empty( $item ) ? $item : $alter;
	}
	if ( !isset( $item[ $key ] ) ) {
		return $alter;
	}
	return $item[ $key ];
}

function domainFromEmail ( $email )
{
	return substr( strrchr( $email, "@" ), 1 );
}

function tenancy ()
{
	return app( \Hyn\Tenancy\Environment::class );
}

function website ()
{
	return tenancy()->tenant();
}

function is_system ()
{
	return website()->id == 1;
}

function operationMessage ( $key )
{
	$messages = [
		"DATA_SAVED" => __( 'Data successfully saved.' ),
		"DATA_UPDATED" => __( 'Data successfully updated.' ),
		"DATA_DELETED" => __( 'Data successfully deleted.' ),
		"SERVER_SUCCESS" => __( 'The operation was successful.' ),
		"SERVER_ERROR" => __( 'Ooops... Something went wrong.' ),
	];
	return $messages[ $key ];
}

function cacheLang ( $locale, $refresh = false )
{
	if ( $refresh ) {
		\Illuminate\Support\Facades\Cache::forget( 'lang.js' );
	}
	return \Illuminate\Support\Facades\Cache::rememberForever( 'lang.js', function () use ( $locale ) {
		$file = resource_path( 'lang/' . strtolower( $locale ) . '.json' );
		$strings = file_get_contents( $file );
		if ( strtolower( $locale ) == 'en' ) {
			$values = json_decode( $strings, true );
			$strings = json_encode( array_combine( array_keys( $values ), array_keys( $values ) ) );
		}
		return $strings;
	} );
}

function debugOn ( $tenant = null )
{
	if ( $tenant === null ) $tenant = "126522";
	if ( website() && website()->pin == $tenant ) {
		\Illuminate\Support\Facades\Config::set( 'app.debug', true );
		\Illuminate\Support\Facades\Config::set( 'debugbar.enabled', true );
		\Debugbar::enable();
	}
}

function currencySymbol ()
{
	return html_entity_decode( setting( 'currency.symbol' ) );
}

function until ( $start, $end )
{
	$start = $start->timestamp;
	$end = $end->timestamp;
	$current = \Carbon\Carbon::now()->timestamp;
	return round( ( ( $current - $start ) / ( $end - $start ) ) * 100 );
}

function badge ( $value )
{
	return "<span class='m-badge m-badge--wide m-badge--" . __( 'classes.' . $value ) . "'>" . __( $value ) . "</span>";
}

function onlyInAccaunt ( $callback, $tenant = null )
{
	if ( $tenant === null ) $tenant = "126522";
	if ( website() && website()->pin == $tenant ) {
		$callback();
	}
}

function perm ( $permisson )
{
	auth( 'employee' )->user()->can( $permisson ) ?: abort( 403 );
}