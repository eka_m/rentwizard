<?php

namespace App\Helpers;

use App\Helpers\Media\TenantStorage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;


class ImageManager
{

	public static function makeThumb ( $originalImage, $width, $height, $prefix = "", $quality = 60 )
	{
		$storage = Storage::disk( 'tenant' );
		$tenantstorage = app( TenantStorage::class );
		if ( $storage->exists( $originalImage ) ) {

			$file = $storage->path( $originalImage );

			$img = Image::make( $file );
			$width = $width != 'auto' ? $width : null;
			$height = $height != 'auto' ? $height : null;

			$optimizedimg = $img->resize( $width, $height, function ( $constraint ) {
				$constraint->aspectRatio();
				$constraint->upsize();
			} );

			$imageName = basename( $originalImage );
			$directory = rtrim( $originalImage, $imageName );

			$thumbDirectory = "thumbs/";
			$newImageName = str_replace( $imageName, $thumbDirectory . $prefix . $imageName, $originalImage );

			if ( !$storage->exists( $directory . $thumbDirectory ) ) {
				$storage->makeDirectory( $directory . $thumbDirectory );
			}

			if ( !$storage->exists( $newImageName ) ) {
				$optimizedimg->save( $tenantstorage->toStorage( $newImageName ), $quality );
			}

			return $newImageName;
		};
		return $originalImage;
	}

	public static function fromBase64 ( $base64, $path = '', $name = null )
	{
		$storage = Storage::disk( 'tenant' );
		$tenantstorage = app( TenantStorage::class );

		$name = $name ? $name : Hash::encode( time() );

		$image = Image::make( $base64 );


		$folder = 'media/' . $path . '/';
		$extension = self::getExtentsion( $image );
		$imageName = $folder . $name . $extension;

		if ( !$storage->exists( $folder ) ) {
			$storage->makeDirectory( $folder );
		}

		$image->save( $tenantstorage->toStorage( $imageName ) );

		return [
			"mime" => $image->mime(),
			"size" => $image->filesize(),
			"name" => $name . $extension,
			"file" => $imageName,
		];
	}

	public static function getExtentsion ( $image )
	{
		$res = explode( '/', $image->mime() );
		return '.' . end( $res );
	}
}