<?php

namespace App\Helpers\Media;

use App\Helpers\ImageManager;
use Illuminate\Support\Facades\Storage;

trait HasMediaTrait
{

	private $files = [];

	public static function bootHasMedia ()
	{
	}

	public function media ()
	{
		return $this->morphMany( MediaModel::class, 'model' );
	}

	public function file ( $file ): MediaBuilder
	{
		return app( MediaBuilder::class )->file( $file, $this );
	}

	public function fileFromBase64 ( $file, $path, $name )
	{
		return app( MediaBuilder::class )->fromBase64( $file, $path, $name, $this );
	}

	public function filesFromRequest ( callable $callable = null, string $input = 'files' ): array
	{
		$result = [];
		$files = request()->file( $input );
		if ( !is_array( $files ) ) $files = [ $files ];
		foreach ( $files as $file ) {
			$builder = $this->file( $file );
			$result[] = $callable ? $callable( $builder )->add() : $builder->add();
		}
		return $result;
	}

	public function filesFromTmp ( $ids, callable $callback = null ): array
	{
		$result = [];
		if ( !is_array( $ids ) ) $ids = [ $ids ];
		$files = MediaModel::whereIn( 'id', $ids )->get();
		foreach ( $files as $file ) {
			$model = get_class( $this );
			if ( $file->model_type !== $model ) {
//				if ( Storage::disk( 'tenant' )->move( 'temp/' . $file->url, 'public/' . $file->url ) ) {
				$file->model_id = $this->id;
				$file->model_type = $model;
				$file->tmp = null;
//				}
			}
			$result[] = $callback ? $callback( $file )->save() : $file->save();
		}
		return $result;
	}

	public function clearMedia ( string $collection = null )
	{
		return $this->media()->where( function ( $query ) use ( $collection ) {
			if ( $collection ) return $query->where( 'collection', $collection );
			return $query;
		} )->delete();
	}

	public function clearMediaExcept ( array $ids, string $collection = null )
	{
		return $this->media()->where( function ( $query ) use ( $collection ) {
			if ( $collection ) return $query->where( 'collection', $collection );
			return $query;
		} )->whereNotIn( "id", $ids )->delete();
	}

}