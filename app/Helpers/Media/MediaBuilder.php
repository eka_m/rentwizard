<?php
/**
 * Created by PhpStorm.
 * User: eka
 * Date: 2019-01-25
 * Time: 16:37
 */

namespace App\Helpers\Media;

use App\Helpers\ImageManager;
use App\Helpers\TenantManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class MediaBuilder
{

	private $model;
	private $file;
	private $params = [
		"disk" => 'tenant',
		"collection" => 'default',
		"file_name" => null,
		"original_name" => null,
		"url" => null,
		"mime_type" => null,
		"tmp" => null,
		"size" => 0,
		"manipulations" => [],
		"properties" => [],
	];

	public function file ( $file, Model $model )
	{
		$this->model = $model;
		$this->file = $file;
		$this->params[ 'file_name' ] = $this->generateRandomName();
		$this->params[ 'mime_type' ] = $file->getMimeType();
		$this->params[ 'original_name' ] = $file->getClientOriginalName();
		$this->params[ 'size' ] = $file->getSize();
		return $this;
	}

	public function fromBase64 ( $file, $path, $name, Model $model )
	{
		$this->model = $model;
		$result = ImageManager::fromBase64( $file, $path, $name );
		$this->file = $result[ 'file' ];
		$this->params[ 'file_name' ] = $result[ 'name' ];
		$this->params[ 'mime_type' ] = $result[ 'mime' ];
		$this->params[ 'size' ] = $result[ 'size' ];
		return $this;
	}

	public function setName ( string $name ): self
	{
		$this->params[ 'file_name' ] = $name . '.' . $this->file->getClientOriginalExtension();
		return $this;
	}

	public function withPrefix ( string $prefix ): self
	{
		$this->params[ 'file_name' ] = $prefix . $this->params[ 'file_name' ];
		return $this;
	}

	public function withProperties ( string $properties ): self
	{
		$this->params[ 'properties' ] = json_decode( $properties );
		return $this;
	}

	public function addProperties ( array $properties ): self
	{
		foreach ( $properties as $key => $property ) {
			$this->addProperty( $key, $property );
		}
		return $this;
	}

	public function addProperty ( string $key, string $value ): self
	{
		$this->params[ 'properties' ][ $key ] = $value;
		return $this;
	}

	public function toDisk ( string $disk = 'public' ): self
	{
		$this->params[ 'disk' ] = $disk;
		return $this;
	}

	public function toCollection ( string $collection = "default" ): self
	{
		$this->params[ 'collection' ] = $collection;
		return $this;
	}

	public function temporary ( int $minutes = 0 ): self
	{
		if ( $minutes ) {
			$this->params[ 'tmp' ] = date( 'Y-m-d H:i:s', time() + $minutes * 60 );
		}
		return $this;
	}

	public function add ()
	{
		if ( is_string( $this->file ) ) {
			$this->params[ 'url' ] = $this->file;
			return $this->model->media()->create( $this->params );
		}

		if ( $this->params[ 'url' ] = UploadManager::upload( $this->file, $this->params ) ) {
			return $this->model->media()->create( $this->params );
		}
		return false;
	}

	public function get ( string $value )
	{
		return $this->params[ $value ];
	}

	private function generateRandomName ()
	{
		return 'file_' . mt_rand( 100, 999999 ) . '_' . time() . '.' . $this->file->getClientOriginalExtension();
	}
}