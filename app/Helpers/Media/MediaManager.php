<?php
/**
 * Created by PhpStorm.
 * User: eka
 * Date: 2019-01-25
 * Time: 17:29
 */

namespace App\Helpers\Media;


use Illuminate\Database\Eloquent\Model;

class MediaManager extends Model
{
	use HasMediaTrait;

	private static $instance;

	public static function open ()
	{
		if ( !self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function anonims ()
	{
		return MediaModel::whereNull( 'model_id' );
	}

	public function getMedia ()
	{
		return MediaModel::all();
	}
}