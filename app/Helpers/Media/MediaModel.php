<?php

namespace App\Helpers\Media;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MediaModel extends Model
{

	use UsesTenantConnection;
	use HasMediaTrait;

	protected $table = 'media';

	protected $guarded = [];
	protected $appends = [ 'dataURL' ];
	protected $casts = [ 'properties' => 'array', 'manipulations' => 'array' ];

	public function model ()
	{
		return $this->morphTo();
	}

	public function getDataURLAttribute ()
	{
		return "/" . $this->attributes[ 'url' ];
	}
}