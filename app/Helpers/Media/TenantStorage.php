<?php
/**
 * Created by PhpStorm.
 * User: eka
 * Date: 2019-02-18
 * Time: 09:15
 */

namespace App\Helpers\Media;


use Hyn\Tenancy\Website\Directory;

class TenantStorage extends Directory
{
	public function url ( $path )
	{
		return $this->filesystem->url(
			$this->path( $path )
		);
	}

	public function toStorage ( $file )
	{
		return storage_path( "app/tenancy/tenants/". $this->path() . $file );
	}
}