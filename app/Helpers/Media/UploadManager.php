<?php

namespace App\Helpers\Media;

use Illuminate\Support\Facades\Storage;

class UploadManager
{
	public static function upload ( $file, array $params )
	{
		return Storage::disk( $params[ 'disk' ] )->putFileAs( "media/" . $params[ 'collection' ], $file, $params[ 'file_name' ] );
	}
}