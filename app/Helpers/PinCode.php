<?php

namespace App\Helpers;
class PinCode
{
	private $model;
	private $length;

	public function __construct ( $model, $length = 6 )
	{
		$this->length = $length;
		$this->model = $model;
	}

	public function generate ()
	{
		$pin = $this->randomInteger();
		if ( $this->model::where( 'pin', $pin )->exists() ) {
			$this->generate();
		}
		return $pin;
	}

	private function randomInteger ()
	{
		$result = '';
		for ( $i = 0; $i < $this->length; $i++ ) {
			$result .= mt_rand( 0, 9 );
		}
		return $result;
	}
}