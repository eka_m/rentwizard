<?php

namespace App\Helpers;

use App\Models\App;
use App\Models\Category;
use App\Models\Role;
use App\Models\User;
use App\Models\Website;
use App\Models\Hostname;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Vinkla\Hashids\Facades\Hashids;
use Hyn\Tenancy\Environment;

class TenantManager
{

	public static function switch ( $pin )
	{
		$website = Website::pin( $pin );
		tenancy()->tenant( $website );
		return $website;
	}

	public static function switchAndLogin ( $pin, $email )
	{
		$website = self::switch( $pin );
		if ( $user = User::where( "email", $email )->first() ) {
			$res = Auth::guard( "employee" )->login( $user );
			return $website;
		}
		return null;
	}

	public static function create ( $user )
	{
		if ( $user->company ) {

			$pin = new PinCode( "App\Models\Website" );
			$code = $pin->generate();
			if ( $user->company == 'Mandarin Agency' ) $code = '000000';

			$timezone = \DateTimeZone::listIdentifiers( \DateTimeZone::PER_COUNTRY, strtoupper( $user->locale ) );

			$website = new Website;
			$website->uuid = $code;
			$website->pin = $code;
			$website->customer_id = $user->id;
			$website->timezone = $timezone[ 0 ];

			app( WebsiteRepository::class )->create( $website );


			$host = new Hostname;
			$host->fqdn = $code . "." . env( "APP_FQDN" );
			$host->force_https = true;
			$host = app( HostnameRepository::class )->create( $host );
			app( HostnameRepository::class )->attach( $host, $website );
			self::addInitialData( $user, $website );
		}
		return true;
	}

	public static function addInitialData ( $user, $website )
	{
		$tenancy = app( Environment::class );

		$main = website();
		$tenancy->tenant( $website );

		$currency = Currency::getCurrencyByCoutryCode( $user->locale );

		User::create( $user->makeVisible( "password" )->toArray() )->assignRole( "root" );
//		User::create( [ "name" => "SYSTEM", "locale" => "EN", "email" => "sys@sys.s", "password" => Hash::make( 'system9898' ) ] )->assignRole( "root" );
		App::insert( [
			[ "key" => "app", "value" => 'good' ],
			[ "key" => "timezone", "value" => $website->timezone ],
			[ "key" => "currency.code", "value" => $currency[ 'code' ] ],
			[ "key" => "currency.symbol", "value" => $currency[ 'symbol' ] ]
		] );
		$tenancy->tenant( $main );
	}

}