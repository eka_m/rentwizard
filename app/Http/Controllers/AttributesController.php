<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Toastr;

class AttributesController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$attributes = Attribute::orderBy( 'created_at', 'DESC' )->get();
		return view( 'attributes.index', compact( 'attributes' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		$attribute = new Attribute;
		return view( 'attributes.create', compact( 'attribute' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store ( Request $request )
	{
		Attribute::create( $request->all() );
		\Toastr::success( "Атрибут успешно добавлен" );
		return redirect()->route( 'attributes.index' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Attribute $attribute
	 * @return \Illuminate\Http\Response
	 */
	public function show ( Attribute $attribute )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Attribute $attribute
	 * @return \Illuminate\Http\Response
	 */
	public function edit ( Attribute $attribute )
	{
		return view( 'attributes.edit', compact( 'attribute' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\Models\Attribute $attribute
	 * @return \Illuminate\Http\Response
	 */
	public function update ( Request $request, Attribute $attribute )
	{
		$attribute->update( $request->all() );
		\Toastr::success( "Атрибут успешно сохранен" );
		return redirect()->route( 'attributes.index' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Attribute $attribute
	 * @return \Illuminate\Http\Response
	 * @throws \Exception
	 */
	public function destroy ( Attribute $attribute )
	{
		$attribute->delete();
		\Toastr::success( "Атрибут успешно удален" );
		return redirect()->route( 'attributes.index' );
	}
}
