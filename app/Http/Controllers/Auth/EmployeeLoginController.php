<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class EmployeeLoginController extends Controller
{

	protected $redirectTo = '/';

	public static function check ( Request $request )
	{
		if ( $request[ "pin" ] && $website = Website::pin( $request[ 'pin' ] ) ) {
			tenancy()->tenant( $website );
			$user = User::where( 'email', $request[ 'email' ] )->first();
			if ( $user && Hash::check( $request[ 'password' ], $user->makeVisible( 'password' )->password ) ) {
				$hash = Crypt::encrypt( $user->email . "%sep%" . $user->makeVisible( "password" )->password . "%sep%" . $website->pin );
				return route( "tenant.login", [ "tenant" => $website->host, "hash" => $hash ] );
			}
			return false;
		}
		return false;
	}

	public function login ( Request $request )
	{

		$data = explode( "%sep%", Crypt::decrypt( $request[ "hash" ] ) );
		$auth[ "email" ] = $data[ 0 ];
		$auth[ "password" ] = $data[ 1 ];

		$user = User::where( $auth )->first();

		if ( $user ) {
			Auth::guard( "employee" )->login( $user );
		}
		return redirect()->route( 'dashboard', [ "tenant" => website()->host ] );
	}


	public function logout ( Request $request )
	{
		$this->guard()->logout();

		$request->session()->invalidate();

		return redirect()->route( "homepage" );
	}

	protected function authenticated ( Request $request, $user )
	{
		if ( $request->ajax() ) {
			$userBar = view( 'web.partials.user-box' )->render();
			return response()->json( [ "data" => $userBar, "redirect" => $this->redirectTo !== '/' ? $this->redirectTo : null ] );
		}
	}

	protected function guard ()
	{
		return Auth::guard( "employee" );
	}

}
