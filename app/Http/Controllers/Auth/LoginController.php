<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\TenantManager;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Website;
use App\Rules\Pin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	protected $guard = "web";

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		$this->middleware( 'guest' )->except( 'logout' );
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
	 *
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function login ( Request $request )
	{
		$this->validateLogin( $request );

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if ( $this->hasTooManyLoginAttempts( $request ) ) {
			$this->fireLockoutEvent( $request );

			return $this->sendLockoutResponse( $request );
		}

		if ( $link = EmployeeLoginController::check( $request ) ) {
			$request->session()->regenerate();
			$this->clearLoginAttempts( $request );
			if ( $request->ajax() ) {
				return response()->json( [ "redirect" => $link ] );
			}
			return redirect( $link );
		}

		if ( $this->attemptLogin( $request ) ) {
			return $this->sendLoginResponse( $request );
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts( $request );

		return $this->sendFailedLoginResponse( $request );
	}


	/**
	 * Validate the user login request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return void
	 *
	 * @throws \Illuminate\Validation\ValidationException
	 */
	protected function validateLogin ( Request $request )
	{

		$request->validate( [
			$this->username() => 'required|string',
			'pin' => [ new Pin ],
			'password' => 'required|string',
		] );
	}


	protected function authenticated ( Request $request, $user )
	{

		if ( $request->ajax() ) {
			$userBar = view( 'web.partials.user-box' )->render();
			return response()->json( [ "data" => $userBar, "redirect" => $this->redirectTo ] );
		}
	}


	protected function guard ()
	{
		return Auth::guard();
	}
}
