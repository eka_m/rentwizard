<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\PasswordReset;

class ResetPasswordController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Where to redirect users after resetting their password.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		$this->middleware( 'guest' );
	}

	protected function resetPassword ( $user, $password )
	{
		$main = tenancy()->website();
		$pass = Hash::make( $password );
		$user->password = $pass;

		$user->setRememberToken( Str::random( 60 ) );

		foreach ( $user->websites as $website ) {
			tenancy()->tenant( $website );
			User::where( 'email', $user->email )->update( [ 'password' => $pass ] );
		}
		tenancy()->tenant( $main );
		$user->save();

		event( new PasswordReset( $user ) );

		$this->guard()->login( $user );
	}

	/**
	 * Get the response for a successful password reset.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  string $response
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
	 */
	protected function sendResetResponse ( Request $request, $response )
	{
		if ( $request->ajax() ) {
			return response()->json( trans( $response ) );
		}
		return redirect( $this->redirectPath() )
			->with( 'status', trans( $response ) );
	}

	protected function sendResetFailedResponse ( Request $request, $response )
	{
		if ( $request->ajax() ) {
			return response( trans( $response ), 404 );
		}
		return redirect()->back()
			->withInput( $request->only( 'email' ) )
			->withErrors( [ 'email' => trans( $response ) ] );
	}
}
