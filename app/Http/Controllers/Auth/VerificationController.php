<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\TenantManager;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Website;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Access\AuthorizationException;

class VerificationController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Email Verification Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling email verification for any
	| user that recently registered with the application. Emails may also
	| be re-sent if the user didn't receive the original email message.
	|
	*/

	use VerifiesEmails;

	/**
	 * Where to redirect users after verification.
	 *
	 * @var string
	 */
	protected $redirectTo = '/login';

	public function verify ( Request $request )
	{
		$user = Customer::whereNull( "email_verified_at" )->find( $request->route( 'id' ) );

		if ( $user === null || $request->route( 'id' ) != $user->getKey() ) {
			throw new AuthorizationException;
		}

		if ( $user->markEmailAsVerified() ) {
			event( new Verified( $user ) );
		}
		return redirect( $this->redirectPath() )->with( 'verified', true );
	}

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct ()
	{
//		$this->middleware( 'auth' );
		$this->middleware( 'signed' )->only( 'verify' );
		$this->middleware( 'throttle:6,1' )->only( 'verify', 'resend' );
	}


	public function resend ( Request $request )
	{
		if ( $request->user()->hasVerifiedEmail() ) {
			return redirect( $this->redirectPath() );
		}

		$request->user()->sendEmailVerificationNotification();

		if ( $request->ajax() ) {
			return response()->json( [], 200 );
		}

		return back()->with( 'resent', true );
	}
}
