<?php

namespace App\Http\Controllers;

class BaseController extends Controller
{

	public function optimizeForGraph ( $data )
	{

		$result = [];
		for ( $i = 1; $i <= 12; $i++ ) {
			$m = date( 'm', mktime( 0, 0, 0, $i, 1 ) );
			$result[ "2018-$m" ] = [ "date" => "2018-$m", "sum" => 0, 'count' => 0 ];
		}
		if ( $result ) {
			foreach ( $data as $key => $value ) {
				$result[ $key ] = [ "date" => $key, "sum" => $value->sum( 'price' ), 'count' => $value->count() ];
			}
		}

		return json_encode( array_values( $result ) );
	}

	public function createYearData ( $data )
	{
		$result = collect( [ 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0 ] );
		foreach ( $data as $key => $value ) {
			$result->put( $key, $value );
		}
		return $result;
	}
}
