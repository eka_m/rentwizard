<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;
use App\Http\Controllers\BaseController;

class CategoriesController extends BaseController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$categories = Category::orderBy( 'created_at', 'DESC' )->get();
		return view( 'categories.index', compact( 'categories' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		$category = new Category();
		return view( 'categories.create', compact( 'category' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store ( Request $request )
	{
		$request->request->add( [ "slug" => str_slug( $request->name ) ] );
		$category = Category::create( $request->all() );

		if ( $request->ajax() ) {
			return response()->json( [ 'message' => 'Категория успешно добавлена', 'id' => $category->id ] );
		}
		Toastr::success( 'Категория успешно добавлена' );
		return redirect()->route( 'categories.index' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show ( $id )
	{
//        $categories = Product::findOrFail($id);
//        return view('categories.show',compact('categories'));
		return redirect()->route( 'categories.index' );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ( $id )
	{
		$category = Category::findOrFail( $id );
		return view( 'categories.edit', compact( 'category' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update ( Request $request, $id )
	{
		$request->request->add( [ "slug" => str_slug( $request->name ) ] );
		Category::find( $id )->update( $request->all() );
		if ( $request->ajax() ) {
			return response()->json( [ 'message' => 'Категория успешно сохранена' ] );
		}
		Toastr::success( 'Категория сохранена' );
		return redirect()->route( 'categories.index' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ( Request $request, $id )
	{
		Category::find( $id )->delete();
		if ( $request->ajax() ) {
			return response()->json( [ 'message' => 'Категория успешно удалена' ] );
		}
		Toastr::success( 'Категория успешно удалена' );
		return redirect()->route( 'categories.index' );
	}
}
