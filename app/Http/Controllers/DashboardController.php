<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Deal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;

class DashboardController extends Controller
{
	public function show ()
	{

		$month = Deal::whereBetween( 'paid_at', [ Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth() ] )->withoutStaff()->onlyClosed()->status('Completed')->get();
		$week = $month->whereBetween( 'paid_at', [ Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek() ] )->toArray();
		$today = $month->whereBetween( 'paid_at', [ Carbon::now()->startOfDay(), Carbon::now()->endOfDay() ] )->toArray();
		return view( 'dashboard.index', compact( "month", "week", "today" ) );
	}

	public function recentCustomers ()
	{

		$items = Client::whereMonth( 'created_at', Carbon::now()->month )->orderBy( 'created_at', 'DESC' )->paginate( 10 );
		$res = view( 'dashboard.clients.inner', compact( 'items' ) )->render();
		return response()->json( $res );
	}

	public function todaysDeals ()
	{
		$items = Deal::whereDay( 'created_at', Carbon::now()->day )->orderBy( 'created_at', 'DESC' )->with( 'client' )->paginate( 10 );
		$res = view( 'dashboard.deals.inner', compact( 'items' ) )->render();
		return response()->json( $res );
	}

	public function online ( Request $request )
	{
		Redis::set( "online-users", json_encode( $request->get("users") ) );
	}

	public function ofline ( Request $request )
	{
		$online  = Redis::get('online-users');
		Redis::set( "online-users", json_encode( $request->get("users") ) );
	}
}
