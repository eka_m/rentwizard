<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\Client;
use App\Models\DealJob;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Deal;
use Kamaln7\Toastr\Facades\Toastr;
use App\Models\Inventory;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;


class DealsController extends BaseController
{


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		perm( 'View deals' );

		$statuses = Deal::getStatuses();
		return view( 'deals.index', compact( 'statuses' ) );
	}

	public function getDeals ( Request $request )
	{
		$field = $request->input( "search" );
		$status = $request->input( "status" );
		$deal = Deal::orderBy( 'created_at', "DESC" )->with( 'client', 'items', 'manager' )->search( $field )->status( $status )->paginate( 10 );
		return response()->json( $deal );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		perm( 'Create deals' );
		$item = new Deal();
		$item->fill( [
			"manager_profit" => auth( 'employee' )->user()->percent,
			"manager_profit_type" => "percent",
			"autoactivation" => 1,
			"sale" => 0
		] );
		$item->manager = auth( 'employee' )->user();
		$clientstatuses = Client::getStatuses();
		$inventorytstatuses = Inventory::getStatuses();
		Javascript::put( [
			'attributes' => Attribute::forSearch()->get()
		] );
		return view( 'deals.create', compact( 'item', 'clientstatuses', 'inventorytstatuses' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store ( Request $request )
	{
		$this->middleware( "permission:Create deals" );
		$deal = $request->all();
		$deal = $this->setDealStatus( $deal );
		$itemStatus = Inventory::setStatus( $deal[ 'status' ] );
		$items = json_decode( $request[ 'items' ], true );
		$dealModel = Deal::create( $deal );
		$dealModel->items()->attach( $items );
		foreach ( $dealModel->items as $item ) {
			$item->update( [ 'status' => $itemStatus ] );
		}
		DealJob::add( $dealModel );
		Toastr::success( operationMessage( 'DATA_SAVED' ) );
		return redirect()->route( 'deals.index' );
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show ( $id )
	{
		$this->middleware( "permission:View deals" );
		$item = Deal::find( $id );
		$client = $item->client;
		$dealitems = $item->items;

		return view( 'deals.show', compact( 'item', 'client', 'dealitems' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ( $id )
	{
		$clientstatuses = Client::getStatuses();
		$inventorytstatuses = Inventory::getStatuses();
		$item = Deal::with( 'items' )->findOrFail( $id );
		Javascript::put( [
			'attributes' => Attribute::forSearch()->get()
		] );
		return view( 'deals.edit', compact( 'item', 'inventorytstatuses', 'clientstatuses' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update ( Request $request, $id )
	{
		$deal = Deal::findOrFail( $id );
		$data = $request->all();
		if ( Carbon::parse( $data[ 'start' ] )->notEqualTo( $deal->start ) || Carbon::parse( $data[ 'end' ] )->notEqualTo( $deal->end ) ) {
			$data = $this->setDealStatus( $data );
		}

		$itemStatus = Inventory::setStatus( $data[ 'status' ] );

		$items = json_decode( $data[ 'items' ] );

		foreach ( $deal->items as $item ) {
			$item->update( [ 'status' => 'Available' ] );
		}

		if ( $deal->update( $data ) ) {
			$deal->items()->sync( $items );
			foreach ( $deal->refresh()->items as $item ) {
				$item->update( [ 'status' => $itemStatus ] );
			}
		};

		DealJob::change( $id, $data[ 'start' ], $data[ 'end' ], $data[ 'status' ] == "Active" ? 1 : 0 );

		Toastr::success( operationMessage( 'DATA_UPDATED' ) );
		return redirect()->route( 'deals.index' );
	}


	public function setDealStatus ( $deal )
	{
		$deal[ 'status' ] = 'Planned';
		if ( Carbon::parse( $deal[ 'start' ] )->lessThanOrEqualTo( Carbon::now() ) ) {
			$deal[ 'status' ] = 'Active';
		}
		if ( Carbon::parse( $deal[ 'end' ] )->lessThanOrEqualTo( Carbon::now() ) ) {
			$deal[ 'status' ] = 'Expired';
		}
		return $deal;
	}

	public function closeDeal ( Request $request, $id )
	{
		try {
			$deal = Deal::find( $id );
			$deal->update( $request->all() );
			DealJob::remove( $deal->id );
			$items = $deal->items;
			foreach ( $items as $item ) {
				$item->update( [ 'status' => 'Available' ] );
			}
			return response()->json( [ "message" => operationMessage( 'DATA_SAVED' ) ], 200 );
		} catch ( \Exception $e ) {
			return response()->json( operationMessage( 'SERVER_ERROR' ), 500 );
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ( $id )
	{
		$deal = Deal::find( $id );
		$type = "success";
		if ( $deal->status != 'finished' && $deal->status != 'notpaid' ) {
			foreach ( $deal->items as $item ) {
				$deal->items()->updateExistingPivot( $item->id, [ 'deleted_at' => Carbon::now() ] );
				Inventory::find( $item->id )->update( [ 'status' => 'Available' ] );
			}
			DealJob::remove( $id );
			$deal->delete();
			$message = operationMessage( 'DATA_DELETED' );
		} else {
			$message = operationMessage( 'SERVER_ERROR' );
			$type = "warning";
		}
		return response()->json( [ "message" => $message, "type" => $type ], 200 );
	}
}
