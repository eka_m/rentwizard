<?php

namespace App\Http\Controllers;

use App\Exports\DealExport;
use App\Exports\InventoryExport;
use Illuminate\Http\Request;

class ExportsController extends Controller
{
	public function export ( Request $request, $model )
	{
		if ( method_exists( $this, $model ) ) {
			return call_user_func_array( [ $this, $model ], [ $request ] );
		} else {
			abort( 404 );
		}
	}

	public function inventory ( $request )
	{
		return ( new InventoryExport )->download( 'inventory.xlsx' );
	}

	public function deals ( $request )
	{
		return ( new DealExport )->download( 'deals.xlsx' );
	}
}
