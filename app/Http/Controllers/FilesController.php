<?php

namespace App\Http\Controllers;

use App\Helpers\Media\MediaManager;
use Illuminate\Http\Request;

class FilesController extends Controller
{

	public function upload ( Request $request, $input = 'files', $folder = '/' )
	{

	}

	public function uploadTMP ( Request $request, $input = 'files', $collection = 'default', $temporary = 0)
	{

		$properties = $request->has( 'properties' ) ? $request->properties: [];
		return MediaManager::open()->filesFromRequest( function ( $file ) use ( $collection, $temporary, $properties ) {
			return $file->toCollection( $collection )
				->withProperties( $properties[ $file->get( 'original_name' ) ] )
				->temporary( $temporary );
		}, $input);
	}
}
