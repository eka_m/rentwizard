<?php

namespace App\Http\Controllers;

use App\Events\ShowInventory;
use App\Models\App;
use App\Models\Attribute;
use App\Models\Deal;
use App\Notifications\AppNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use App\Models\Category;
use App\Models\Inventory;
use App\Helpers\ImageManager;

class InventoryController extends BaseController
{
	public $timestamps = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index ()
	{
		$categories = Category::orderBy( 'created_at', 'DESC' )->get();
		$statuses = Inventory::getStatuses();
		$attributes = Attribute::forTable()->get();
		return view( 'inventory.index', compact( 'categories', 'statuses', 'attributes' ) );
	}

	public function showInventory ()
	{
		App::send( new AppNotification( "NOTIFICATION LAST" ) );
		return response()->json( "yes" );
	}

	public function getInventory ( Request $request )
	{
		$field = $request->input( "search" );
		$category = $request->input( "category" );
		$status = $request->input( "status" );
		$items = Inventory::orderBy( 'created_at', 'DESC' )->with( 'category' )->search( $field )->category( $category )->status( $status )->paginate( 10 );
		return response()->json( $items );
	}

	public function asyncSearch ( Request $request )
	{
		$result = $this->searchForItem( $request->input( 'field' ), $request->input( 'start' ), $request->input( 'end' ) );
		return response()->json( array_values( $result->toArray() ) );
	}

	public function searchForItem ( $field, $start = null, $finish = null )
	{
		return Inventory::withoutActiveDeals( $start, $finish )->search( $field )->get();
	}

	public function chekForActiveDeals ( Request $request )
	{
		$inputs = $request->all();
		$result = [];
		$deleted = [];
		foreach ( json_decode( $inputs[ 'items' ] ) as $item ) {
			$available = Inventory::withoutActiveDeals( $inputs[ 'start' ], $inputs[ 'end' ] )->withoutDeal( $inputs[ 'deal' ] )->find( $item->id );
			if ( $available ) {
				$result[] = $item;
			} else {
				$deleted[] = $item;
			}
		}
		return response()->json( [ 'items' => $result, 'deleted' => $deleted ] );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		$item = new Inventory();
		$item->status = 'available';
		$item->purschase_date = Carbon::now()->format( 'Y-m-d' );
		$categories = Category::orderBy( 'created_at', 'DESC' )->get();
		$statuses = Inventory::getStatuses();
		$states = Inventory::getStates();
		$attributes = Attribute::all();
		return view( 'inventory.create', compact( 'item', 'categories', 'statuses', 'states', 'attributes' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store ( Request $request )
	{

		$item = Inventory::create( $request->all() );
		$ids = collect( json_decode( $request->images, true ) )->pluck( "id" )->toArray();
		$item->filesFromTmp( $ids, function ( $file ) {
			if ( $file->properties[ 'main' ] ) {
				ImageManager::makeThumb( $file->url, 'auto', 50, 'thumb_', 90 );
			}
			return $file;
		} );
		Toastr::success( 'Предмет успешно добавлен' );
		return redirect()->route( 'inventory.index' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show ( $id )
	{
		$item = Inventory::with( 'category' )->findOrFail( $id );
		$attributes = Attribute::all();

		return view( 'inventory.show', compact( 'item', 'attributes' ) );
	}


	public function getProfitStatistics ( Request $request, $id )
	{
		$records = Inventory::where( "id", $id )->with( 'deals' )->first();
		dd( $records->deals );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ( $id )
	{
		$item = Inventory::with( 'category' )->with( 'media' )->findOrFail( $id );
		$categories = Category::orderBy( 'created_at', 'DESC' )->get();
		$statuses = Inventory::getStatuses();
		$states = Inventory::getStates();
		$attributes = Attribute::all();
		return view( 'inventory.edit', compact( 'item', 'categories', 'statuses', 'states', 'attributes' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update ( Request $request, $id )
	{
		$item = Inventory::find( $id );
		$media = collect( json_decode( $request->images, true ) );
		$ids = $media->pluck( "id" )->toArray();
		$item->clearMediaExcept( $ids );
		$item->filesFromTmp( $ids, function ( $file ) use ( $media ) {
			$file->properties = $media->firstWhere( "id", $file->id )[ 'properties' ];
			if ( $file->properties[ 'main' ] ) {
				ImageManager::makeThumb( $file->url, 'auto', 50, 'thumb_', 90 );
			}
			return $file;
		} );
		$item->update( $request->all() );
		Toastr::success( 'Предмет сохранен' );
		return redirect()->route( 'inventory.index' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ( Request $request, $id )
	{
		Inventory::find( $id )->delete();
		$message = 'Предмет удален из инвентаря';
		if ( $request->ajax() ) {
			return response()->json( [ 'message' => $message ], 200 );
		}
		Toastr::success( $message );
		return redirect()->route( 'inventory.index' );

	}
}
