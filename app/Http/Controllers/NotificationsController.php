<?php

namespace App\Http\Controllers;

use App\Jobs\MarkNotificationsAsRead;
use App\Models\App;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
	public function markAsRead ( Request $request )
	{
		$res = App::getInstance()->notifications()->whereIn( "id", collect($request->all())->pluck("id") )->update( [ 'read_at' => now() ] );
		return response()->json( $res );
	}
}
