<?php

namespace App\Http\Controllers;

use App\Helpers\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Kamaln7\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;


class SettingsController extends BaseController
{
	public function index ()
	{
		return view( 'settings.settings' );
	}

	public function main ()
	{
		return view( 'settings.main' );
	}

	public function save ( Request $request )
	{
		$settings = $request->except( [ '_token', '_method' ] );
		$settings[ 'currency' ][ 'symbol' ] = Currency::getSymbol( $settings[ 'currency' ][ 'code' ] );

		setting( $settings )->save();

		if ( website()->timezone != $settings[ 'timezone' ] ) {
			website()->update( [ 'timezone' => $settings[ 'timezone' ] ] );
		}

		\Toastr::success( operationMessage( 'DATA_SAVED' ) );
		return redirect()->back();
	}

	public function logs ()
	{
		return view( 'settings.log' );
	}

	public function clearLog ( $name )
	{
		if ( $name == 'all' ) {
			$name = [ 'model', 'default', 'auth' ];
		}

		Activity::inLog( $name )->delete();
		$user = Auth::user()->name;
		activity()->state( 'info' )->log( "Пользователь $user очистил лог." );
		Toastr::success( 'Лог успешно очистен!' );
		return redirect()->route( 'settings.for', 'log' );
	}
}
