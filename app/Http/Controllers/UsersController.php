<?php

namespace App\Http\Controllers;

use App\Helpers\Calculate;
use App\Helpers\ImageManager;
use App\Helpers\Media\TenantStorage;
use App\Models\Permission;
use App\Models\User;
use App\Models\Role;
use App\Notifications\UserAdded;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Kamaln7\Toastr\Facades\Toastr;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class UsersController extends BaseController
{



	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$this->middleware( "permission:View users" );
		$users = User::withoutRoot()->orderBy( 'created_at', 'DESC' )->get();
		$roles = Role::withoutRoot()->with( 'permissions' )->orderBy( 'created_at', 'DESC' )->get();
		$permissions = Permission::all();
		return view( 'users.index', compact( 'users', 'roles', 'permissions' ) );
	}

	public function profile ( Request $request )
	{
		$user = auth( 'employee' )->user();
		$res = $user->deals()->onlyClosed()->get();
		$month = $res->whereBetween( 'paid_at', [ Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth() ] )->toArray();
		$week = $res->whereBetween( 'paid_at', [ Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek() ] )->toArray();
		$today = $res->whereBetween( 'paid_at', [ Carbon::now()->startOfDay(), Carbon::now()->endOfDay() ] )->toArray();
		return view( 'users.profile', compact( 'user', 'today', 'month', 'week' ) );
	}

	public function saveProfile ( Request $request )
	{
		$user = auth( 'employee' )->user();

		User::where( "id", $user->id )->update( $request->except( [ '_token', '_method', 'photo' ] ) );
		$locale = $request->get( 'locale' );

		if ( $locale != LaravelLocalization::getCurrentLocale() ) {
			cacheLang( $locale, true );
		}

		\Toastr::success( operationMessage( 'DATA_SAVED' ) );

		return redirect()->back();
	}

	public function avatar ( Request $request )
	{
		try {
			$photo = json_decode( $request->photo );
			$user = auth( 'employee' )->user();
			if ( $photo ) {
				$user->clearMedia();
				$user->fileFromBase64( $photo->output->image, 'users', $user->id . '-avatar' )
					->toCollection( 'users' )->add();
			}
		} catch ( \Exception $e ) {
			return response()->json( [ "message" => operationMessage( "SERVER_ERROR" ) ], 500 );
		}
	}

	public function removeAvatar ()
	{
		try {
			auth( 'employee' )->user()->clearMedia();
			return response()->json( [ "message" => operationMessage( "DATA_DELETED" ) ], 200 );
		} catch ( \Exception $e ) {
			return response()->json( [ "message" => operationMessage( "SERVER_ERROR" ) ], 500 );
		}

	}

	public function resetPassword ( Request $request, $id )
	{
		User::where( "id", $id )->update( [ 'password' => Hash::make( $request->get( 'password' ) ) ] );

		\Toastr::success( operationMessage( 'DATA_UPDATED' ) );
		return redirect()->back();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		$this->middleware( "permission:Create users" );
		$user = new User;
		$roles = Role::withOutRoot()->where( 'name', '!=', 'root' )->get();
		return view( 'users.create', compact( 'user', 'roles' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function store ( Request $request )
	{
		$this->middleware( "permission:Create users" );
		$this->validate( $request, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:tenant.users,email',
		] );

		$randomPass = Str::random( 6 );

		$inputs = $request->all();
		$inputs[ 'password' ] = Hash::make( $randomPass );
		$user = new User;
		$user->fill( $inputs );
		$user->save();
		$user->assignRole( $request->role );
		$user->notify( new UserAdded( $randomPass, $request->role, website()->pin ) );
		Toastr::success( operationMessage( 'DATA_SAVED' ) );
		return redirect()->route( 'users.index' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return void
	 */
	public function show ( $id )
	{


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ( $id )
	{
		$this->middleware( "permission:Create users" );
		$roles = Role::withOutRoot()->get();
		$user = User::findOrFail( $id );
		return view( 'users.edit', compact( 'user', 'roles' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function update ( Request $request, $id )
	{
		$this->middleware( "permission:Create users" );
		$this->validate( $request, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:tenant.users,email',
		] );
		$inputs = $request->all();
		$inputs[ 'password' ] = bcrypt( $inputs[ 'password' ] );
		$user = User::find( $id );
		//TODO: Сделать так чтоб невозможно было дать пользователью больше одного роля
		if ( !$user->hasRole( $request->role ) ) {
			$user->assignRole( $request->role );
		}
		$user->update( $inputs );
		Toastr::success( operationMessage( 'DATA_UPDATED' ) );
		return redirect()->route( 'users.index' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ( $id )
	{
		$this->middleware( "permission:Delete users" );
		User::destroy( $id );
		Toastr::success( operationMessage( 'DATA_DELETED' ) );
		return redirect()->route( 'users.index' );
	}
}
