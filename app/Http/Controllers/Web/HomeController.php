<?php

namespace App\Http\Controllers\Web;

use App\Helpers\PinCode;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hyn\Tenancy\Environment;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class HomeController extends Controller
{
	public function index ()
	{
		return view( 'web.home' );
	}
}
