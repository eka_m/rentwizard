<?php

namespace App\Http\Controllers\Web;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class SystemController extends Controller
{

	public function index ()
	{
		if ( $this->login() ) {
			Artisan::call( "tenancy:run", [ "run" => "get:info" ] );
			$info = json_decode( Cache::get( 'tenantsinfo' ), true );
			$online = collect(json_decode(Redis::get('online-users')));
			$customers = Customer::with( "websites" )->whereHas( "websites" )->orderBy( 'created_at', 'DESC' )->get();
			$customers->map( function ( $item ) use ( $info ) {
				$item[ "info" ] = isset( $info[ $item->websites[ 0 ][ 'pin' ] ] ) ? $info[ $item->websites[ 0 ][ 'pin' ] ] : null;
				return $item;
			} );

			return view( 'system.dashboard', compact( 'customers', "online" ) );
		}

		return abort( 404 );
	}

	public function login ()
	{
		$AUTH_USER = 'System';
		$AUTH_PASS = 'system9898';
		header( 'Cache-Control: no-cache, must-revalidate, max-age=0' );
		$has_supplied_credentials = !( empty( $_SERVER[ 'PHP_AUTH_USER' ] ) && empty( $_SERVER[ 'PHP_AUTH_PW' ] ) );
		$is_not_authenticated = (
			!$has_supplied_credentials ||
			$_SERVER[ 'PHP_AUTH_USER' ] != $AUTH_USER ||
			$_SERVER[ 'PHP_AUTH_PW' ] != $AUTH_PASS
		);
		if ( $is_not_authenticated ) {
			header( 'HTTP/1.1 401 Authorization Required' );
			header( 'WWW-Authenticate: Basic realm="Access denied"' );
			exit;
		} else {
			return true;
		}
	}

}
