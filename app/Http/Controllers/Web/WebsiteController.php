<?php

namespace App\Http\Controllers\Web;

use App\Helpers\TenantManager;
use App\Models\Customer;
use App\Models\User;
use App\Models\Website;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hyn\Tenancy\Environment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class WebsiteController extends Controller
{
	public function switch ( Request $request, $pin )
	{
		$website = Website::pin( $pin );
		$user = auth()->user();
		$hash = Crypt::encrypt( $user->email . "%sep%" . $user->makeVisible( "password" )->password . "%sep%" . $pin );
		return redirect()->route( 'tenant.login', [ "tenant" => $website->host, "hash" => $hash ] );
	}

}
