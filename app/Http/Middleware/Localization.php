<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Config;
use Jenssegers\Date\Date;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Localization
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle ( $request, Closure $next )
	{
		$locale = auth( 'employee' )->user()->locale;
		$timezone = setting( 'timezone' );
		LaravelLocalization::setlocale( strtolower( $locale ) );
		Date::setLocale( $timezone );
		date_default_timezone_set( $timezone );
		setLocale(LC_TIME,$locale);
		Config::set( 'app.timezone', $timezone );
		Carbon::setLocale( $locale );
		return $next( $request );
	}
}
