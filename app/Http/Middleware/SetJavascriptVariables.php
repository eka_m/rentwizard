<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;
use App\Models\Deal;
use Closure;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SetJavascriptVariables
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */

	public function handle ( $request, Closure $next )
	{
		$user = auth( "employee" )->user();
		$user[ 'roles' ] = $user->roles;
		$user[ 'permissions' ] = $user->permissions;
		JavaScript::put( [
			'user' => $user,
			'tenant' => website(),
			'appStarts' => website()->created_at,
			'appCurrency' => setting( 'currency' ),
			'appLocale' => LaravelLocalization::getCurrentLocale(),
			'appTimezone' => setting( 'timezone' ),
		] );
		return $next( $request );
	}

}
