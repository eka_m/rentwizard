<?php

namespace App\Http\Middleware;

use Closure;
use DebugBar\DebugBar;
use Hyn\Tenancy\Environment;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class TenantHost
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle ( $request, Closure $next )
	{
		if ( $website = app( Environment::class )->tenant() ) {
			\URL::defaults( [ 'tenant' => $website->host ] );
			$request->route()->forgetParameter( 'tenant' );
		}
		return $next( $request );
	}
}
