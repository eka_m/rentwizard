<?php

namespace App\Jobs;

use App\Helpers\Time;
use App\Models\App;
use App\Models\Deal;
use App\Notifications\AppNotification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ActivateDeal implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


	protected $deal;
	protected $website;
	protected $dealjob;
	public $deleteWhenMissingModels = true;


	/**
	 * Create a new job instance.
	 *
	 * @param $deal
	 */
	public function __construct ( $job )
	{
		$this->dealjob = $job;
		$this->deal = $job->deal_id;
		$this->website = $job->website;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle ()
	{

		tenancy()->tenant( $this->website );
		Time::setTimezone( $this->website->timezone );
		$deal = Deal::where( 'id', $this->deal )
			->where( 'start', '<=', Carbon::now() )
			->where( 'status', 'Planned' )
			->with( 'items' )
			->first();

		if ( $deal ) {
			$deal->status = 'Active';
			if ( $deal->save() ) {
				foreach ( $deal->items as $item ) {
					$item->update( [ 'status' => 'Rented' ] );
				}
				$this->dealjob->activated = 1;
				$this->dealjob->save();
				activity( 'activated' )->log( $deal->hash );
				App::send( new AppNotification( "%Activated deal%" . ': ' . $$deal->hash ) );
			}
		}
	}
}
