<?php

namespace App\Jobs;

use App\Events\DeactivatedDeal;
use App\Helpers\Time;
use App\Models\App;
use App\Models\Deal;
use App\Models\DealJob;
use App\Models\Website;
use App\Notifications\AppNotification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeactivateDeal implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $deal;
	protected $website;
	protected $dealjob;
	public $deleteWhenMissingModels = true;


	/**
	 * Create a new job instance.
	 *
	 * @param $job
	 */
	public function __construct ( $job )
	{
		$this->dealjob = $job;
		$this->deal = $job->deal_id;
		$this->website = $job->website;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle ()
	{
		tenancy()->tenant( $this->website );
		Time::setTimezone( $this->website->timezone );
		$deal = Deal::where( 'id', $this->deal )
			->where( 'end', '<=', Carbon::now() )
			->where( 'status', 'Active' )
			->where( 'autoactivation', true )
			->first();

		if ( $deal ) {
			$deal->status = 'Expired';
			if ( $deal->save() ) {
				DealJob::find( $this->dealjob->id )->delete();
				activity( 'expired' )->log( $deal->hash );
				App::send( new AppNotification( "%Expired deal%" . ': ' . $deal->hash ) );
			}
		}
	}
}
