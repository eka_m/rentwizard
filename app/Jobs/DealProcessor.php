<?php

namespace App\Jobs;

use App\Helpers\Time;
use App\Models\App;
use App\Models\Deal;
use App\Models\DealJob;
use App\Notifications\AppNotification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;
use Jenssegers\Date\Date;

class DealProcessor implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $tries = 5;
	protected $jobs;

	/**
	 * Create a new job instance.
	 *
	 * @param $mustActivated
	 */
	public function __construct ( $jobs )
	{
		$this->jobs = $jobs;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle ()
	{
		foreach ( $this->jobs as $job ) {

			if ($job->activated) {
				tenancy()->tenant( $job->website );
				Time::setTimezone( $job->website->timezone );
				$deal = Deal::where( 'id', $job->deal_id )
					->where( 'end', '<=', Carbon::now() )
					->where( 'status', 'Active' )
					->with( 'items' )
					->first();
				if ( $deal ) {
					DeactivateDeal::dispatch( $deal );
				}

			} else {
				tenancy()->tenant( $job->website );
				Time::setTimezone( $job->website->timezone );
				$deal = Deal::where( 'id', $job->deal_id )
					->where( 'start', '<=', Carbon::now() )
					->where( 'status', 'Planned' )
					->where( 'autoactivation', true )
					->first();
				if ( $deal ) {
					ActivateDeal::dispatch( $deal );
					$job->activated = 1;
					$job->save();
				}
			}
		}
	}
}
