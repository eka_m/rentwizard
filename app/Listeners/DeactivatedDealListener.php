<?php

namespace App\Listeners;

use App\Events\DeactivatedDeal;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class DeactivatedDealListener
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  DeactivatedDeal $event
	 * @return void
	 */
	public function handle ( DeactivatedDeal $event )
	{
		Log::info( 'yes' );
		Log::info($event->website->id );
	}
}
