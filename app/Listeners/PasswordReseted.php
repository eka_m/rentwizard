<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class PasswordReseted
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  PasswordReset $event
	 * @return void
	 */
	public function handle ( PasswordReset $event )
	{
//		dd( $event->user );

//		foreach ( $event->user->websites as $website ) {
//			tenancy()->tenant( $website );
//			User::where( 'email', $event->user->email )->update( [ 'password' => $event->user->makeVisible( 'password' )->password ] );
//		}
	}
}
