<?php

namespace App\Models;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Spatie\Activitylog\Models\Activity as ActivityLog;

class Activity extends ActivityLog
{
	use UsesTenantConnection;
}
