<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class App extends Model
{
	use UsesTenantConnection, Notifiable, Cachable;
	protected $table = 'settings';

	protected $fillable = [ "key", "value" ];
	public $timestamps = false;

	public static function send ( $instance )
	{
		return self::getInstance()->notify( $instance );
	}

	public static function getNotifications ( $limit = null )
	{
		return self::getInstance()->notifications()->limit( $limit )->get();
	}

	public static function notification ( $id )
	{
		return self::getInstance()->notifications()->where( "id", "=", $id )->first();
	}

	public static function getInstance ()
	{
		return self::find( 1 );
	}

	public function receivesBroadcastNotificationsOn ()
	{
		$tenant = website()->id;
		return "app.notification.$this->id.$tenant";
	}


}
