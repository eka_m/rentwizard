<?php

namespace App\Models;

use Hyn\Tenancy\Traits\UsesTenantConnection;

class Attribute extends Base
{
	use UsesTenantConnection;
	protected $fillable = [ 'name', 'slug', 'type', 'in_table', 'in_search' ];

	protected $casts = [
		'in_table' => 'boolean',
		'in_search' => 'boolean',
	];

	public function setSlugAttribute ()
	{
		$this->attributes[ 'slug' ] = str_slug( $this->name );
	}

	public function getSlugAttribute ()
	{
		return str_slug( $this->name );
	}

	public function scopeForTable ( $query )
	{
		return $query->where( 'in_table', 1 );
	}
	public function scopeForSearch ( $query )
	{
		return $query->where( 'in_search', 1 );
	}
}
