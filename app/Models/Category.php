<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Category extends Base
{
	use UsesTenantConnection;
//	use Cachable;

	protected $fillable = [ 'name', 'slug' ];

	public function items ()
	{
		return $this->hasMany( Inventory::class );
	}
}
