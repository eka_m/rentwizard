<?php

namespace App\Models;

use App\Helpers\Media\HasMediaTrait;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Client extends Base
{
	use UsesTenantConnection;
//	use Cachable;
	use SoftDeletes,
		HasMediaTrait,
		HasRelationships;

	public static $statuses = [ "VIP", "Reliable", "Undefined", "Blocked", "Unreliable", "Employee" ];

	protected $fillable = [ 'client_id', 'name', 'phone', 'email', 'address', 'passport_number', 'description', 'slug', 'status', 'attributes', 'is_notifiable' ];

	protected $appends = [ 'thumb' ];

	public function contactface ()
	{
		return $this->hasOne( 'App\Models\Client', 'id', 'client_id' );
	}

	public function deals ()
	{
		return $this->hasMany( Deal::class );
	}

	public function inventory ()
	{
		return $this->hasManyDeep( 'App\Models\Inventory', [ 'App\Models\Deal', 'deal_inventory' ] );
	}

	public function getThumbAttribute ()
	{
		$main = $this->media()->where( 'properties->main', true )->first();
		return $main ? rtrim( $main->url, basename( $main->url ) ) . 'thumbs/thumb_' . basename( $main->url ) : null;
	}

	public function scopeSearch ( $query, $field = null )
	{
		if ( $field === null ) return $query;
		return $query->where( function ( $query ) use ( $field ) {
			$query->where( 'id', 'like', "%$field%" )
				->orWhere( 'name', 'like', "%$field%" )
				->orWhere('phone', 'like', "%$field%")
				->orWhere('email', 'like', "%$field%")
				->orWhere('passport_number', 'like', "%$field%")
				->orWhere( "attributes", "like", "%$field%" );
		} );
	}

	public function scopeStatus ( $query, $status = null )
	{
		return $status ? $query->where( "status", $status ) : $query;
	}

	public static function getStatuses ()
	{
		return self::$statuses;
	}
}
