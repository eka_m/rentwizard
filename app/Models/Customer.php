<?php

namespace App\Models;


use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Customer extends Authenticatable implements MustVerifyEmail
{
	use UsesSystemConnection;
	use Notifiable;

	protected $table = 'customers';

	protected $fillable = [
		'name', 'email', 'phone', 'company', 'pin', 'locale', 'password', 'params', 'percent',
	];

	protected $casts = [ 'params' => 'array', 'email_verified_at' => 'datetime', ];


	protected $hidden = [
		'password', 'remember_token',
	];

	public function websites ()
	{
		return $this->hasMany( Website::class );
	}

//	public function hostnames ()
//	{
//		return $this->belongsToMany( Hostname::class, 'customer_hostname' );
//	}

}
