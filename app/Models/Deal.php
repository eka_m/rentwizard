<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deal extends Base
{
	use UsesTenantConnection;
//	use Cachable;
	use SoftDeletes;

	protected $fillable = [
		'start',
		'end',
		'paid_at',
		'hash',
		'client_id',
		'status',
		'description',
		'params',
		'price',
		'realprice',
		'sale',
		'manager_id',
		'manager_profit',
		'manager_profit_type',
		'autoactivation'
	];

	public static $statuses = [ 'Active', 'Planned', 'Completed', 'Expired', 'Not paid' ];

	protected $appends = [ 'statuses', 'cost', 'realcost' ];


	/** RELATIONS */

	public function items ()
	{
		return $this->belongsToMany( Inventory::class )
			->withTimestamps()
			->withPivot( 'deleted_at' )
			->whereNull( 'deal_inventory.deleted_at' );
	}

	public function client ()
	{
		return $this->belongsTo( Client::class )->withDefault( function () {
			$client = new Client();
			$client->id = 0;
			$client->name = __( "Missing" );
			$client->phone = __( "Not specified" );
			$client->stats = "Undefined";
			$client->adress = __( "Not specified" );
			return $client;
		} );;
	}

	public function manager ()
	{
		return $this->belongsTo( User::class );
	}

	/** END REALATIONS */

	/** SCOPES */

	public function scopeWithoutStaff ( $query )
	{
		return $query->whereDoesntHave( 'client', function ( $query ) {
			$query->where( 'status', 'Employee' );
		} );
	}

	public function scopeManager ( $query, $id )
	{
		return $id ? $query->where( 'manager_id', $id ) : $query;
	}

	public function scopeOnlyClosed ( $query, $is = true )
	{
		return $is ? $query->where( 'status', 'Completed' )->orWhere( 'status', 'Not paid' ) : $query;
	}

	public function scopeInProcess ( $query )
	{
		return $query->whereNull( 'paid_at' );
	}

	public function scopeOnlyPaid ( $query )
	{
		return $query->where( 'status', 'Completed' );
	}

	public function scopeOnlyNotPaid ( $query )
	{
		return $query->where( 'status', 'Not paid' );
	}

	public function scopeStatus ( $query, $status )
	{
		return $status ? $query->where( 'status', $status ) : $query;
	}

	public function scopeOrStatus ( $query, $status = null )
	{
		return $query->orWhere( 'status', $status );
	}

	public function scopeSearch ( $query, $field = null )
	{
		if ( $field === null ) return $query;
		return $query->where( function ( $query ) use ( $field ) {
			$query->where( 'hash', 'like', "%$field%" )
				->orWhere( 'id', 'like', "%$field%" )
				->orWhereHas( "client", function ( $query ) use ( $field ) {
					return $query->where( 'id', 'like', "%$field%" )
						->orWhere( 'name', 'like', "%$field%" )
						->orWhere( 'phone', 'like', "%$field%" )
						->orWhere( 'email', 'like', "%$field%" )
						->orWhere( 'passport_number', 'like', "%$field%" );
				} );
		} );
	}


	/** END SCOPES */

	/** APPENDS */
	public function getCostAttribute ()
	{
		return round( $this->price, 2 ) . ' ' . html_entity_decode( setting( 'currency.symbol' ) );
	}

	public function getRealcostAttribute ()
	{
		return round( $this->realprice, 2 ) . ' ' . html_entity_decode( setting( 'currency.symbol' ) );
	}

	public function getStatusesAttribute ()
	{
		return self::getStatuses();
	}
	/** END APPENDS */


	/** HELPERS */

	public static function startDate ()
	{
		$start = self::min( 'start' );
		return $start ? $start : "01.01.2018";
	}

	public static function getStatuses ()
	{
		return self::$statuses;
	}

	/** END HELPERS */

}
