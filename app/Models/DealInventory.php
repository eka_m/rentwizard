<?php

namespace App\Models;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealInventory extends Base
{
	use UsesTenantConnection;
	use SoftDeletes;
	protected $table = 'deal_inventory';
	protected $fillable = [ 'deal_id', 'inventory_id', 'deleted_at' ];

	public static function boot ()
	{
		parent::boot();
		self::deleted( function ( $model ) {
			$model->inventory()->update( [ "status" => "onRent" ] );
		} );
	}

	public function deal ()
	{
		return $this->hasOne( Deal::class );
	}

	public function inventory ()
	{
		return $this->hasOne( Inventory::class );
	}
}
