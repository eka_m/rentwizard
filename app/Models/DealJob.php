<?php

namespace App\Models;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;

class DealJob extends Model
{

	use UsesSystemConnection;

	protected $table = 'deal_jobs';

	protected $fillable = [ 'website_id', 'deal_id', 'start', 'end', 'activated' ];

	public function website ()
	{
		return $this->belongsTo( Website::class );
	}

	public static function deal ( $id )
	{
		return self::where( 'deal_id', $id )->first();
	}

	public static function add ( $deal, $website = null )
	{
		if ( $deal->status != "Expired" ) {
			return self::create( self::make( $deal, $deal->status == "Active" ? 1 : 0, $website ) );
		}
	}

	public static function change ( $deal, $start, $end, $activated, $website = null )
	{
		$result = self::make( $deal, $activated, $website );
		self::remove( $result[ 'deal_id' ] );
		$result[ 'start' ] = $start;
		$result[ 'end' ] = $end;
		return self::create( $result );
	}

	public static function remove ( $id )
	{
		if ( $job = self::deal( $id ) ) {
			return $job->delete();
		}
		return false;
	}

	private static function make ( $deal, $activated, $website )
	{
		if ( is_null( $website ) ) $website = website();
		if ( is_array( $deal ) ) {
			$dealID = $deal[ 'id' ];
			$dealStart = $deal[ 'start' ];
			$dealEnd = $deal[ 'end' ];
		} else if ( is_object( $deal ) ) {
			$dealID = $deal->id;
			$dealStart = $deal->start;
			$dealEnd = $deal->end;
		} else {
			$dealID = $deal;
			$dealStart = null;
			$dealEnd = null;
		}
		return [ 'deal_id' => $dealID, 'start' => $dealStart, 'end' => $dealEnd, 'website_id' => $website->id, 'activated' => $activated ];
	}
}
