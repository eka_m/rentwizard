<?php

namespace App\Models;
use Hyn\Tenancy\Models\Hostname as TenantHostname;

class Hostname extends TenantHostname
{
	protected $table = 'hostnames';

	public function users ()
	{
		return $this->belongsToMany( Customer::class );
	}
}
