<?php

namespace App\Models;

use Carbon\Carbon;
//use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Media\HasMediaTrait;
use Vinkla\Hashids\Facades\Hashids;

class Inventory extends Base
{
	use UsesTenantConnection;
	use HasMediaTrait;
	use Cachable;
	use SoftDeletes;
	protected $table = 'inventory';
	protected $appends = [ 'thumb', 'cost' ];
	protected $fillable = [
		'category_id',
		'name',
		'slug',
		'photos',
		'state',
		'status',
		'attributes',
		'description',
		'rent_price',
		'rent_per',
		'rent_count',
		'total_profit',
		'purchase_price',
		'purchase_date',
	];
	protected $casts = [
		'attributes' => 'array'
	];

	public static $statuses = [ 'Available', 'Reserved', 'Rented', 'Missing', "In repair" ];

	public static $states = [ "New", "Average", "Bad" ];


	public function category ()
	{
		return $this->belongsTo( 'App\Models\Category' );
	}

	/** RELATIONS */

	public function deals ()
	{
		return $this->belongsToMany( 'App\Models\Deal' )
			->withTimestamps()
			->withPivot( 'deleted_at' )
			->whereNull( 'deal_inventory.deleted_at' );
	}


	public function clients ()
	{
		return $this->hasManyThrought( 'App\Models\Client', 'App\Models\Deal' );
	}

	public function allDeals ()
	{
		return $this->belongsToMany( 'App\Models\Deal' );
	}

	/** END RELATIONS */

	/** SCOPES */

	public function scopeWithoutActiveDeals ( $query, $start, $end )
	{
		$start = Carbon::parse( $start );
		$end = Carbon::parse( $end );
		return $query->whereDoesntHave( 'deals', function ( $query ) use ( $start, $end ) {
			$query->whereNull( 'paid_at' )->overlapping( $start, $end );
		} );
	}

	public function scopeSearch ( $query, $field = null )
	{
		if ( $field === null ) return $query;
		return $query->where( function ( $query ) use ( $field ) {
			$query->where( 'name', 'like', "%$field%" )
				->orWhere( 'id', 'like', "%$field%" )
				->orWhere( "attributes", "like", "%$field%" );
		} );
	}

	public function scopeCategory ( $query, $id = null )
	{
		return $id ? $query->where( "category_id", $id ) : $query;
	}

	public function scopeStatus ( $query, $status = null )
	{
		return $status ? $query->where( "status", $status ) : $query;
	}

	public function scopeWithoutDeal ( $query, $id )
	{
		if ( $id ) {
			return $query->orWhereHas( 'deals', function ( $query ) use ( $id ) {
				$query->where( "deals.id", $id );
			} );
		}
		return $query;
	}

	public function scopeQR ( $query, $qr )
	{
		$id = Hashids::decode( explode( '@', $qr )[ 0 ] );
		return $query->where( 'id', $id );
	}

	/** END SCOPES */

	/** ACCESSORS */

	public function getThumbAttribute ()
	{
		$main = $this->media()->where( 'properties->main', true )->first();
		return $main ? rtrim( $main->url, basename( $main->url ) ) . 'thumbs/thumb_' . basename( $main->url ) : null;
	}

	public function getCostAttribute ()
	{
		return round( $this->rent_price, 2 ) . ' ' . html_entity_decode( setting( 'currency.symbol' ) ) . ' / ' . __( $this->rent_per );
	}

//	public function getQrAttribute ()
//	{
//		$id = Hashids::encode( $this->id );
//		$created = Hashids::encode( Carbon::parse( $this->created_at )->timestamp );
//		return $id . "@" . $created;
//	}
	/** END ACCESSORS*/

	/** MUTATORS */

	public function setRentData ( $deal )
	{

		$dealHours = Carbon::parse( $deal->start )->diffInHours( $deal->end, false );

		$itemPriceInHour = $this->rent_per === 'day' ? $this->rent_price / 24 : $this->rent_price;

		$profit = round( $itemPriceInHour * $dealHours, 1 );

		$finishProfit = $deal->sale > 0 ? round( ( $profit - $profit / 100 * $deal->sale ), 1 ) : $profit;

		$this->total_profit = $this->total_profit + $finishProfit;
		$this->rent_count = $this->rent_count + 1;

		$this->save();
		return $this;
	}

	/** END MUTATORS */


	/** HELPERS */
	public static function setStatus ( $dealstatus )
	{
		switch ( $dealstatus ) {
			case "Active":
			case "Expired":
				return 'Rented';
				break;
			case "Completed":
			case "Not paid":
				return 'Available';
				break;
			case "Planned":
				return "Reserved";
				break;
		}
	}

	public static function getStatuses ()
	{
		return self::$statuses;
	}

	public static function getStates ()
	{
		return self::$states;
	}

	public static function generateQrCrypt ( $inventory )
	{

	}
	/** END HELPERS */
}
