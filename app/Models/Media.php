<?php

namespace App\Models;

use Spatie\MediaLibrary\Models\Media as BaseMedia;
use Hyn\Tenancy\Traits\UsesTenantConnection;
class Media extends BaseMedia
{
	use UsesTenantConnection;
	protected $appends = [ 'dataURL' ];

	public function getDataURLAttribute ()
	{
		return $this->getUrl();
	}

	public function updateOrAddMedia ()
	{

	}
}
