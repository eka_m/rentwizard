<?php

namespace App\Models;

use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Database\Eloquent\Model;

class OnlineTenants extends Model
{
	use UsesSystemConnection;

	protected $guarded = [];

}
