<?php

namespace App\Models;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
	use UsesTenantConnection;

	public function scopeWithoutRoot ( $query )
	{
		return $query->where('name', '!=', 'root');
	}
}