<?php

namespace App\Models;


use App\Helpers\Media\HasMediaTrait;
use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;


class User extends Authenticatable implements MustVerifyEmail
{
	use UsesTenantConnection;
	use HasRoles;
	use Notifiable;
	use HasMediaTrait;

	protected $guard_name = "employee";

	protected $fillable = [
		'name', 'email', 'phone', 'company', 'pin', 'locale', 'password', 'params', 'percent',
	];

	protected $casts = [ 'params' => 'array'];


	protected $hidden = [
		'password', 'remember_token',
	];

	public function scopeWithoutRoot ( $query )
	{
		return $query->whereDoesntHave( "roles", function ( $query ) {
			return $query->where( "name", 'root' );
		} );
	}

	public function deals ()
	{
		return $this->hasMany( Deal::class, 'manager_id', 'id' );
	}


	public function avatar ()
	{
		return !empty( $this->media ) && isset( $this->media->first()->url ) ? $this->media->first()->url : null;
	}

}
