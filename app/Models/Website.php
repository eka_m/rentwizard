<?php

namespace App\Models;

use Hyn\Tenancy\Models\Website as TenantWebsite;
use Hyn\Tenancy\Traits\UsesSystemConnection;
use Illuminate\Notifications\Notifiable;

class Website extends TenantWebsite
{
	use Notifiable;

	use UsesSystemConnection;
	protected $table = 'websites';

	protected $guarded = [];
	protected $appends = [ "host", "prettypin" ];

	public function customer ()
	{
		return $this->belongsTo( Customer::class );
	}

	public static function scopeMain ( $query )
	{
		return $query->where( "id", 1 )->first();
	}

	public static function scopePin ( $query, $pin )
	{
		return $query->where( "pin", str_replace( "-", "", $pin ) )->first();
	}

	public static function scopeTenantExists ( $query, $pin )
	{
		return $query->where( "pin", str_replace( "-", "", $pin ) )->exists();
	}

	public function getPrettyPinAttribute ()
	{
		return substr_replace( $this->attributes[ "pin" ], '-', 3, 0 );
	}

	public function getHostAttribute ()
	{
		return explode( "_", $this->attributes[ "uuid" ] )[ 0 ];
	}
}
