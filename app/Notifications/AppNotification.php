<?php

namespace App\Notifications;

use App\Models\App;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class AppNotification extends Notification
{
	use Queueable;
	public $notification;
	public $state;
	public $params;

	public function __construct ( $notification, $params = null, $state = 'info' )
	{
		$this->notification = $notification;
		$this->params = $params;
		$this->state = $state;
	}

	public function via ( $notifiable )
	{
		return [ 'database', 'broadcast' ];
	}

	public function toArray ( $notifiable )
	{
		return [
			"state" => $this->state,
			"message" => $this->notification,
			"params" => $this->params
		];
	}

	public function toBroadcast ( $notifiable )
	{
		return App::getInstance()->notifications()->latest()->first()->toArray();
	}

}
