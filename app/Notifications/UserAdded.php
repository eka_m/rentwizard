<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserAdded extends Notification
{
	use Queueable;

	private $password;
	private $pin;
	private $role;

	/**
	 * Create a new notification instance.
	 * @param $password
	 * @param $role
	 * @param $pin
	 */
	public function __construct ( $password, $role, $pin )
	{
		$this->password = $password;
		$this->role = $role;
		$this->pin = $pin;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function via ( $notifiable )
	{
		return [ 'mail' ];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail ( $notifiable )
	{
		return ( new MailMessage )
			->line( __( 'system.messages.user_added', [ 'role' => mb_strtolower(__( $this->role )) ] ) )
			->line( __( "Username" ) . ': ' . $notifiable->email )
			->line( __( "Password" ) . ': ' . $this->password )
			->line( __( "Company pin" ) . ': ' . $this->pin )
			->action( __( 'Sign in' ), route('login') )
			->line( __('Thank you for using our application!') );
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed $notifiable
	 * @return array
	 */
	public function toArray ( $notifiable )
	{
		return [
			//
		];
	}
}
