<?php

namespace App\Observers;

use App\Models\Deal;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Vinkla\Hashids\Facades\Hashids;

class DealObserver
{
	/**
	 * Handle the deal "creating" event.
	 *
	 * @param  \App\Models\Deal $deal
	 * @return void
	 */
	public function creating ( Deal $deal )
	{
		$hash = Hashids::encode( time() );
		$deal->hash = $hash;
		$deal->manager_id = auth( 'employee' )->user()->id;
		Artisan::call( "modelCache:clear" );
	}

	/**
	 * Handle the deal "created" event.
	 *
	 * @param  \App\Models\Deal $deal
	 * @return void
	 */
	public function created ( Deal $deal )
	{
		//
	}

	/**
	 * Handle the deal "updated" event.
	 *
	 * @param  \App\Models\Deal $deal
	 * @return void
	 */
	public function updated ( Deal $deal )
	{
		$oldStatus = $deal->getOriginal( 'status' );
		$status = $deal->status;
		if ( $oldStatus == 'Active' || $oldStatus == 'Expired' && $status == 'Completed' || $status == 'Not paid' ) {
			if ( $deal->client->status !== "Employee" ) {
				foreach ( $deal->items as $item ) {
					$item->setRentData( $deal );
				}
			}
		}
		Artisan::call( "modelCache:clear" );
	}

	public function saving ( Deal $deal )
	{
		//TODO Перенеести функцию "setDealStatus" сюда
		if ( auth( 'employee' )->check() ) {
			$deal->manager_profit = $deal->manager_profit_type === 'percent' ? auth( 'employee' )->user()->percent : $deal->manager_profit;
		}
		Artisan::call( "modelCache:clear" );
	}

	/**
	 * Handle the deal "deleted" event.
	 *
	 * @param  \App\Models\Deal $deal
	 * @return void
	 */
	public function deleted ( Deal $deal )
	{
		Artisan::call( "modelCache:clear" );
	}

	/**
	 * Handle the deal "restored" event.
	 *
	 * @param  \App\Models\Deal $deal
	 * @return void
	 */
	public function restored ( Deal $deal )
	{
		//
	}

	/**
	 * Handle the deal "force deleted" event.
	 *
	 * @param  \App\Models\Deal $deal
	 * @return void
	 */
	public function forceDeleted ( Deal $deal )
	{
		//
	}
}
