<?php

namespace App\Providers;

use App\Models\Deal;
use App\Observers\DealObserver;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot ()
	{
		$this->initPastFutureMacro();
		$this->initObservers();
		Blade::component( 'components.fixed_head_portlet', 'fxportlet' );
		Blade::component( 'components.portlet', 'portlet' );
		Blade::component( 'components.card', 'card' );
		Blade::component( 'components.loader', 'load' );
		Blade::directive('svg', function($arguments) {
			// Funky madness to accept multiple arguments into the directive
			list($path, $class) = array_pad(explode(',', trim($arguments, "() ")), 2, '');
			$path = trim($path, "' ");
			$class = trim($class, "' ");

			// Create the dom document as per the other answers
			$svg = new \DOMDocument();
			$svg->load(public_path($path));
			$svg->documentElement->setAttribute("class", $class);
			$output = $svg->saveXML($svg->documentElement);

			return $output;
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register ()
	{
		//
	}

	private function initPastFutureMacro ()
	{
		Builder::macro( 'past', function ( $column, $when = 'now', $strict = false ) {
			$when = Carbon::parse( $when );
			$operator = $strict ? '<' : '<=';
			return $this->where( $column, $operator, $when );
		} );
		Builder::macro( 'future', function ( $column, $when = 'now', $strict = false ) {
			$when = Carbon::parse( $when );
			$operator = $strict ? '>' : '>=';
			return $this->where( $column, $operator, $when );
		} );
	}

	private function initObservers ()
	{
		Deal::observe( DealObserver::class );
	}

}
