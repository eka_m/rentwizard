<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class LocalizationServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot ()
	{
		LaravelLocalization::setLocale( setting( 'localization.current' ) );
		Date::setLocale( setting( 'localization.current' ) );


		config( [ 'app.timezone' => setting( 'localization.timezone' ) ] );
	}

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register ()
	{
		config( [
			'laravellocalization.supportedLocales' => [
				'en' => [ 'name' => 'English', 'script' => 'Latn', 'native' => 'English', 'regional' => 'en_GB' ],
				'az' => [ 'name' => 'Azerbaijani', 'script' => 'Latn', 'native' => 'azərbaycanca', 'regional' => 'az_AZ' ],
				'ru' => [ 'name' => 'Russian', 'script' => 'Cyrl', 'native' => 'русский', 'regional' => 'ru_RU' ],
			],
			'laravellocalization.useAcceptLanguageHeader' => true,
			'laravellocalization.hideDefaultLocaleInURL' => true,
			'urlsIgnored' => [ '/skipped' ]
		] );
	}
}
