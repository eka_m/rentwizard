<?php

namespace App\Rules;

use App\Models\Website;
use Illuminate\Contracts\Validation\Rule;

class Pin implements Rule
{

	private $message = 'validation.pin.type';

	/**
	 * Create a new rule instance.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		//
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param  string $attribute
	 * @param  mixed $value
	 * @return bool
	 */
	public function passes ( $attribute, $value )
	{
		if ( $value == null || $value == '' ) return true;
		$pin = str_replace( "-", "", $value );
		$result = is_numeric( $pin ) && $pin >= 000000 && $pin <= 999999;
		if ( !$result ) return false;
		if ( !Website::tenantExists( $pin ) ) {
			$this->message = 'validation.pin.notexists';
			return false;
		}
		return true;
	}


	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message ()
	{
		return trans( $this->message );
	}
}
