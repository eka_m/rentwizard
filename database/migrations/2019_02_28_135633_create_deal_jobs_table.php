<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealJobsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create( 'deal_jobs', function ( Blueprint $table ) {
			$table->bigIncrements( 'id' );
			$table->integer( 'website_id' );
			$table->integer( 'deal_id' );
			$table->dateTime( 'start' );
			$table->dateTime( 'end' );
			$table->boolean( 'activated' )->default( false );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::dropIfExists( 'deal_jobs' );
	}
}
