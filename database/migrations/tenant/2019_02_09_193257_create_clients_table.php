<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create( 'clients', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'client_id' )->nullable();
			$table->string( 'name' );
			$table->string( 'phone' )->nullable();
			$table->string( 'email' )->nullable();
			$table->string( 'address' )->nullable();
			$table->string( 'passport_number' )->nullable();
			$table->text( 'description' )->nullable();
			$table->string( 'status' )->default( 'Undefined' );
			$table->string( 'slug' )->nullable();
			$table->json( 'attributes' )->nullable();
			$table->boolean( 'is_notifiable' )->default( true );
			$table->softDeletes();
			$table->timestamps();
		} );
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop( 'clients' );
	}

}
