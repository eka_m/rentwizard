<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDealsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->unsigned();
			$table->string('hash')->nullable();
			$table->string('status')->default('Planned');
			$table->string('price')->default('0');
			$table->string('realprice')->nullable();
			$table->string('sale')->default('0');
			$table->text('description')->nullable();
			$table->dateTime('start')->nullable();
			$table->dateTime('end')->nullable();
			$table->dateTime('paid_at')->nullable();
			$table->boolean('autoactivation')->default(1);
			$table->integer('manager_id');
			$table->integer('manager_profit')->nullable();
			$table->string('manager_profit_type')->nullable()->default('percent');
			$table->text('params')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deals');
	}

}
