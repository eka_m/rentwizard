<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventory', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('category_id')->default(0);
			$table->string('name');
			$table->string('slug')->nullable();
			$table->text('photos')->nullable();
			$table->string('status')->nullable()->default('available');
			$table->string('state')->nullable()->default('new');
			$table->text('description')->nullable();
			$table->float('rent_price')->unsigned()->default(0);
			$table->integer('rent_count')->unsigned()->nullable()->default(0);
			$table->string('rent_per', 5)->nullable()->default('hour');
			$table->integer('total_profit')->unsigned()->nullable()->default(0);
			$table->float('purchase_price')->unsigned()->nullable()->default(0);
			$table->date('purchase_date')->nullable();
			$table->text('purchase')->nullable();
			$table->longText('attributes')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventory');
	}

}
