<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('model_type')->nullable();
			$table->bigInteger('model_id')->nullable();
			$table->string('collection')->nullable();
			$table->string('original_name')->nullable();
			$table->string('file_name')->default('');
			$table->string('url')->nullable();
			$table->string('mime_type')->nullable();
			$table->string('disk')->nullable();
			$table->integer('size')->unsigned()->nullable();
			$table->text('manipulations')->nullable();
			$table->text('properties')->nullable();
			$table->integer('order')->unsigned()->nullable();
			$table->dateTime('tmp')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media');
	}

}
