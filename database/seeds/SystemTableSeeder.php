<?php

use Illuminate\Database\Seeder;

class SystemTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{
		DB::table( 'websites' )->insert( [
			'uuid' => 'system',
			"pin" => 0,
			"customer_id" => 1
		] );

		DB::table( 'hostnames' )->insert( [
			'fqdn' => 'rent-wizard.com',
			'website_id' => 1
		] );
	}
}
