<?php

use Illuminate\Database\Seeder;

class TenantDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{
		DB::table( 'permissions' )->insert( [
			[ "name" => 'View stats', "guard_name" => "employee" ],
			[ "name" => 'View own stats', "guard_name" => "employee" ],
			[ "name" => 'View stats calendar', "guard_name" => "employee" ],
			[ "name" => 'View deals', "guard_name" => "employee" ],
			[ "name" => 'Create deals', "guard_name" => "employee" ],
			[ "name" => 'Edit deals', "guard_name" => "employee" ],
			[ "name" => 'Delete deals', "guard_name" => "employee" ],
			[ "name" => 'Close deals', "guard_name" => "employee" ],
			[ "name" => 'View inventory', "guard_name" => "employee" ],
			[ "name" => 'Create inventory', "guard_name" => "employee" ],
			[ "name" => 'Edit inventory', "guard_name" => "employee" ],
			[ "name" => 'Delete inventory', "guard_name" => "employee" ],
			[ "name" => 'Create attributes', "guard_name" => "employee" ],
			[ "name" => 'Edit attributes', "guard_name" => "employee" ],
			[ "name" => 'Delete attributes', "guard_name" => "employee" ],
			[ "name" => 'Create categories', "guard_name" => "employee" ],
			[ "name" => 'Edit categories', "guard_name" => "employee" ],
			[ "name" => 'Delete categories', "guard_name" => "employee" ],
			[ "name" => 'View clients', "guard_name" => "employee" ],
			[ "name" => 'Edit clients', "guard_name" => "employee" ],
			[ "name" => 'Update clients', "guard_name" => "employee" ],
			[ "name" => 'Delete clients', "guard_name" => "employee" ],
			[ "name" => 'View users', "guard_name" => "employee" ],
			[ "name" => 'Create users', "guard_name" => "employee" ],
			[ "name" => 'Update users', "guard_name" => "employee" ],
			[ "name" => 'Delete users', "guard_name" => "employee" ],
			[ "name" => 'Edit settings', "guard_name" => "employee" ],
			[ "name" => 'View logs', "guard_name" => "employee" ],
			[ "name" => 'Delete logs', "guard_name" => "employee" ],
		] );
		DB::table( 'roles' )->insert( [
			[ "guard_name" => "employee", "name" => "root" ],
			[ "guard_name" => "employee", "name" => "Admin" ],
			[ "guard_name" => "employee", "name" => "Moderator" ],
			[ "guard_name" => "employee", "name" => "Manager" ] ] );
	}
}
