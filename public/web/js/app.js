const phoneCodes = require( "./mask/country_codes.json" );
(function ( $ ) {
	"use strict";



	//Hide Loading Box (Preloader)
	function handlePreloader () {
		if ( $( '.preloader' ).length ) {
			$( '.preloader' ).delay( 200 ).fadeOut( 500 );
		}
	}


	//Update Header Style and Scroll to Top
	function headerStyle () {
		if ( $( '.main-header' ).length ) {
			var windowpos = $( window ).scrollTop();
			var siteHeader = $( '.main-header' );
			var scrollLink = $( '.scroll-to-top' );
			if ( windowpos >= 1 ) {
				siteHeader.addClass( 'fixed-header' );
				scrollLink.fadeIn( 300 );
			} else {
				siteHeader.removeClass( 'fixed-header' );
				scrollLink.fadeOut( 300 );
			}
		}
	}

	headerStyle();


	//Submenu Dropdown Toggle
	if ( $( '.main-header li.dropdown ul' ).length ) {
		$( '.main-header li.dropdown' ).append( '<div class="dropdown-btn"><span class="fa fa-angle-down"></span></div>' );

		//Dropdown Button
		$( '.main-header li.dropdown .dropdown-btn' ).on( 'click', function () {
			$( this ).prev( 'ul' ).slideToggle( 500 );
		} );

		//Disable dropdown parent link
		$( '.main-header .navigation li.dropdown > a,.hidden-bar .side-menu li.dropdown > a' ).on( 'click', function ( e ) {
			e.preventDefault();
		} );
	}


	//Add One Page nav
	if ( $( '.scroll-nav' ).length ) {
		$( '.scroll-nav ul' ).onePageNav();
	}

	//Hide Bootstrap Menu On Click over Mobile View
	$( '.scroll-nav ul.navigation > li > a' ).on( 'click', function () {
		var windowWidth = $( window ).width();
		if ( windowWidth <= 767 ) {
			$( '.nav-outer .navbar-toggler' ).trigger( "click" );
		}
	} );


	if ( $( '#master-slider' ).length ) {
		var sliderMaster = new MasterSlider();

		sliderMaster.control( 'arrows' );
		sliderMaster.control( 'circletimer', { color: "#FFFFFF", stroke: 9 } );

		sliderMaster.setup( 'master-slider', {
			width: 570,
			height: 340,
			space: 0,
			loop: true,
			view: 'partialWave',
			layout: 'partialview'
		} );
	}


	//Main Slider Carousel
	if ( $( '.main-slider-carousel' ).length ) {
		$( '.main-slider-carousel' ).owlCarousel( {
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			loop: false,
			margin: 0,
			nav: false,
			autoHeight: true,
			smartSpeed: 500,
			autoplay: false,
			navText: [ '<span class="flaticon-left-arrow"></span>', '<span class="flaticon-right-arrow"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				800: {
					items: 1
				},
				1024: {
					items: 1
				},
				1200: {
					items: 1
				}
			}
		} );
	}

	$( ".sc-nav" ).click( function ( e ) {
		e.preventDefault();
		$( [ document.documentElement, document.body ] ).animate( {
			scrollTop: $( "#features" ).offset().top
		}, 1000 );
	} );
	//Two Item Carousel
	if ( $( '.two-item-carousel' ).length ) {
		$( '.two-item-carousel' ).owlCarousel( {
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			loop: true,
			margin: 120,
			nav: true,
			autoHeight: true,
			smartSpeed: 500,
			autoplay: 6000,
			navText: [ '<span class="flaticon-left-arrow"></span>', '<span class="flaticon-right-arrow"></span>' ],
			responsive: {
				0: {
					items: 1,
					margin: 30
				},
				600: {
					items: 1,
					margin: 30
				},
				800: {
					items: 2,
					margin: 30
				},
				1024: {
					items: 2,
					margin: 30
				},
				1200: {
					items: 2
				}
			}
		} );
	}


	// Testimonial Carousel
	if ( $( '.testimonial-carousel' ).length ) {
		$( '.testimonial-carousel' ).owlCarousel( {
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			loop: true,
			margin: 0,
			nav: true,
			autoHeight: true,
			smartSpeed: 500,
			autoplay: 6000,
			navText: [ '<span class="flaticon-left-arrow"></span>', '<span class="flaticon-right-arrow"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				800: {
					items: 2
				},
				1024: {
					items: 2
				},
				1200: {
					items: 2
				}
			}
		} );
	}


	//Tabs Box
	if ( $( '.tabs-box' ).length ) {
		$( '.tabs-box .tab-buttons .tab-btn' ).on( 'click', function ( e ) {
			e.preventDefault();
			var target = $( $( this ).attr( 'data-tab' ) );

			if ( $( target ).is( ':visible' ) ) {
				return false;
			} else {
				target.parents( '.tabs-box' ).find( '.tab-buttons' ).find( '.tab-btn' ).removeClass( 'active-btn' );
				$( this ).addClass( 'active-btn' );
				target.parents( '.tabs-box' ).find( '.tabs-content' ).find( '.tab' ).fadeOut( 0 );
				target.parents( '.tabs-box' ).find( '.tabs-content' ).find( '.tab' ).removeClass( 'active-tab' );
				$( target ).fadeIn( 300 );
				$( target ).addClass( 'active-tab' );
			}
		} );
	}


	// Single Item Carousel
	if ( $( '.single-item-carousel' ).length ) {
		$( '.single-item-carousel' ).owlCarousel( {
			loop: true,
			margin: 0,
			nav: true,
			smartSpeed: 500,
			autoplay: 5000,
			navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				800: {
					items: 1
				},
				1024: {
					items: 1
				},
				1200: {
					items: 1
				}
			}
		} );
	}


	// Sponsors Carousel
	if ( $( '.sponsors-carousel' ).length ) {
		$( '.sponsors-carousel' ).owlCarousel( {
			loop: true,
			margin: 30,
			nav: true,
			smartSpeed: 500,
			autoplay: 4000,
			navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 2,
					margin: 30
				},
				600: {
					items: 2,
					margin: 30
				},
				800: {
					items: 4,
					margin: 30
				},
				1024: {
					items: 4
				}
			}
		} );
	}


	// Sponsors Carousel
	if ( $( '.sponsors-carousel-two' ).length ) {
		$( '.sponsors-carousel-two' ).owlCarousel( {
			loop: true,
			margin: 0,
			nav: true,
			smartSpeed: 500,
			autoplay: 4000,
			navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 2
				},
				600: {
					items: 2
				},
				800: {
					items: 3
				},
				1024: {
					items: 3
				}
			}
		} );
	}


	// Three Item Carousel
	if ( $( '.three-item-carousel' ).length ) {
		$( '.three-item-carousel' ).owlCarousel( {
			loop: true,
			margin: 30,
			nav: true,
			smartSpeed: 500,
			autoplay: 4000,
			navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 2
				},
				800: {
					items: 3
				},
				1024: {
					items: 3
				}
			}
		} );
	}


	// News Carousel
	if ( $( '.news-carousel' ).length ) {
		$( '.news-carousel' ).owlCarousel( {
			loop: true,
			margin: 0,
			nav: true,
			smartSpeed: 500,
			autoplay: 4000,
			navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 2
				},
				800: {
					items: 3
				},
				1024: {
					items: 3
				}
			}
		} );
	}


	// Testimonial Carousel
	if ( $( '.testimonial-carousel' ).length ) {
		$( '.testimonial-carousel' ).owlCarousel( {
			loop: true,
			margin: 80,
			nav: true,
			smartSpeed: 500,
			autoplay: 4000,
			navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1,
					margin: 30
				},
				600: {
					items: 2,
					margin: 30
				},
				800: {
					items: 2,
					margin: 30
				},
				1024: {
					items: 2
				}
			}
		} );
	}


	//LightBox / Fancybox
	if ( $( '.lightbox-image' ).length ) {
		$( '.lightbox-image' ).fancybox( {
			openEffect: 'fade',
			closeEffect: 'fade',
			helpers: {
				media: {}
			}
		} );
	}


	//Contact Form Validation
	if ( $( '#contact-form' ).length ) {
		$( '#contact-form' ).validate( {
			rules: {
				firstname: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				subject: {
					required: true
				},
				message: {
					required: true
				}
			}
		} );
	}


	// Scroll to a Specific Div
	if ( $( '.scroll-to-target' ).length ) {
		$( ".scroll-to-target" ).on( 'click', function () {
			var target = $( this ).attr( 'data-target' );
			// animate
			$( 'html, body' ).animate( {
				scrollTop: $( target ).offset().top
			}, 1500 );

		} );
	}


	// Elements Animation
	if ( $( '.wow' ).length ) {
		var wow = new WOW(
			{
				boxClass: 'wow',      // animated element css class (default is wow)
				animateClass: 'animated', // animation css class (default is animated)
				offset: 0,          // distance to the element when triggering the animation (default is 0)
				mobile: true,       // trigger animations on mobile devices (default is true)
				live: true       // act on asynchronously loaded content (default is true)
			}
		);
		wow.init();
	}

	if ( $( '.input-pin' ).length ) {
		$( '.input-pin' ).mask( '000-000', { placeholder: "XXX-XXX", clearIfNotMatch: true } );
	}

	// if ( $( "#userDropDown" ).length ) {

	// function startTour () {
// 	var tour = new Tour( {
// 		steps: [
// 			{
// 				element: "#userDropDown",
// 				title: "Tip",
// 				content: "Content of my step",
// 				backdrop: true,
// 				backdropPadding: 5,
// 				delay: 1000,
// 				placement: "left",
// 				next: -1,
// 				prev: -1,
// 				template: `<div class="popover tour tour-tour" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>`,
// 				onShown () {
// 					$("#userDropDown").dropdown('toggle')
// 				}
// 			},
// 		]
// 	} );
// 	// }
// 	// }
// 	tour.init();
//
// // Start the tour
// 	tour.start();

	if ( $( "#loginForm" ).length ) {
		ajaxForm( "#loginForm", false, function ( response, $form ) {
			setTimeout( function () {
				if ( response.data ) {
					$( "#user-box" ).html( response.data );
				}
				if ( response.redirect ) {
					if ( response.redirect === "/" && window.location.pathname === '/login' ) {
						window.location.href = "/";
					} else if ( response.redirect !== "/" ) {
						window.location.href = response.redirect;
					}
				}
			}, 1000 );
		} );
	}

	if ( $( '#modalLoginForm' ).length ) {
		ajaxForm( "#modalLoginForm", false, function ( response, $form ) {
			setTimeout( function () {
				$( "#modalLogin" ).modal( "hide" );
				if ( response.data ) {
					$( "#user-box" ).html( response.data );
				}
				if ( response.redirect ) {
					if ( response.redirect === "/" && window.location.pathname === '/login' ) {
						window.location.href = "/";
					} else if ( response.redirect !== "/" ) {
						window.location.href = response.redirect;
					}
				}
			}, 1000 );
		} );
	}

	if ( $( '#registrationForm' ).length ) {
		var $localeInput = $( "#regInputLcoale" );
		var $phoneInput = $( ".regInputPhone" );
		$phoneInput.prop( "disabled", true );
		$localeInput.change( function ( e ) {
			let country = phoneCodes.find( x => x.code === this.value );
			$phoneInput.prop( "disabled", false );
			$phoneInput.unmask();
			$phoneInput.val( country.dial_code );
			$phoneInput.mask( country.dial_code + " 0#", {
				placeholder: country.dial_code,
				clearIfNotMatch: true,
				onKeyPress: function ( cep, event, currentField, options ) {
					if ( cep.length < country.dial_code.length ) {
						$phoneInput.val( country.dial_code );
					}
				}
			} );
		} );


		$( "#agreeTerms" ).click( function () {
			$( "#termsCheckbox" ).prop( "checked", true );
		} );

		ajaxForm( "#registrationForm", false, function ( response, $form ) {
			setTimeout( function () {
				$form.hide( 'slow' ).delay( 200 ).remove();
				$( response ).hide().appendTo( "#registrationFormContainer" ).fadeIn( 1000 );
			}, 1000 )
		} );
	}

	var $spinner = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>';

	function ajaxForm ( form, callbackBefore, callbackSuccess, callbackError ) {
		callbackBefore = callbackBefore || function () {
		};
		callbackSuccess = callbackSuccess || function () {
		};
		callbackError = callbackError || function () {
		};

		var $form = $( form );
		var $inputs = $form.find( "input" );
		var $submitBtn = $form.find( 'button[type="submit"]' );

		var method = $submitBtn.data( 'method' ) || "POST";
		var url = $form.attr( "action" );
		var submitBtnDefaultText = $submitBtn.html();

		var deleteErrors = function () {
			$form.find( ".invalid-feedback, .invalid-response" ).hide( 'slow' ).delay( 500 ).remove();
			$form.find( ".is-invalid" ).removeClass( '.is-invalid' );
		};

		var inputStates = function ( state ) {
			state = state === "disabled";
			$inputs.prop( "disabled", state );
			$submitBtn.html( state ? $spinner : submitBtnDefaultText ).prop( "disabled", state );
		};

		$form.submit( function ( e ) {
			e.preventDefault();
			var data = $form.serialize();
			$.ajax( {
					url: url,
					method: method,
					data: data,
					beforeSend () {
						deleteErrors();
						inputStates( "disabled" );
						callbackBefore( $form );
					},
					success ( response ) {
						deleteErrors();
						inputStates( "enabled" );
						return callbackSuccess( response, $form );
					},
					error ( errorResponse ) {
						inputStates( "enabled" );
						if ( errorResponse.hasOwnProperty( "responseJSON" ) ) {
							var errorList = errorResponse.responseJSON.errors;
							for ( let err in errorList ) {
								if ( errorList.hasOwnProperty( err ) ) {
									var $invalidInput = $form.find( 'input[name="' + err + '"]' );
									$invalidInput.addClass( 'is-invalid' );
									if ( err === 'g-recaptcha-response' ) {
										$( "<div/>", { class: "invalid-response text-danger" } ).text( errorList[ err ] ).insertAfter( "#captcha" );
										grecaptcha.reset();
									} else {
										$( "<div/>", { class: "invalid-feedback" } ).text( errorList[ err ] ).insertAfter( $invalidInput );
									}
								}
							}
						}
						return callbackError( errorResponse, $form );
					}
				}
			);
		} );

	}

	if ( $( "#mock" ).length ) {
		var $video = document.getElementById( 'mock' );
		var $link = $( ".video-link" );
		$video.addEventListener( 'loadeddata', function () {
			$video.play();
		}, false );
		$link.click( function ( e ) {
			e.preventDefault();
			var $current = $( this );
			$video.setAttribute( "src", $current.data( "video" ) );
			$video.load();
			$video.play();
			$( ".video-link.active-btn" ).removeClass( "active-btn" );
			$current.addClass( "active-btn" );
		} );
	}

	/* ==========================================================================
	   When document is Scrollig, do
	   ========================================================================== */

	$( window ).on( 'scroll', function () {
		headerStyle();
	} );

	/* ==========================================================================
	   When document is loading, do
	   ========================================================================== */

	$( window ).on( 'load', function () {
		handlePreloader();
	} );


})
( window.jQuery );