import './bootstrap';
import Vue from 'vue';
import VueStash from 'vue-stash';
import store from './store-manager';
import VueProgressBar from 'vue-progressbar'
import Translations from './plugins/trans';
import Sound from './plugins/sounds';
import Helpers from './helpers';
import Middleware from './middleware';
import GlobalMethods from './global-methods';
import Auth from './plugins/auth';
import './websocket';
import './imports';

Vue.prototype.moment = moment;
Vue.prototype.mApp = mApp;


import { SimpleSVG } from 'vue-simple-svg';

Vue.component( 'simple-svg', SimpleSVG );

// Vue.config.errorHandler = function ( err, vm, info ) {
// 	console.error( err, vm, info );
// 	toastr.error( err, info );
// };

// window.onerror = function () {
// 	toastr.error( 'JAVASCRIPT ERROR', 'Open console' );
// };

Vue.use( VueProgressBar, {
	color: '#61D39F',
	failedColor: '#EB506C',
	thickness: '5px',
	autoFinish: false
} );
Vue.use( Translations );
Vue.use( Sound );
Vue.use( Helpers );
Vue.use( VueStash );

Vue.use( Auth );
Vue.mixin( GlobalMethods );

window.Vue = new Vue( {
	el: '#app',
	mixin: [ Middleware ],
	data: { store },
	mounted () {
		this.$helpers.replaceFailedImages();
	}
} );
