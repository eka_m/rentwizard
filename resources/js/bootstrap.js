$.ajaxSetup( {
	headers: {
		'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr( 'content' )
	},
} );

import lodash from "lodash";
import { noun } from 'plural-ru';
import axios0 from "axios";
import 'events';
import imagesLoaded from 'imagesloaded';

window.moment = require( 'moment' );
import 'moment/locale/ru';
import 'moment/locale/az';
import 'moment-timezone';

window.moment.locale( window.appLocale );
window.moment.tz.setDefault( window.appTimezone );
window.imagesLoaded = imagesLoaded;
window._ = lodash;
window.noun = noun;
window.axios = axios0;


$( document ).ajaxStart( function () {
	Vue.$Progress.start();
} );
$( document ).ajaxComplete( function () {
	Vue.$Progress.finish();
} );
$( document ).ajaxError( function () {
	Vue.$Progress.fail();
	// toastr.error( 'Server error' );
} );

window.axios.defaults.headers.common[ 'X-Requested-With' ] = 'XMLHttpRequest';
axios.interceptors.request.use( config => {
	Vue.$Progress.start();
	return config;
} );

axios.interceptors.response.use(
	response => {
		Vue.$Progress.finish();
		switch ( response.status ) {
			case 200:
				if ( response.data.message ) {
					let type = 'success';
					if ( response.data.hasOwnProperty( 'type' ) ) type = response.data.type;
					toastr.remove();
					toastr[ type ]( response.data.message );
				}
				break;
		}
		return response;
	},
	error => {
		Vue.$Progress.fail();
		switch ( error.response.status ) {
			case 500:
				// toastr.error( "Server error" );
				toastr.error( error.response.data.message );
				break;
			case 400:
				toastr.error( error.response.data.message );
				break;
		}
		return Promise.reject( error );
	}
);

let token = document.head.querySelector( 'meta[name="csrf-token"]' );

if ( token ) {
	window.axios.defaults.headers.common[ 'X-CSRF-TOKEN' ] = token.content;
} else {
	console.error( 'CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token' );
}