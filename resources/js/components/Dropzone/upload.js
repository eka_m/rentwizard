export default {
	mounted () {
		this.listenSubmit();
	},
	methods: {
		listenSubmit () {
			this.$refs.upload_manager.closest( 'form' ).addEventListener( 'submit', event => {
				event.preventDefault();
				const form = event.currentTarget;
				if ( this.files.length === 0 && $( form ).data( 'validator' ).form() ) {
					return form.submit();
				}
				mApp.block( form );
				this.upload().then( data => {
					if ( data.length > 0 ) {
						data.forEach( item => {
							let elem = this.files.find( x => x.hasOwnProperty( 'object' ) && x.object.name === item.original_name );
							elem.id = item.id;
						} );
					}
				} ).then( () => {
					if($( form ).data( 'validator' ).form()) {
						form.submit();
					}
					mApp.unblock( form );
				} );
			} )
		},
		upload () {
			return new Promise( ( resolve, reject ) => {
				let data = new FormData();
				this.files.forEach( file => {
					if ( file.hasOwnProperty( 'object' ) && file.object ) {
						data.append( `files[]`, file.object );
						data.append( `properties[${ file.object.name }]`, JSON.stringify( file.properties ) );
					}
				} );
				if ( [ ...data ].length === 0 ) return resolve( [] );
				axios.post( this.propEndpoint, data ).then( r => {
					resolve( r.data, )
				} ).catch( error => {
					reject( error )
				} );
			} );
		}
	}
}