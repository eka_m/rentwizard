import Uppy from "@uppy/core";
import DragDrop from "@uppy/drag-drop";
import XHRUpload from "@uppy/xhr-upload";
import Form from '@uppy/form';

export default {
	name: "Uppy",
	data: () => ({
		show_uppy: false,
		uppy: null,
		options: {
			autoProceed: false,
			allowMultipleUploads: true
		}
	}),
	mounted () {
		this.methods = {
			onBeforeUpload: ( files ) => {
				let updatedFiles = Object.assign( {}, files );
				Object.keys( files ).map( fileId => {
					const file = this.files.find( x => x.id = fileId );
					// this.uppy.setFileMeta( fileId, { properties: JSON.stringify(file.properties) } );
					updatedFiles[ fileId ].meta = file.properties;
					console.log( updatedFiles[ fileId ] );
				} );
				return updatedFiles;
			}
		};
		this.options = Object.assign( this.options, this.propOptions, this.methods );
		this.init();
		this.initEvents();
	},
	methods: {
		init () {
			this.uppy = new Uppy( this.options );
			this.uppy.use( DragDrop, { target: this.$refs.uppy } );
			this.uppy.use( Form, {
				target: this.$refs.upload_manager.closest( 'form' ),
				getMetaFromForm: false,
				addResultToForm: false,
				resultName: 'uppyResult',
				triggerUploadOnSubmit: true,
				submitOnSuccess: false
			} );
			this.uppy.use( XHRUpload, {
				endpoint: this.propEndpoint,
				headers: { 'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr( 'content' ) },
				formData: true,
				bundle: false,
			} );
		}
		,
		initEvents () {
			this.uppy.on( "file-added", file => {
				this.toBase64( file ).then( f => {
					file[ 'dataURL' ] = f;
					this.add( file );
					this.show_uppy = false;
				} );
			} );
			this.uppy.on( 'upload-success', ( file, body ) => {
				this.files.map( f => {
					if ( f.id === file.id ) {
						f.tmp = body[ 0 ].tmp;
						f.id = body[ 0 ].id;
					}
				} );
			} );
			this.uppy.on( 'complete', ( result ) => {
				console.log( result.body );
			} );
			this.uppy.on( 'file-removed', ( file ) => {
				console.log( 'Removed file', file )
			} );
		}
		,
		upload () {
			this.uppy.upload();
		}
		,
		choose () {
			$( '.uppy-DragDrop-input' ).trigger( 'click' );
		}
		,
		toBase64 ( file ) {
			return new Promise( ( resolve, reject ) => {
				const reader = new FileReader();
				reader.readAsDataURL( file.data );
				reader.onload = () => resolve( reader.result );
				reader.onerror = error => reject( error );
			} );
		}
	}
}