export const columnDefs = {
	data: () => ({
		columnDefs: [
			{
				targets: 2,
				render: function ( data, type, row, meta ) {
					return $( "<div/>" ).html( data ).text();
				}
			},
			{
				targets: 3,
				title: 'Название',
				orderable: true,
				render: function ( data, type, row, meta ) {
					return `<a href="/inventory/${ row.id }">${ row.name }</a> `;
				}
			},
			{
				targets: 5,
				title: 'Фото',
				orderable: false,
				render: function ( data, type, row, meta ) {
					if ( row.thumb !== null ) {
						return '<img src="' + row.thumb + '" class="img-fluid">';
					}
					return Vue.$helpers.fallbackImg();
				},
			},
			{
				targets: 7,
				title: 'Категория',
				orderable: false,
				render: function ( data, type, row, meta ) {
					return `<span class="m-badge m-badge--focus m-badge--wide m--font-boldest" >${ row.category }</span>`;
				},
			},
			{
				targets: 8,
				title: 'Статус',
				orderable: false,
				render: function ( data, type, row, meta ) {
					return `<span class="m-badge m-badge--wide m--font-boldest m-badge--${ row.statuses[ row.status ].class }">
                        ${ row.statuses[ row.status ].title }
                        </span>`;
				},
			},
			{
				targets: -1,
				render ( data, type, row, meta ) {
					return ` <div class="row-actions" id="${ row.id }"  data-actions="edit,remove"></div>`;
				}
			}
		]
	}),
};

export const columns = {
	data: () => ({
		columns: [
			{ data: 'id', name: 'id', width: "3%" },
			{ data: 'inventory_code', name: 'inventory_code', width: "5%" },
			{ data: 'qr', name: 'qr', width: "10%", className: "text-center ", orderable: false, searchable: false },
			{ data: 'name', name: 'name', className: "text-center" },
			{ data: 'model', name: 'model', title: "Модель" },
			{ data: 'thumb', width: "8%", className: "text-center", orderable: false, searchable: false },
			{ data: 'cost', name: 'cost', orderable: false, searchable: false },
			{ data: 'category', name: 'category.name', width: "13%", className: "text-center" },
			{ data: 'status', width:"20%", name: 'status', className: "text-center" },
			{ data: 'actions', width:"15%", orderable: false, searchable: false },
		]
	})
};

