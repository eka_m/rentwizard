export default {
	data: () => ({
		table: null,
		options: {
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			searching: true,
			lengthChange: false,
			dom: 'B, t, i, p, r',
			rowId: "id",
			ajax: null,
			order: [ [ 0, 'DESC' ] ],
			language: {
				url: "/plugins/datatables/ru.json"
			}
		},
	}),
	created () {
		this.options.ajax = {
			url: '/async' + this.actionsUrl,
			method: 'GET',
			complete: () => this.$store.loader = false
		};
	},
	mounted () {
		$.fn.dataTable.ext.errMode = 'none';
		this.init();
		this.initEvents();
		this.initActions();
	},
	computed: {
		/** Create table options from parts */
		tableOptions () {
			return Object.assign( {}, this.options, { columns: this.columns }, { columnDefs: this.columnDefs } );
		}
	},
	methods: {
		init () {
			/** Initialize table  */
			this.table = $( this.$refs.table ).DataTable( this.tableOptions );
		},
		initEvents () {
			this.table.on( `${ this.events.init } ${ this.events.draw }`, e => {
				mApp.initTooltips();
			} );
			this.table.on( this.events.error, e => {
				toastr.error( 'Something went wrong' );
				Vue.$Progress.fail();
			} );
		}
	}
};