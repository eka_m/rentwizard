export default {
	install ( Vue ) {
		Vue.prototype.$helpers = this.methods;
	},
	methods: {
		tenant: window.tenant,
		loader: true,
		isEmpty ( value ) {
			switch ( value ) {
				case "":
				case 0:
				case "0":
				case null:
				case false:
				case typeof this === "undefined":
					return true;
				default:
					return false;
			}
		},
		monthNames () {
			return [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ];
		},
		csrf () {
			return document.head.querySelector( 'meta[name="csrf-token"]' ).content;
		},
		imageBroken ( e ) {
			const elem = event.currentTarget;
			elem.onerror = null;
			elem.outerHTML = this.fallbackImg();
			return true;
		},
		replaceFailedImages ( continer = "#app" ) {
			imagesLoaded( $( continer ) ).on( 'progress', ( instance, image ) => {
				if ( !image.isLoaded ) {
					image.img.outerHTML = this.fallbackImg();
				}
			} )
		},
		fallbackImg ( type = "svg" ) {
			return `<img src="/img/no-image.${ type }" style="width:100%;">`;
		},
		attachScript ( tag_id, path ) {
			return new Promise( ( resolve, reject ) => {
				const tag = $( tag_id );
				if ( tag.length === 0 ) {
					const tag = $( "<script />", {
						type: "text/javascript",
						src: path,
						id: tag_id
					} );
					$( "head" ).append( tag );
					resolve( true )
				} else {
					reject( false )
				}
			} );

		},
		attachStyle ( tag_id, path ) {
			return new Promise( ( resolve, reject ) => {
				const tag = $( tag_id );
				if ( tag.length === 0 ) {
					const tag = $( "<link />", {
						rel: "stylesheet",
						href: path,
						id: tag_id
					} );
					$( "head" ).append( tag );
					resolve( true )
				} else {
					reject( "sdsd" )
				}
			} );
		},
		scroll ( selector ) {
			$( selector ).each( function () {
				const el = $( this );
				mUtil.scrollerInit( this, {
					disableForMobile: true, handleWindowResize: true, height: function () {
						if ( mUtil.isInResponsiveRange( 'tablet-and-mobile' ) && el.data( 'mobile-height' ) ) {
							return el.data( 'mobile-height' );
						} else {
							return el.data( 'height' );
						}
					}
				} );
			} );
		},
		badge ( value ) {
			return `<span class='m-badge m-badge--wide m-badge--${ Vue.trans( 'classes.' + value ) }'>${ Vue.trans( value ) }</span>`;
		}
	}
}
