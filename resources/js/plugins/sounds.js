import  'ion-sound/js/ion.sound.min';

export default {
	install ( Vue ) {
		this.init();
		Vue.prototype.$sound = ion.sound;
	},
	init () {
		ion.sound( {
			sounds: [
				{
					name: "notify"
				},
			],
			volume: 0.5,
			path: "/sounds/",
			preload: true
		} );
	}
}
