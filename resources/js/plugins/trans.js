import matchAll from 'match-all';

export default {
	install ( Vue ) {
		Vue.prototype.trans = ( string ) => {
			let result = _.get( window.i18n, string );
			if ( window.appLocale == "en" ) {
				if ( _.startsWith( string, 'classes' ) ) {
					return this.classes( string );
				}
				return string;
			}
			return result ? result : string;
		};
		Vue.prototype.$locale = window.appLocale;
		Vue.prototype.$transRegex = this.change;
	},
	classes ( value ) {
		const classes = {
			"Available": "success",
			"Reserved": "warning",
			"Rented": "brand",
			"Missing": "danger",
			"Bad": "danger",
			"Average": "warning",
			"New": "success",
			"VIP": "focus",
			"Reliable": "success",
			"Undefined": "metal text-dark",
			"Blocked": "danger",
			"Unreliable": "warning",
			"Employee": "accent",
			"Active": "info",
			"Planned": "warning",
			"Completed": "success",
			"Expired": "focus",
			"Not paid": "danger"
		};

		return _.get( classes, value.slice( 8, value.indexOf( 'classes.' ) + value.length ) );
	},
	change ( string ) {
		const matches = matchAll( string, /%(.*?)%/g ).toArray();
		matches.map( word => {
			let trans = window.appLocale == "en" ? word : _.get( window.i18n, word );
			string = string.replace( "%" + word + "%", trans ? trans : word );
		} );
		return string;
	},
}
