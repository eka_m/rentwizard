import wizard from './wizard';
import { initValidations } from './form-validation';

window.addEventListener( 'offline', function ( e ) {
	$( '.offline-notification' ).css( 'color', '#BD301B' );
	$( '.offline-notification-status' ).text( Vue.trans( 'Offline' ) );
	$( '#logoarrows' ).removeClass( 'rotate' );
} );

window.addEventListener( 'online', function ( e ) {
	$( '.offline-notification' ).css( 'color', '#61D39F' );
	$( '.offline-notification-status' ).text( Vue.trans( 'Online' ) );
	$( '#logoarrows' ).addClass( 'rotate' );
} );

$( document ).ready( () => {
	/*Selectpicker*/
	const selectPickerElement = $( ".selectPicker" );
	const selectPickerrOptions = selectPickerElement.data( "options" );
	selectPickerElement.selectpicker( selectPickerrOptions );
	/*End Selectpicker*/

	if ( classExists( 'initPortlet' ) ) {
		const portlet_options = {
			bodyToggleSpeed: 400,
			tooltips: true,
			tools: {
				toggle: {
					collapse: Vue.trans( 'Collapse' ),
					expand: Vue.trans( 'Expand' )
				},
				reload: Vue.trans( 'Update' ),
				remove: Vue.trans( 'Remove' ),
				fullscreen: {
					on: Vue.trans( 'Fullscreen' ),
					off: Vue.trans( 'Exit fullscreen' )
				}
			}
		};
		$( '.initPortlet' ).each( function ( e ) {
			new mPortlet( this, portlet_options );
		} );
	}

	/*Bootstrap DatePicker*/
	const datePickerElement = $( ".dateInput" );
	const datePickerInitialOptions = {
		autoclose: true,
		weekStart: 1
	};
	const datePickerOptions = Object.assign(
		datePickerInitialOptions,
		datePickerElement.data( "options" )
	);
	datePickerElement.datepicker( datePickerOptions );
	/*End Bootstrap DatePicker*/

	/*Bootstrap DateTimePicker*/
	const dateTimePickerElement = $( ".dateTimeInput" );
	const dateTimePickerInitialOptions = {
		weekStart: 1,
		todayHighlight: true,
		autoclose: true,
	};
	const dateTimePickerOptions = Object.assign(
		dateTimePickerInitialOptions,
		dateTimePickerElement.data( "options" )
	);
	dateTimePickerElement.datetimepicker( dateTimePickerOptions );
	/*End Bootstrap DateTimePicker*/

	/* Switch */
	if ( classExists( "switch" ) ) {
		const valueInput = $( '.switch > input[type="hidden"]' );
		const switchInput = $( '.switch > input[type="checkbox"]' );
		valueInput.val( valueInput.data( "val" ) );
		// console.log(valueInput.val());
		switchInput
			.bootstrapSwitch( {
				state: valueInput.data( "val" )
			} )
			.on( "switchChange.bootstrapSwitch", ( event, state ) => {
				valueInput.val( state ? 1 : 0 );
			} );
	}
	/* End switch */

	if ( classExists( 'timezoneSelect' ) ) {
		$( '.timezoneSelect' ).on( 'changed.bs.select', function ( e, clickedIndex, isSelected, previousValue ) {
			Vue.$store.timezone = e.currentTarget.value;
		} );
	}
} );
initValidations();
wizard();


/* Pagination */
if ( classExists( "paginatiable-box" ) ) {
	const boxItem = $( ".paginatiable-box" );
	boxItem.each( function ( e ) {
		const elem = $( this );
		const url = elem.data( "url" );
		getPaginatiableData( url, elem );
	} );

	$( document ).on( "click", ".paginatiable-box .pagination a", function ( e ) {
		e.preventDefault();
		const url = $( this ).attr( "href" );
		const conatiner = $( this ).closest( ".paginatiable-box" );
		getPaginatiableData( url, conatiner );
	} );
}

function getPaginatiableData ( url, container ) {
	$.ajax( {
		url: url,
		dataType: "json",
		beforeSend () {
			mApp.block( container, {
				overlayColor: "rgba(36, 127, 225, 1.00)",
				type: "loader",
				state: "success",
				message: Vue.trans( "Please wait" ) + '...'
			} );
		}
	} )
		.done( function ( data ) {
			container.html( data );
			// location.hash = page;
			mApp.unblock( container );
			mApp.initTooltips();
		} )
		.fail( function ( err ) {
			console.log( err );
			mApp.unblock( container );
		} );
}

/* End Pagination */


/*CLOCK*/
if ( classExists( "dashboard" ) ) {
	let time = moment().tz( window.appTimezone ),
		hours = time.hours(),
		minutes = time.minutes(),
		seconds = time.seconds();

	document.getElementById( "second-hand" ).className = "sh-" + seconds;
	document.getElementById( "minute-hand" ).className = "mh-" + minutes;
	document.getElementById( "hour-hand" ).className = "hh-" + hours;
	document.getElementById( "today-date" ).innerText = time.format( 'D MMMM Y' );
}
/*END CLOCK*/

/* FALLBACK IMAGE */
window.fallbackImage = function ( elem ) {
	$( elem ).attr( 'src', "/img/no-image.jpg" );
};

/* END FALLBACK IMAGE */

/*HELPERS*/

function classExists ( className ) {
	return $( "." + className ).length > 0;
}

function idExists ( id ) {
	return $( "#" + id ).length > 0;
}

/*END HELPERS*/
