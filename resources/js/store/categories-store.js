export default {
	all: [],
	changed: false,
	set ( data ) {
		this.all = data;
		this.changed = !this.changed;
	},
	add ( item ) {
		this.all.push( item );
		this.changed = !this.changed;
	}
}