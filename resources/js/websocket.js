import Echo from "laravel-echo";

window.io = require( 'socket.io-client' );

window.Echo = new Echo( {
	broadcaster: 'socket.io',
	host: window.location.hostname + ':6001'
} );

window.Echo.private( `inventory.${ user.id }.${ tenant.id }` )
	.listen( 'ShowInventory', ( e ) => {
		console.log( e );
	} );

let count = 0;
window.Echo.join( 'online' )
	.here( users => {
		 
	} )
	.joining( user => count++ )
	.leaving( user => count-- );
