<?php
return [
	"hour" => "Saat",
	"day" => "Gün",
	"month" => "Ay",
	"year" => "İl",
	"month_names" => [
		"full" => [
			"Yanvar",
			"Fevral",
			"Mart",
			"Aprel",
			"May",
			"Iyun",
			"Iyul",
			"Avqust",
			"Sentyabr",
			"Oktyabr",
			"Noyabr",
			"Dekabr"
		],
		"abbr" => [
			"Yan.",
			"Fev.",
			"Mart.",
			"Apr.",
			"May",
			"Iyun",
			"Iyul",
			"Avq.",
			"Sen.",
			"Okt.",
			"Noy.",
			"Dek."
		]
	]
];