<?php
return [
	"hour" => "Hour",
	"day" => "Day",
	"month" => "Month",
	"year" => "Year",
	"month_names" => [
		"full" => [
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December"
		],
		"abbr" => [
			"Jan.",
			"Feb.",
			"Mar.",
			"Apr.",
			"May",
			"June",
			"July",
			"Aug.",
			"Sept.",
			"Oct.",
			"Nov.",
			"Dec."
		]
	]
];