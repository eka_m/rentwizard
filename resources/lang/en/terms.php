<?php
return [
	"terms" => "
<h3>User Agreement</h3>
<p>This User Agreement (hereinafter the &ldquo;Agreement&rdquo;) shall govern relations between Rent Wizard LTD (hereinafter &ldquo;Rent Wizard&rdquo; or &ldquo;Administration&rdquo;), of the one part, and the website user, of the other part.</p>
<p>The Rent Wizard website is a mass medium.</p>

<p>By using the website, you agree to the terms and conditions of this Agreement.</p>
<p>If you do not agree to the terms and conditions of this Agreement, please, do not use the Rent Wizard website!</p>

<h3>Subject Matter of the Agreement</h3>
<p>The Administration shall grant the user the right to post the following information on the website:</p>

<h3>Subject Matter of the Agreement</h3>
<p>The Administration shall grant the user the right to post the following information on the website:</p>
<p>- Text information</p>
<p>- Photos</p>
<p>- Links to materials posted on other websites</p>

<h3>Rights and Obligations of the Parties</h3>
<p>The user shall have the right to:</p>
<p>- search for information on the website</p>
<p>- receive information on the website</p>
<p>- create information for the website</p>
<p>- copy information to other websites with reference to the source</p>
<p>- demand the Administration to hide any user information</p>
<p>- demand the Administration to hide any information transferred to the website by the user</p>
<p>- use the website information for personal non-commercial purposes</p>

<p>The Administration shall have the right to:</p>
<p>- establish, modify, cancel rules as needed at its sole discretion</p>
<p>- restrict access to any information on the website</p>
<p>- create, modify, delete information</p>
<p>- delete accounts</p>
<p>- deny registration without explaining the reasons</p>

<p>The user shall:</p>
<p>- ensure accuracy of the information provided</p>
<p>- ensure security of personal data from access by any third parties</p>
<p>- update Personal Data provided at registration in case of any changes in them</p>
<p>- when copying information from other sources, include information about the author</p>
<p>- not disseminate information aimed at propaganda of war, incitement of national, racial or religious hatred and animosities, as well as other information the dissemination of which is prosecuted criminally or administratively</p>
<p>- not disrupt operation of the website</p>
<p>- not take actions aimed at misleading of other users</p>
<p>- not transfer his/her account and/or login and password to his/her account to any third parties</p>
<p>- not post materials of advertising, erotic, pornographic or offensive nature, as well as other information which is prohibited to be posted by or is contrary to the provisions of the applicable legislation of the Russian Federation</p>
<p>- not use scripts (programs) for automated data collection and/or interaction with the website and its services</p>

<p>The Administration shall:</p>
<p>- maintain operability of the website, unless it is impossible for the reasons beyond the Administration&rsquo;s control.</p>
<p>- ensure versatile protection of the user account</p>
<p>- protect information the dissemination of which is restricted or prohibited by law by issuing a warning or deleting the account of the user who has violated the rules</p>

<h3>Liability of the Parties</h3>
<p>- the user shall be solely liable for information he/she disseminates.</p>
<p>- the Administration shall not be liable for any services provided by the third parties</p>
<p>- in the event of force majeure (hostilities, emergency, natural disaster, etc.), the Administration does not guarantee safety of information posted by the user, as well as smooth operation of the information resource</p>

<h3>Terms of the Agreement</h3>
<p>This Agreement shall come into force upon registration on the website.</p>
<p>This Agreement shall cease to be effective upon publication of its new version.</p>
<p>The Administration reserves the right to amend this Agreement unilaterally at its sole discretion.</p>
<p>When amending this Agreement, the Administration may notify users thereof in some cases in a way convenient to the Administration.</p>
	"
];