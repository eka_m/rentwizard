<?php
return [
	"Available" => "success",
	"Reserved" => "warning",
	"Rented" => "brand",
	"Missing" => "danger",
	"In repair"=> "info",
	"Bad" => "danger",
	"Average" => "warning",
	"New" => "success",
	"VIP" => "focus",
	"Reliable" => "success",
	"Undefined" => "metal text-dark",
	"Blocked" => "danger",
	"Unreliable" => "warning",
	"Employee"=> "accent",
	"Active"=> "info",
	"Planned"=> "warning",
	"Completed"=> "success",
	"Expired"=> "focus",
	"Not paid"=> "danger"
];