<div class="row">
	<div class="col-md-6 offset-md-3">
		<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon"><i class="flaticon-web"></i></span>
						<h3 class="m-portlet__head-text">
							{{__('Attribute')}}
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="row">
					<div class="form-group col-md-12">
						<label class="form-control-label">{{__('Name')}}:</label>
						<input type="text" class="form-control m-input" name="name" placeholder="{{__('Name')}}"
						       value="{{old('name', $attribute->name)}}" required>
						<input type="hidden" class="form-control m-input" name="type" value="text">
						<input type="hidden" class="form-control m-input" name="slug" value="text">
					</div>
				</div>
				<div class="m-form__group form-group row">
					<label class="col-3 col-form-label">{{__('Show in the table')}}</label>
					<div class="col-3">
						<input type="hidden" name="in_table" value="0">
						<span class="m-switch m-switch--icon">
							<label>
									<input type="checkbox" name="in_table" @if($attribute->in_table) checked @endif value="1">
								<span></span>
							</label>
						</span>
					</div>
					<label class="col-3 col-form-label">{{__('Show in the search results for deal')}}</label>
					<div class="col-3">
						<input type="hidden" name="in_search" value="0">
						<span class="m-switch m-switch--icon">
							<label>
								<input type="checkbox" name="in_search" @if($attribute->in_search) checked @endif value="1">
								<span></span>
							</label>
						</span>
					</div>
				</div>
				<div class=" row pt-5">
					<div class="col-md-6">
						<a href="{{ url()->previous() }}" class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md">
							{{__('Back')}}
						</a>
					</div>
					<div class="col-md-6 m--align-right">
						<button type="submit" class="btn btn-success m-btn m-btn--icon m-btn--wide m-btn--md">
							{{(__('Save'))}}
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

