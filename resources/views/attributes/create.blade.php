@extends('layouts.base')
@section('title',' | ' . __('Attribute'))
@section('content')
	<div class="m-content">
		<form action="{{route('attributes.store')}}" method="post" class="form validate">
			@method('POST')
			@csrf
			@include('attributes._form')
		</form>
	</div>
@endsection
