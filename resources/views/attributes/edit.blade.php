@extends('layouts.base')
@section('title',' | '.$attribute->name)
@section('content')
  <div class="m-content">
    <form action="{{route('attributes.update', $attribute->id)}}" method="post" class="form validate">
      @method('PUT')
      @csrf
      @include('attributes._form')
    </form>
  </div>
@endsection
