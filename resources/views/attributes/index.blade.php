@extends('layouts.base')
@section('title', ' | ' .__('Attributes'))
@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon"><i class="flaticon-web"></i></span>
								<h3 class="m-portlet__head-text">
									{{__('Attributes')}}
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="{{route('attributes.create')}}"
									   data-toggle="m-tooltip"
									   data-placement="top"
									   data-html="true"
									   data-skin="light"
									   data-original-title='{{__('Add')}}'
									   class="m-portlet__nav-link m-portlet__nav-link--icon">
										<i class="la la-plus"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						<div class="list-group m-scrollable" data-scrollbar-shown="true" data-scrollable="true"
						     data-height="300"
						     data-mobile-max-height="300">
							@foreach($attributes as $attrubute)
								<div class="list-group-item">
									{{$attrubute->name}}
									<div class="pull-right">
										<a href="{{route('attributes.edit', $attrubute->id)}}"
										   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
										   title="Редактировать">
											<i class="la la-edit"></i>
										</a>
										<delete-btn
											class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
											url="{{route('attributes.destroy', $attrubute->id)}}"
											confirm="{{__('Are you sure?')}}">
											<i class="la la-trash"></i>
										</delete-btn>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
