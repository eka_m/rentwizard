@extends('layouts.app')
@section('content')
	<div class="container-fluid register">
		<div class="row pt-5">
			<div class="col-md-3 register-left">
				<img src="{{asset('web/images/icons/preloader.svg')}}" alt=""/>
				<h5 class="text-dark">{{__("Welcome")}}</h5>
				<p>{{__("Sign in to get started")}}.</p>
				<br/>
				<br/>
				<br/>
			</div>
			<div class="col-md-9 register-right">
				<h3 class="register-heading">{{__("Sign in")}}</h3>
				<div id="registrationFormContainer" class="register-form commitment-form row">
					@if (session('verified'))
						<div class="col-md-10 offset-md-1">
							<div class="alert alert-success" role="alert">
								<p class="text-white m-0">{{__( "Your email address has been successfully verified. Please login to get started." )}}</p>
							</div>
						</div>
					@endif
					<div class="col-md-8 offset-md-2">
						<form method="POST" action="{{ route('login') }}" id="loginForm">
							@csrf
							<div class="row">
								<div class="col-md-8 pr-md-1">
									<div class="form-group">
										<small class="form-text text-muted text-right">&nbsp;</small>
										<input type="email" class="form-control" name="email" value="{{ old('email') }}"
										       placeholder="{{__('E-mail')}} {{mb_strtolower(__('Address'))}}" required autofocus>
									</div>
								</div>
								<div class="col-md-4 pl-md-0">
									<div class="form-group">
										<small class="form-text text-muted text-right">{{__('Optional')}}.</small>
										<input type="text" class="input-pin form-control"
										       name="pin"
										       value="{{ old('pin') }}" minlength="7" maxlength="7" placeholder="{{__('Company pin')}}">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<input type="password" class="form-control" name="password" placeholder="{{__('Password')}}"
										       required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="remember"
									       id="termsCheckbox" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="termsCheckbox">
										{{ __('Remember Me') }}
									</label>
								</div>
							</div>

							<div class="form-group">
								<button id="loginsubmit" type="submit" class="btn btn-success btn-lg pull-right w-100">
									{{ __('Login') }}
								</button>
							</div>
							<div class="form-group mt-5">
								<a href="{{ route('password.request') }}">
									{{ __('Forgot Your Password?') }}
								</a>
								<div class="pull-right">
									<span>{{ __("Don't have account?") }}</span>
									<a href="{{ route('register') }}">
										{{ __("Create") }}
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
