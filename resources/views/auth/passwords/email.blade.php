@extends('layouts.app')

@section('content')

	<div class="container-fluid register">
		<div class="row pt-5">
			<div class="col-md-3 register-left">
				<img src="{{asset('web/images/icons/preloader.svg')}}" alt=""/>
				<h5 class="text-dark">{{ __('Reset Password') }}</h5>
				<p>{{__("Enter your e-mail adress to receive password reset link")}}.</p>
				<br/>
				<br/>
				<br/>
			</div>
			<div class="col-md-9 register-right">
				<div id="registrationFormContainer" class="register-form commitment-form row">
					@if (session('status'))
						<div class="col-md-10 offset-md-1">
							<div class="alert alert-success" role="alert">
								<p class="text-white m-0">{{ session('status') }}</p>
							</div>
						</div>
					@endif
					<div class="col-md-10 offset-md-1">
						<form method="POST" action="{{ route('password.email') }}" id="sendResetEmailForm">
							@csrf
							<div class="form-group ">
								<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
								       name="email" value="{{ old('email') }}"
								       placeholder="{{ __('E-Mail') }} {{mb_strtolower(__("Address"))}}"
								       required>

								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										 <strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-success pull-right">
									{{ __('Send Password Reset Link') }}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
