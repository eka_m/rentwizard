@extends('layouts.app')

@section('content')
	<div class="container-fluid register pt-5">
		<div class="row pt-5">
			<div class="col-md-3 register-left">
				<img src="{{asset('web/images/icons/preloader.svg')}}" alt=""/>
				<h5 class="text-dark">{{__("Password Reset")}}</h5>
				<p>{{__("Set a new password")}}.</p>
				<br/>
				<br/>
				<br/>
			</div>
			<div class="col-md-9 register-right">

				<div id="registrationFormContainer" class="register-form commitment-form row">
					@if (session('status'))
						<div class="col-md-10 offset-md-1">
							<div class="alert alert-success" role="alert">
								<p class="text-white m-0">{{ session('status') }}</p>
							</div>
						</div>
					@endif
					<div class="col-md-10 offset-md-1">
						<form method="POST" action="{{ route('password.update') }}" id="resetPasswordForm">
							@csrf
							<input type="hidden" name="token" value="{{ $token }}" >
							<div class="form-group">
								<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
								       name="email" value="{{ $email ?? old('email') }}"
								       placeholder="{{ __('E-Mail') }} {{mb_strtolower(__("Address"))}}" required autofocus>
								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
							<div class="form-group row">
								<div class="col-md-6">
									<input type="password"
									       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
									       placeholder="{{ __('New Password') }}"
									       required>
									@if ($errors->has('password'))
										<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
									@endif
								</div>
								<div class="col-md-6">
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation"
									       placeholder="{{ __('Confirm Password') }}"
									       required>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-success pull-right">
									{{ __('Reset') }}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
