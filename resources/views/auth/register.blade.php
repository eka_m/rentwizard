@extends('layouts.app')
@push('js')
	{!! NoCaptcha::renderJs(app()->getLocale()) !!}
@endpush
@section('content')
	<div class="container-fluid register">
		<div class="row pt-5">
			<div class="col-md-3 register-left">
				<img src="{{asset('web/images/icons/preloader.svg')}}" alt=""/>
				<h5 class="text-success">{{__("Welcome")}}</h5>
				<p>{{__("You are 30 seconds from taking your business to the next level")}}.</p>
				<button class="btn btn-success" data-toggle="modal" data-target="#modalLogin">{{__("Sign in")}}</button>
				<br/>
				<br/>
				<br/>
			</div>
			<div class="col-md-9 register-right">
				<h3 class="register-heading">{{__("Registration")}}</h3>
				<div id="registrationFormContainer" class="register-form commitment-form">
					<form id="registrationForm" method="POST" action="{{ route('register') }}">
						@csrf
						<div class="row">
							<div class="col-md-6 form-group">
								<input id="name"
								       type="text"
								       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
								       name="name"
								       value="{{ old('name') }}"
								       placeholder="{{__('First name')}}*"
								       required
								       autofocus>
								@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
								@endif
							</div>
							<div class="col-md-6 form-group">
								<input id="last_name"
								       type="text"
								       class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
								       name="last_name"
								       value="{{ old('last_name') }}"
								       placeholder="{{__('Last name')}}*"
								       required>
								@if ($errors->has('last_name'))
									<span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('last_name') }}</strong>
                  </span>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-group">
								<input
												type="email"
												class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
												name="email"
												value="{{ old('email') }}"
												placeholder="{{__('E-mail')}} {{mb_strtolower(__("Address"))}}*"
												required>
								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
							<div class="col-md-6 form-group">
								<input id="company"
								       type="text"
								       class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}"
								       name="company"
								       value="{{ old('company') }}"
								       placeholder="{{ __('Company name') }}*"
								       required>
								@if ($errors->has('company'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('company') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-group">
								<select name="locale" id="regInputLcoale" class="form-control">
									<option class="hidden" selected disabled>{{__('Please select your country')}}*</option>
									@foreach(Country::all() as $code => $name)
										@if($code == 'AM')
											@continue
										@endif
										<option value="{{$code}}">{{$name}}</option>
									@endforeach
								</select>
								<input type="hidden" name="timezine" id="timezone" value="">
							</div>
							<div class="col-md-6 form-group">
								<input id="phone"
								       type="tel"
								       class="regInputPhone form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
								       name="phone"
								       value="{{ old('phone') }}"
								       placeholder="{{__('Phone number')}}"
								       required>
								@if ($errors->has('phone'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('phone') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-group">
								<input type="password"
								       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
								       name="password"
								       placeholder="{{ __('Password') }}"
								       required>
								@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
							<div class="col-md-6 form-group">
								<input id="password-confirm"
								       type="password"
								       class="form-control"
								       name="password_confirmation"
								       placeholder="{{__('Confirm Password')}}"
								       required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="terms"
									       id="termsCheckbox" {{ old('terms') ? 'checked' : '' }}>
									<label class="custom-control-label" for="termsCheckbox">
										{{ __('I am agree with') }} <a href="" data-toggle="modal"
										                               data-target="#termsModal">{{__('terms and conditions')}}</a>
									</label>
								</div>
								@if ($errors->has('terrms'))
									<span class="help-block">
								        <strong>{{ $errors->first('terrms') }}</strong>
								    </span>
								@endif
							</div>
							<div class="col-md-6 form-group">
								<div class="pull-right">
									<div id="captcha">{!! NoCaptcha::display() !!}</div>
									@if ($errors->has('g-recaptcha-response'))
										<span class="help-block">
								        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
								    </span>
									@endif
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 offset-md-6">
								<button type="submit" id="registerSubmit"
								        class="btn btn-success btnRegister">{{__("Register")}}</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade termsModal" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalHeader"
	     aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="modal-header bg-main">
					<h5 class="modal-title text-white" id="termsModalHeader">{{__('Terms and Conditions')}}</h5>
				</div>
				<div class="modal-body">
					<div class="termsContent">
						{!! __('terms.terms') !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="agreeTerms" data-dismiss="modal">{{__("Agree")}}</button>
				</div>
			</div>
		</div>
	</div>

@endsection
