<div class="row ">
	<div class="col">
		<div class="card">
			<div class="card-body bg-main text-white">
				<div id="resendMessage" class="alert alert-success d-none" role="alert">
					{{ __('A fresh verification link has been sent to your email address') }}.
				</div>
				<p class="text-white">
					{{__("Your email address")}}
					<a href="{{domainFromEmail($email)}}" class="cl-main-two" target="_blank">{{$email}}</a>
				</p>
				<p>{{ __('Before proceeding, please check your email for a verification link and confirm your email address to complete your registration') }}</p>
				<p>
					{{ __('If you did not receive the email') }}
					<a href="{{ route('verification.resend') }}" class="cl-main-two" id="resendBtn">{{ __('click here to request another') }}.</a>
				</p>
			</div>
		</div>
	</div>
</div>

