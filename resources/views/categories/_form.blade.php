<div class="row">
	<div class="col-md-6 offset-md-3">
		<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon"><i class="flaticon-attachment"></i></span>
						<h3 class="m-portlet__head-text">
							{{__('Categories')}}
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools"></div>
			</div>
			<div class="m-portlet__body">
				<div class="form-group">
					<label class="form-control-label"> {{__('Name')}}:</label>
					<input type="text" id="prod-name" class="form-control m-input" name="name" placeholder="{{__('Name')}}"
					       value="{{old('name', $category->name)}}">
				</div>
				<div class="row pt-5">
					<div class="col-md-6">
						<a href="{{ url()->previous() }}" class="btn btn-danger">
							{{__('Back')}}
						</a>
					</div>
					<div class="col-md-6 m--align-right">
						<button type="submit" class="btn btn-success">
							{{__('Save')}}
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

