<div class="row">
	<div class="col-md-4">
		<div class="m-portlet">
			{{--<div class="m-portlet__head">--}}
				{{--<div class="m-portlet__head-tools">--}}
				{{--</div>--}}
			{{--</div>--}}
			<div class="m-portlet__body">
				<div class="tab-content">
					<div class="tab-pane active show" id="base">
						<div class="form-group">
							<label class="form-control-label">{{__('Fullname')}}</label>
							<input type="text" id="client-name" class="form-control m-input" name="name"
							       placeholder="{{__('Fullname')}}"
							       value="{{old('name', $item->name)}}" required>
						</div>
						<div class="form-group">
							<label class="form-control-label">{{__('Phone number')}}</label>
							<input type="text" class="form-control m-input" name="phone" placeholder="{{__('Phone number')}}"
							       value="{{old('phone', $item->phone)}}">
						</div>
						<div class="form-group">
							<label class="form-control-label">{{__('E-mail')}}</label>
							<input type="text" class="form-control m-input" name="email" placeholder="Электронная почта"
							       value="{{old('email', $item->email)}}" data-rule-required="false">
						</div>
						<div class="form-group">
							<label class="form-control-label">{{__('Address')}}</label>
							<input type="text" class="form-control m-input" name="address" placeholder="{{__('Address')}}"
							       value="{{old('adress', $item->address)}}">
						</div>
						<div class="form-group">
							<label class="form-control-label">{{__('Passport number')}}</label>
							<input type="text" class="form-control m-input" id="passport_number" name="passport_number"
							       placeholder="{{__('Passport number')}}"
							       value="{{old('passport_number', $item->passport_number)}}">
						</div>
						<div class="form-group">
							<label class="form-control-label">{{__('Contact person')}}</label>
							<select name="client_id" id="client_id" class="form-control selectPicker" data-live-search="true">
								<option value="0">{{__('Missing')}}</option>
								@foreach($clients as $client)
									@if($client->id != $item->id)
										<option
											value="{{$client->id}}" {{$item->client_id && $item->client_id  == $client->id ? 'selected' : ''}}>
											{{$client->name}}
										</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label class="form-control-label">{{__('Status')}}</label>
							<select name="status" id="status" class="form-control m-select2 selectPicker">
								@foreach($statuses as  $status)
									<option value="{{$status}}" {{$item->status && $item->status == $status ? 'selected' : ''}}
									data-content='<span class="m-badge m-badge--wide m-badge--{{trans('classes.'.$status)}}">{{__($status)}}</span>'>
										{{__($status)}}
									</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="m-portlet">
			<div class="m-portlet__body">
				<upload-manager
					prop-endpoint="{{route('async.upload.tmp', ['input'=> 'files', 'collection' => 'clients', 'temporary' => 24*60])}}"
					:prop-files="{{json_encode($item->media)}}"
				/>
			</div>
		</div>
		<div class="m-portlet">
			<div class="m-portlet__body pt-5">
				<label class="form-control-label">{{__('Additional information')}}</label>
				<editor
					name="description"
					placeholder="{{__('Additional information')}}"
					prop-content="{{old('description', $item->description)}}"
					:prop-config="{btns: [['strong', 'em', 'foreColor']]}"></editor>
			</div>
		</div>
	</div>
</div>