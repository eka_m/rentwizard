<div class="row h-100">
	<div class="col-md-4 mb-2">
		@card
		@slot('title')
			<h6 class="m-0">{{__('Images')}}</h6>
		@endslot
		<fotorama data-allowfullscreen="native" data-nav="thumbs" :prop-media="{{$item->media->toJson()}}">
		</fotorama>
		@endcard
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-12">
				@card
				@slot('title')
					{{ __('Description')}}
					<div class="pull-right">
						<span class="m-badge m-badge--wide m-badge--{{__('classes.'.$item->status)}}">{{__($item->status)}}</span>
						<span class="m-badge m-badge--wide">№ {{$item->id}}</span>
					</div>
				@endslot
				<table class="attributes-table table table-bordered">
					<tr>
						<td>{{__('Fullname')}}</td>
						<td>{{$item->name}}</td>
					</tr>
					<tr>
						<td>{{__('Passport number')}}</td>
						<td>{{is_exists($item->passport_number)}}</td>
					</tr>
					<tr>
						<td>{{__('Contact person')}}</td>
						<td>{{is_exists_arr($item->contactface,'name', __('Other'))}}</td>
					</tr>
					<tr>
						<td>{{__('E-mail')}}</td>
						<td>{{is_exists($item->email)}}</td>
					</tr>
					<tr>
						<td>{{__('Phone number')}}</td>
						<td>{{is_exists($item->phone)}}</td>
					</tr>
					<tr>
						<td>{{__('Address')}}</td>
						<td>{{is_exists($item->address)}}</td>
					</tr>
					<tr>
						<td>{{__('Date of adding')}}  </td>
						<td>{{\Carbon\Carbon::parse($item->created_at)->formatLocalized('%d %B %Y')}}</td>
					</tr>
				</table>
				@endcard
			</div>
		</div>
		<div class="row mt-2">
			<div class="col-12">
				@card(["class" => "h-100"])
				@slot('title')
					<h6 class="m-0">{{ __('Additional information')}}</h6>
				@endslot
				{!! $item->description !!}
					@empty($item->description)
						<div class="text-muted text-center py-4">{{__('Is empty')}}</div>
					@endempty
				@endcard
			</div>
		</div>
	</div>
</div>
