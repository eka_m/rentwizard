@extends('layouts.base')
@section('title', ' | '. __('Customer') . ' | ' .$item->name)
@section('content')
  <div class="m-content">
    @fxportlet(["class" => "m-portlet--tabs shadow-none", "title" => __('Customer'), "icon" => "flaticon-users"])
    @slot('actions')
      <ul class="nav nav-tabs m-tabs m-tabs-line  m-tabs-line--right" role="tablist">
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#information">{{__('Main')}}</a>
        </li>
        {{--<li class="nav-item m-tabs__item">--}}
          {{--<a class="nav-link m-tabs__link" data-toggle="tab" href="#analytics">{{__('Analytics')}}</a>--}}
        {{--</li>--}}
      </ul>
    @endslot
    @slot('body')
      <div class="tab-content">
        <div class="tab-pane fade active show" id="information" role="tabpanel">
          @include('clients.parts.item')
        </div>
        <div class="tab-pane fade" id="analytics" role="tabpanel">

        </div>
      </div>
    @endslot
    @endfxportlet
  </div>
@endsection
