<div class="card shadow-sm {{isset($class) ? $class : ''}}">
	@isset($title)
		<div class="card-header bg-white {{isset($noheaderborder) ? 'border-0' : ''}}">
			{{$title}}
		</div>
	@endisset
	<div class="card-body">
		{{ $slot }}
	</div>
</div>