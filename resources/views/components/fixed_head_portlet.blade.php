<div
	class="m-portlet m-portlet--last m-portlet--brand m-portlet--head-solid-bg m-portlet--head-md
			m-portlet--responsive-mobile {{isset($class) ? $class : ''}} initPortlet"
	id="main_portlet">
	<div class="m-portlet__head shadow fixed-header">
		<div class="m-portlet__head-wrapper">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					@if(isset($icon))
						<span class="m-portlet__head-icon"><i class="{{$icon}}"></i></span>
					@endif
					@if(isset($title))
						<h3 class="m-portlet__head-text">
							{{$title}}
						</h3>
					@endif
				</div>
			</div>
			<div class="m-portlet__head-tools">
				{{isset($actions) ? $actions : ''}}
				<ul class="m-portlet__nav">
					@if(isset($fullscreen) && $fullscreen)
						<li class="m-portlet__nav-item">
							<a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i
									class="la la-expand"></i></a>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</div>
	<div class="m-portlet__body px-0 bg-main">
		{{$body}}
	</div>
</div>