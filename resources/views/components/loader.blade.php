<div>
	<loader type="{{isset($type) ? $type : 'table'}}" v-if="$store.loader"></loader>
	<div v-show="!$store.loader">
		{{ $slot }}
	</div>
</div>