<div class="m-portlet {{isset($class) ? $class : ''}}">
	<div class="m-portlet__head bg-light shadow fixed-header">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					@if(isset($icon))
						<span class="m-portlet__head-icon"><i class="{{$icon}}"></i></span>
					@endif
					@if(isset($title))
						<h3 class="m-portlet__head-text">{{$title}}</h3>
					@endif
				</div>
			<div class="m-portlet__head-tools">
				{{isset($actions) ? $actions : ''}}
				<ul class="m-portlet__nav">
					@if(isset($fullscreen) && $fullscreen)
						<li class="m-portlet__nav-item">
							<a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i
									class="la la-expand"></i></a>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		{{$body}}
	</div>
</div>