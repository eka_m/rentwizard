<div class="m-portlet m-portlet--full-height ">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					{{__("New customers")}}
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body paginatiable-box pb-0" data-url="{{route('async.recentCustomers')}}">

	</div>
</div>