<div class="m-widget4">
	@forelse($items as $item)
		<div class="m-widget4__item">
			<div class="m-widget4__img m-widget4__img--pic">
				<img src="{{$item->thumb}}" alt="" style="width: 50px; height: 50px;" onerror="fallbackImage(this)">
			</div>
			<div class="m-widget4__info">
				<a href="{{route('clients.show', $item->id)}}" class="m-widget4__title m-link m-link--brand">{{$item->name}}</a> <br/>
				<span class="m-widget4__sub">{{\Carbon\Carbon::parse($item->created_at)->formatLocalized('%d %B %Y / %H:%m')}}</span>
			</div>
			<div class="m-widget4__ext w-client-status">
				{!! badge($item->status) !!}
			</div>
		</div>
	@empty
		<div class="text-center">
			{{__("Is empty")}}
		</div>
	@endforelse
</div>

<div class="mt-5 pagination-sm d-flex align-items-center justify-content-end">
	{{$items->links()}}
</div>