<div class="m-widget4">
	@forelse($items as $item)
		<div class="m-widget4__item">
			<div class="m-widget4__info">
				<a href="{{route('deals.show', $item->id)}}" class="m-widget4__title m-link m-link--focus">{{$item->client->name}}</a> <br/>
				<span class="m-widget4__sub">
						<div class="badge badge-dark p-1">{{$item->hash}}</div>
					{{\Carbon\Carbon::parse($item->created_at)->formatLocalized('%d %B %Y / %H:%m')}}
				</span>
			</div>
			<div class="m-widget4__ext">
				{!! badge($item->status) !!}
			</div>
		</div>
	@empty
		<div class="text-center">
			{{__("Is empty")}}
		</div>
	@endforelse
</div>

<div class="mt-5 pagination-sm d-flex align-items-center justify-content-end">
	{{$items->links()}}
</div>