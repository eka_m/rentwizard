@extends('layouts.base')
@section('title', ' | ' . __('Dashboard'))

@section('content')
	<div class="m-content dashboard">
		<div class="row">
			<div class="col-md-6">
				<div class="card m--bg-brand">
					<div class="card-body p-0">
						@include('dashboard.clock')
					</div>
				</div>
			</div>
			<div class="col-md-6 ">
				<div class="row h-100">
					<div class="col-md-4 h-100">
						<a href="{{route('deals.create')}}" class="m-link d-block h-100">
							<div class="card h-100">
								<div class="card-body h-100 d-flex align-items-center justify-content-center">
									<div class="text-center">
										<i class="flaticon-share m--font-brand" style="font-size:46px;"></i>
										<div>{{__('New')}} {{mb_strtolower(__('Deal'))}}</div>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-md-4">
						<a href="{{route('inventory.create')}}" class="m-link d-block h-100">
							<div class="card h-100">
								<div class="card-body h-100 d-flex align-items-center justify-content-center">
									<div class="text-center">
										<i class="flaticon-open-box" style="font-size:46px;"></i>
										<div>{{__('New')}} {{mb_strtolower(__('Item'))}}</div>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-md-4">
						<a href="{{route('clients.create')}}" class="m-link d-block h-100">
							<div class="card h-100">
								<div class="card-body h-100 d-flex align-items-center justify-content-center">
									<div class="text-center">
										<i class="flaticon-user-add" style="font-size:46px;"></i>
										<div>{{__('New')}} {{mb_strtolower(__('Client'))}}</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body p-0">
						@include('dashboard.profit')
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-md-6">
				@include('dashboard.deals.deals')
			</div>
			<div class="col-md-6">
				@include('dashboard.clients.clients')
			</div>
		</div>
	</div>

@endsection