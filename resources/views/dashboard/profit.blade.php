<div class="row m-row--no-padding m-row--col-separator-xl">
	<div class="col-md-4">
		<div class="m-widget24 pb-5">
			<div class="m-widget24__item">
				<h4 class="m-widget24__title">
					{{__('Today')}}
				</h4><br>
				<span class="m-widget24__desc text-capitalize"> {{\Carbon\Carbon::now()->formatLocalized("%d %A")}}	</span>
				<span class="m-widget24__stats m--font-brand">+	{{collect($today)->sum("price")}} {!! currencySymbol() !!}
							<small class="m-widget24__desc d-block text-right" style="font-size: 12px;">{{__('Deals')}}: <span
												class="m--font-info m--font-bold">+ {{count($today)}}</span></small>
				</span>
				<div class="m--space-10"></div>
				<div class="progress m-progress--sm">
					<div class="progress-bar m--bg-brand" role="progressbar"
					     style="width: {{until(\Carbon\Carbon::now()->startOfDay(), \Carbon\Carbon::now()->endOfDay())}}%;"
					     aria-valuenow="50"
					     aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="m-widget24">
			<div class="m-widget24__item">
				<h4 class="m-widget24__title">
					{{__("Week")}}
				</h4><br>
				<span class="m-widget24__desc">{{__('This Week')}} </span>
				<span class="m-widget24__stats m--font-info">+	{{collect($week)->sum("price")}} {!! currencySymbol() !!}
						<small class="m-widget24__desc d-block text-right" style="font-size: 12px;">{{__('Deals')}}: <span
											class="m--font-info m--font-bold">+ {{count($week)}}</span></small>
				</span>

				<div class="m--space-10"></div>
				<div class="progress m-progress--sm">
					<div class="progress-bar m--bg-info" role="progressbar"
					     style="width: {{until(\Carbon\Carbon::now()->startOfWeek(), \Carbon\Carbon::now()->endOfWeek())}}%;"
					     aria-valuenow="50"
					     aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="m-widget24">
			<div class="m-widget24__item">
				<h4 class="m-widget24__title">
					{{__('Month')}}
				</h4><br>
				<span class="m-widget24__desc">
												{{__('This Month')}}
												</span>
				<span class="m-widget24__stats m--font-focus">
													{{collect($month)->sum("price")}} {!! currencySymbol() !!}
					<small class="m-widget24__desc d-block text-right" style="font-size: 12px;">{{__('Deals')}}: <span
										class="m--font-info m--font-bold">+ {{count($month)}}</span></small>
												</span>
				<div class="m--space-10"></div>
				<div class="progress m-progress--sm">
					<div class="progress-bar m--bg-focus" role="progressbar"
					     style="width: {{until(\Carbon\Carbon::now()->startOfMonth(), \Carbon\Carbon::now()->endOfMonth())}}%;"
					     aria-valuenow="50"
					     aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				{{--<span class="m-widget24__change">--}}
				{{--Change--}}
				{{--</span>--}}
				{{--<span class="m-widget24__number">--}}
				{{--69%--}}
				{{--</span>--}}
			</div>
		</div>
	</div>
</div>