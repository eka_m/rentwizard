@fxportlet(["class" => "shadow-none", "title" => __('Deal'), "icon" => 'flaticon-share'])
@slot('actions')
	<a href="{{ url()->previous() }}"
	   class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
					<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('Back')}}</span>
					</span>
	</a>
	<button type="submit" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
					<span>
						<i class="la la-check"></i>
						<span>{{__('Save')}}</span>
					</span>
	</button>
@endslot
@slot('body')
	<div class="row">
		<div class="col-12">
			<ul class="nav nav-tabs nav-pills nav-fill nav-pills--brand" role="tablist">
				<li class="nav-item">
					<a class="nav-link active show" data-toggle="tab" href="#main">{{__('Main')}}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " data-toggle="tab" href="#details">{{__('Details')}}</a>
				</li>
			</ul>
		</div>
	</div>


	<div class="tab-content">
		<div class="tab-pane fade active show" id="main" role="tabpanel">
			<div class="row">
				<div class="col-md-6 mb-2">
					@card(['class' => 'mb-2'])
					@slot('title')
						{{__('Customer')}}
					@endslot
					<clients-list prop-statuses="{{json_encode($clientstatuses)}}"
					              @if($item->client) prop-client="{{json_encode($item->client)}}" @endif>
					</clients-list>
					@endcard
					@card(['class' => 'mb-2'])
					@slot('title')
						{{__('Date range')}}
					@endslot
					<deal-dates prop-start="{{$item->start}}"
					            prop-end="{{$item->end}}"></deal-dates>
					@endcard
					@card
					@slot('title')
						{{__('Inventory')}}
					@endslot
					<inventory-list prop-deal="{{$item}}"
					                prop-statuses="{{json_encode($inventorytstatuses)}}"></inventory-list>
					@endcard
				</div>
				<div class="col-md-6">
					@card
					<editor
									name="description"
									placeholder="{{__("Type something")}}"
									prop-content="{{old('description', $item->description)}}"
									:prop-config="{btns: [['strong', 'em', 'foreColor']]}"></editor>
					@endcard
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="details" role="tabpanel">
			<div class="row">
				<div class="col-md-9 mb-2">
					<deal-params prop-deal="{{json_encode($item)}}"></deal-params>
				</div>
				<div class="col-md-3">
					<div class="row mb-2">
						<div class="col-12">
							@card
							@slot('title')
								{{__('Manager profit')}}
							@endslot
							<deal-manager-profit prop-deal="{{json_encode($item)}}"></deal-manager-profit>
							@endcard
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@card
							<div class="input-group justify-content-center">
								<div class="switch">
									<input
													type="checkbox"
													data-size="normal"
													data-inverse="true"
													data-label-text="Автоактивация"
													data-on-text="Вкл"
													data-off-text="Выкл"
													data-on-color="success"
													data-off-color="metal">
									<input type="hidden" name="autoactivation" data-val="{{$item->autoactivation}}">
								</div>
								<input type="hidden" name="status" value="{{$item->status}}">
							</div>
							@endcard
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endslot
@endfxportlet