@extends('layouts.base')
@section('title',' | Сделки')
@section('content')
	<div class="m-content">
		<form action="{{route('deals.store')}}" method="POST" class="form validate">
			@method('POST')
			@csrf
			@include('deals._form')
		</form>
	</div>
@endsection