@extends('layouts.base')
@section('title',' | '. __('Deal') .' |'.$item->hash)
@section('content')
	<div class="m-content">
		<div class="m-portlet m-portlet--full-height">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							{{__('Deal')}}: &nbsp; <span class="text-info">{{$item->hash}}</span>
							@role('root')
							<span>&nbsp; | &nbsp; {{__('Manager')}}: &nbsp;
                <span class="m--font-brand">{{$item->manager->name}}</span>
              </span>
							@endrole
						</h3>
					</div>
				</div>
			</div>
			<form action="{{route('deals.update',$item->id)}}" method="POST" class="form validate">
				@method('PUT')
				@csrf
				@include('deals._form')
			</form>
		</div>
	</div>
@endsection