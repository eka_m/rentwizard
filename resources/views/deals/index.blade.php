@extends('layouts.base')
@section('title', ' | '. __('Deals'))

@section('content')
	<div class="m-content">
		<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg initPortlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon"><i class="flaticon-share"></i></span>
						<h3 class="m-portlet__head-text">
							{{__('Deals')}}
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="{{route('deals.create')}}" class="m-portlet__nav-link m-portlet__nav-link--icon">
								<i class="la la-plus"></i></a>
						</li>
						<li class="m-portlet__nav-item">
							<a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i
												class="la la-expand"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				@load
				<deals-table :prop-statuses="{{json_encode($statuses)}}"></deals-table>
				@endload
			</div>
		</div>
	</div>
@endsection