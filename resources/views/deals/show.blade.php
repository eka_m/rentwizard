@extends('layouts.base')
@section('title', ' | ' . __('Deal'))

@section('content')
	<div class="m-content bg-white">
		<div class="row">
			<div class="col">
				<div class="m-invoice-2">
					<div class="m-invoice__wrapper">
						<div class="m-invoice__head">
							<div class="m-invoice__container m-invoice__container--centered">
								<div class="m-invoice__logo mb-5">
									<a href="#">
										<h1>
											{{__('Contract')}}
											<span class="text-warning" style="font-size:20px;">{{$item->hash}}</span>
										</h1>

									</a>
									<a href="#">
										{{--<img src="{{asset('/img/logo-icon.png')}}">--}}
									</a>
								</div>
								{{--<span class="m-invoice__desc">--}}
								{{--<h5>Mandarin Rent</h5>--}}
								{{--<span>--}}
								{{--Ünvan, Bakı, Azərbaycan--}}
								{{--</span>--}}
								{{--<span>--}}
								{{--info@mandarinagency.az--}}
								{{--</span>--}}
								{{--</span>--}}

								<div class="m-invoice__items">
									<div class="m-invoice__item">
                                    <span class="m-invoice__subtitle">
                                                {{__('Customer')}}
                                        </span>
										<span class="m-invoice__text">{{$client->name}}</span>
									</div>
									<div class="m-invoice__item">
                                    <span class="m-invoice__subtitle">
                                               {{__("Passport number")}}
                                            </span>
										<span class="m-invoice__text">
                                                <p class="m-badge m-badge--wide m-badge--accent">{{is_exists($client->passport_number)}}</p>
                                            </span>
									</div>
									<div class="m-invoice__item">
                                    <span class="m-invoice__subtitle">
                                        {{__('Contacts')}}
                                        </span>
										<span class="m-invoice__text">
                          <div><small>{{__('Phone number')}}:</small>{{is_exists($client->phone)}}</div>
                          <div><small>{{__('E-mail')}}:</small> {{is_exists($client->adress)}}</div>
                     </span>
									</div>
								</div>

							</div>
						</div>
						<div class="m-invoice__body m-invoice__body--centered">
							<div class="table-responsive">
								<table class="table">
									<thead>
									<tr>
										<th>
											{{__('Item')}}
										</th>
										<th>
											{{__('Сondition')}}
										</th>
										<th>
											{{__('Price')}}
										</th>
									</tr>
									</thead>
									<tbody>
									@foreach($dealitems as $dealitem)
										<tr>
											<td>
												#{{$dealitem->id}}  {{$dealitem->name}}
											</td>
											<td>
												<span class="m-badge m-badge--wide m-badge--{{__('classes.'.$dealitem->state)}}">{{__($dealitem->state)}}</span>
											</td>
											<td>
												{!! $dealitem->cost !!}
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="table-responsive mt-5">
								<div class="m-invoice__table  m-invoice__table--centered table-responsive">
									<table class="table">
										<thead>
										<tr>
											<th>
												{{(__('Start date'))}}
											</th>
											<th class="text-left">
												{{__('End date')}}
											</th>
											<th>
												{{__('Price')}}
											</th>
											<th>
												{{(__('Sale'))}}
											</th>
											<th>
												{{(__('Final price'))}}
											</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td>
												{{Carbon\Carbon::parse($item->start)->format('d-m-Y / h:i')}}
											</td>
											<td class="text-left">
												{{Carbon\Carbon::parse($item->end)->format('d-m-Y / h:i')}}
											</td>
											<td>
												{!! $item->cost !!}
											</td>
											<td>
												{{$item->sale}} %
											</td>
											<td class="m--font-danger">
												{!! $item->realcost !!}
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							@if($item->description)
								<div class="table-responsive mt-5">
									<div class="m-invoice__table  m-invoice__table--centered table-responsive">
										<table class="table">
											<thead>
											<tr>
												<th>
													{{__('Note')}}:
												</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td>
													{!! $item->description !!}
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							@endif
						</div>
						<div class="m-invoice__footer">
							<div class="m-invoice__table  m-invoice__table--centered table-responsive">
								<table class="table">
									<thead>
									<tr>
										<th>
											{{__('Date')}}
										</th>
										<th>
											{{__('Signature')}}
										</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>
											</br>
										</td>
										<td>
											</br>
										</td>
									</tr>
									</tbody>
								</table>

							</div>
						</div>

					</div>
				</div>
				<p style="font-size:12px; text-align:right;">№ {{$item->hash}}</p>
			</div>
		</div>
	</div>
@endsection