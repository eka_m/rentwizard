<div class="row">
	<div class="col-md-4">
		@card(["noheaderborder" => true])
		@slot('title')
			<ul class="nav nav-tabs mb-0" role="tablist">
				<li class="nav-item">
					<a class="nav-link active show" data-toggle="tab" href="#base" role="tab"
					   aria-selected="true">
						{{__("Main")}}
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#attr" role="tab" aria-selected="false">
						{{__('Attributes')}}
					</a>
				</li>
			</ul>
		@endslot
		<div class="tab-content">
			<div class="tab-pane active show" id="base">
				<div class="form-group">
					<label>{{__('Name')}}:*</label>
					<input type="text" id="prod-name" class="form-control m-input" name="name" placeholder="{{__('Name')}}"
					       required
					       value="{{old('name', $item->name)}}">
				</div>
				<div class="form-group mt-4">
					<label for="category_id">{{__('Category')}}</label>
					<select name="category_id" id="category_id" class="form-control m-select2 selectPicker"
					        data-live-search="true">
						<option value="0">{{__('Other')}}</option>
						@foreach($categories as $category)
							<option
											{{$item->category && $item->category->id == $category->id ? 'selected' : '' }} value="{{$category->id}}">{{$category->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group mt-4">
					<label>{{__('Purchase date')}}</label>
					<div class="m-input-icon m-input-icon--right date">
						<input type="text"
						       class="form-control m-input dateInput"
						       name="purchase_date"
						       data-options='{"format": "yyyy-mm-dd"}'
						       value="{{$item->purchase_date}}"
						       readonly
						       placeholder="{{__('Purchase date')}}">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i
												class="la la la-calendar-check-o"></i></span></span>
					</div>
				</div>
				<div class="form-group mt-4">
					<label>{{__('Purchase price')}}</label>
					<input type="text" class="form-control m-input" name="purchase_price" placeholder="{{__('Purchase price')}}"
					       value="{{old('purchase_price', $item->purchase_price)}}" data-rule-number="true">
				</div>
				<div class="form-group mt-4">
					<label>{{__('Rent price')}}* </label>
					<div class="input-group">
						<input type="text" class="form-control m-input" name="rent_price" placeholder="{{__('Rent price')}}"
						       value="{{old('rent_price' , $item->rent_price)}}" data-rule-number="true" required>
						<div class="input-group-append">
							<select name="rent_per"
							        class="form-control m-select2 selectPicker" data-width="110">
								<option value="Day" data-content='<span class="text-brand">{{__('Day')}}</span>'
												{{$item->rent_per == 'Day' ? 'selected' : '' }}>{{__('Day')}}
								</option>
								<option value="Hour" data-content='<span class="text-info">{{__('Hour')}}</span>'
												{{$item->rent_per == 'Hour' ? 'selected' : '' }}>{{__('Hour')}}
								</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group mt-4 ">
					<label for="state">{{__('Сondition')}}</label>
					<select name="state" id="state" class="form-control m-select2 selectPicker">
						@foreach($states as $state)
							<option {{$item->state == $state ? 'selected' : '' }}
							        data-content='<span class="m-badge m-badge--wide m-badge--{{__("classes.".$state)}}">{{__($state)}}</span>'
							        {{$item->state == $state ? 'selected' : '' }}
							        value="{{$state}}"> {{__($state)}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group mt-4">
					<label for="status">{{__('Status')}}</label>
					<select name="status" id="status" class="form-control m-select2 selectPicker">
						@foreach($statuses as $status)
							@if($item->status == 'Rented' || $item->status == 'Reserved' ) @continue @endif
							<option {{$item->status == $status ? 'selected' : '' }}
							        {{$status == 'Rented' || $status == 'Reserved' ? 'disabled' : ''}}
							        value="{{$status}}"
							        {{$item->status == $status ? 'selected' : '' }}
							        data-content='<span class="m-badge m-badge--wide m-badge--{{__("classes.".$status)}}"> {{__($status)}}</span>'>
								{{__($status)}}
							</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="tab-pane" id="attr">
				@foreach($attributes as $attribute)
					<div class="form-group mt-4">
						<label>{{$attribute->name}}</label>
						<input type="text" class="form-control m-input" name="attributes[{{$attribute->slug}}]"
						       placeholder="{{$attribute->name}}"
						       value="{{isset($item->attributes[$attribute->slug]) ? $item->attributes[$attribute->slug] : ''}}">
					</div>
				@endforeach
			</div>
		</div>
		@endcard
	</div>
	<div class="col-md-8">
		<div class="m-portlet">
			<div class="m-portlet__body">
				<upload-manager
					prop-endpoint="{{route('async.upload.tmp', ['input'=> 'files', 'collection' => 'inventory', 'temporary' => 24*60])}}"
					:prop-files="{{json_encode($item->media)}}"
				/>
			</div>
		</div>
		<div class="m-portlet">
			<div class="m-portlet__body pt-5">
				<editor
					name="description"
					placeholder="{{__('Additional information')}}"
					prop-content="{{old('description', $item->description)}}"
					:prop-config="{btns: [['strong', 'em', 'foreColor']]}"></editor>
			</div>
		</div>
	</div>
</div>