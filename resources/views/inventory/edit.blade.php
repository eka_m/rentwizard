@extends('layouts.base')
@section('title', @trans('Inventory') .' | ' . @trans('Item') .' | '. $item->name)
@section('content')
	<div class="m-content">
		<form action="{{route('inventory.update', $item->id)}}" method="post" class="validate has-upload_manager">
			@method('PUT')
			@csrf
			@fxportlet([
			"class" => " shadow-none",
			"title" => __('Inventory'),
			"icon" => "flaticon-open-box",
			"fullscreen" => true,
			])
			@slot('body')
				@include('inventory._form')
			@endslot

			@slot("actions")
				<a href="{{ url()->previous() }}"
				   class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
						<span>
							<i class="la la-arrow-left"></i>
							<span>{{__('Back')}}</span>
						</span>
				</a>
				<button type="submit" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
					<span>
						<i class="la la-check"></i>
						<span>{{__('Save')}}</span>
					</span>
				</button>
			@endslot
			@endfxportlet
		</form>
	</div>
@endsection