@extends('layouts.base')
@section('title', ' | '. __('Inventory'))
@section('js')
	@parent
	<script type="text/javascript">
	  Vue.$store.categories.set( @json($categories) );
	</script>
@endsection
@section('content')
	<div class="m-content">
		<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg initPortlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon"><i class="flaticon-open-box"></i></span>
						<h3 class="m-portlet__head-text">
							{{__('Inventory')}}
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<categories/>
						</li>
						<li class="m-portlet__nav-item">
							<a href="{{route('inventory.create')}}"
							   data-toggle="m-tooltip"
							   data-placement="top"
							   data-html="true"
							   data-skin="light"
							   data-original-title='{{__('Add')}}'
							   class="m-portlet__nav-link m-portlet__nav-link--icon">
								<i class="la la-plus"></i></a>
						</li>
						<li class="m-portlet__nav-item">
							<a href="#"
							   m-portlet-tool="fullscreen"
							   class="m-portlet__nav-link m-portlet__nav-link--icon">
								<i class="la la-expand"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				@load
					<inventory-table :prop-attributes="{{ json_encode($attributes) }}"
					           :prop-statuses="{{json_encode($statuses)}}"
					           :prop-categories="{{json_encode($categories)}}"
					></inventory-table>
				@endload
			</div>
		</div>
	</div>

@endsection
