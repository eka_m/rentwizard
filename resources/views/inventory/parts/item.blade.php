<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-12 ">
				@card(["class" => "h-100"])
				@slot('title')
					<h6 class="m-0">{{__('Images')}}</h6>
				@endslot
				<fotorama data-allowfullscreen="native" data-nav="thumbs" :prop-media="{{$item->media->toJson()}}">
				</fotorama>
				@endcard
			</div>
		</div>
	</div>
	<div class="col-md-4">
		@card(["class" => "h-100"])
		@slot('title')
			<ul role="tablist" class="nav nav-tabs">
				<li class="nav-item">
					<a data-toggle="tab" href="#base" class="nav-link m-tabs__link active show">{{ __('Description')}}</a></li>
				<li class="nav-item"><a data-toggle="tab" href="#attributes"
				                        class="nav-link m-tabs__link">{{__('Attributes')}}</a>
				</li>
			</ul>
		@endslot
		<div class="tab-content">
			<div class="tab-pane fade active show" id="base">
				<table class="attributes-table table table-bordered">
					<tr>
						<td>#</td>
						<td>{{$item->id}}</td>
					</tr>
					<tr>
						<td>Название</td>
						<td>{{$item->name}}</td>
					</tr>
					<tr>
						<td>Категория</td>
						<td>{{is_exists_arr($item->category,'name', __('Other'))}}</td>
					</tr>
					<tr>
						<td>Дата приобретения</td>
						<td>{{\Date::parse($item->purchase_date)->format('d M Y')}}</td>
					</tr>
					<tr>
						<td>Цена приобретения</td>
						<td>{{is_exists($item->purchase_price)}} <i
											class="fa">{{html_entity_decode(setting('currencies.list.0.code'))}}</i></td>
					</tr>
					<tr>
						<td>Цена Аренды</td>
						<td>{!! $item->cost !!}</td>
					</tr>
				</table>
			</div>
			<div class="tab-pane fade" id="attributes">
				<table class="attributes-table table table-bordered">
					@foreach($attributes as $attribute)
						<tr>
							<td>{{$attribute->name}}</td>
							<td>{{$item->attributes[$attribute->slug]}}</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		@endcard
	</div>
	<div class="col-md-4">
		@card(["class" => "h-100"])
		@slot('title')
			<h6 class="m-0">{{ __('Additional information')}}</h6>
		@endslot
		{!! $item->description !!}
		@empty($item->description)
			<div class="text-muted text-center py-4">{{__('Is empty')}}</div>
		@endempty
		@endcard
	</div>
</div>