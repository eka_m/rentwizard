<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Rent Wizard - {!! __("Smart service to automate rental business") !!}</title>
	<meta name="keywords" content="{{__("meta.keywords")}}">
	<meta name="description" content="{{__("meta.description")}}">
	<meta name="google-site-verification" content="wOju070mfABPlHusWcrVzpDemhJaRmqh4YHtTsMuIzE" />
	<!-- Stylesheets -->
	<link href="{{asset('web/css/bootstrap.css')}}" rel="stylesheet">
	@stack('css')
	<link href="{{asset('web/css/app.min.css')}}" rel="stylesheet">
	<link href="{{asset('web/css/responsive.css')}}" rel="stylesheet">
	<link href="{{asset('web/css/flags.min.css')}}" rel="stylesheet">
	<link href="{{asset('web/js/toastr/toastr.css')}}" rel="stylesheet">
	<link href="{{asset('web/js/driverjs/driver.min.css')}}" rel="stylesheet">


<!-- Responsive -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="apple-touch-icon-precomposed" sizes="57x57"
	      href="{{asset('web/images/favicon/apple-touch-icon-57x57.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="114x114"
	      href="{{asset('web/images/favicon/apple-touch-icon-114x114.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="72x72"
	      href="{{asset('web/images/favicon/apple-touch-icon-72x72.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="144x144"
	      href="{{asset('web/images/favicon/apple-touch-icon-144x144.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="60x60"
	      href="{{asset('web/images/favicon/apple-touch-icon-60x60.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="120x120"
	      href="{{asset('web/images/favicon/apple-touch-icon-120x120.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="76x76"
	      href="{{asset('web/images/favicon/apple-touch-icon-76x76.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="152x152"
	      href="{{asset('web/images/favicon/apple-touch-icon-152x152.png')}}"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-196x196.png')}}" sizes="196x196"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-96x96.png')}}" sizes="96x96"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-32x32.png')}}" sizes="32x32"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-16x16.png')}}" sizes="16x16"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-128.png')}}" sizes="128x128"/>
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF"/>
	<meta name="msapplication-TileImage" content="{{asset('web/images/favicon/mstile-144x144.png')}}"/>
	<meta name="msapplication-square70x70logo" content="{{asset('web/images/favicon/mstile-70x70.png')}}"/>
	<meta name="msapplication-square150x150logo" content="{{asset('web/images/favicon/mstile-150x150.png')}}"/>
	<meta name="msapplication-wide310x150logo" content="{{asset('web/images/favicon/mstile-310x150.png')}}"/>
	<meta name="msapplication-square310x310logo" content="{{asset('web/images/favicon/mstile-310x310.png')}}"/>
	<meta name="description" content="{{__("Rent Wizard is designed for administrators and owners of rental points and rental of any inventory. The service automates the accounting of goods issued by customers, maintains a history of payments and settlements, generating all the necessary reports for the manager.")}}
		">

	<!--[if lt IE 9]>
	<script src="{{asset('web/https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js')}}"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="{{asset('web/js/respond.js')}}"></script>
	<![endif]-->
</head>

<body>

<div class="page-wrapper">
	<div class="preloader"></div>
	@include("web.partials.header")
	@yield('content')
	@include("web.partials.footer")
</div>
<!--End pagewrapper-->
@guest
	@include('web.partials.modal-login')
@endguest
<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target bg-main-two" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="{{asset('web/js/jquery.js')}}"></script>
<script src="{{asset('web/js/popper.min.js')}}"></script>
<script src="{{asset('web/js/bootstrap.min.js')}}"></script>
<script src="{{asset('web/js/pagenav.js')}}"></script>
<script src="{{asset('web/js/jquery.scrollTo.js')}}"></script>
{{--<script src="{{asset('web/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>--}}
<script src="{{asset('web/js/appear.js')}}"></script>
<script src="{{asset('web/js/owl.js')}}"></script>
<script src="{{asset('web/js/wow.js')}}"></script>
<script src="{{asset('web/js/toastr/toastr.min.js')}}"></script>
@stack('js')
<script src="{{asset('web/js/mask/jquery.mask.min.js')}}"></script>
<script src="{{asset('web/js/bstour/bootstrap-tour.min.js')}}"></script>
<script src="{{asset('web/js/app.min.js')}}"></script>
{!! Toastr::render() !!}

</body>
</html>