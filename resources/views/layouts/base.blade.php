<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<!-- begin::Head -->
<head>
	<meta charset="utf-8"/>
	<title>
		{{env("APP_NAME")}} @yield('title')
	</title>
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load( {
			google: { "families": [ "Montserrat:300,400,500,600,700","Open Sans:300,400,500,600,700" ] },
			active: function () {
				sessionStorage.fonts = true;
			}
		} );
	</script>
	<link rel="apple-touch-icon-precomposed" sizes="57x57"
	      href="{{asset('web/images/favicon/apple-touch-icon-57x57.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="114x114"
	      href="{{asset('web/images/favicon/apple-touch-icon-114x114.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="72x72"
	      href="{{asset('web/images/favicon/apple-touch-icon-72x72.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="144x144"
	      href="{{asset('web/images/favicon/apple-touch-icon-144x144.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="60x60"
	      href="{{asset('web/images/favicon/apple-touch-icon-60x60.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="120x120"
	      href="{{asset('web/images/favicon/apple-touch-icon-120x120.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="76x76"
	      href="{{asset('web/images/favicon/apple-touch-icon-76x76.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="152x152"
	      href="{{asset('web/images/favicon/apple-touch-icon-152x152.png')}}"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-196x196.png')}}" sizes="196x196"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-96x96.png')}}" sizes="96x96"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-32x32.png')}}" sizes="32x32"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-16x16.png')}}" sizes="16x16"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-128.png')}}" sizes="128x128"/>
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF"/>
	<meta name="msapplication-TileImage" content="{{asset('web/images/favicon/mstile-144x144.png')}}"/>
	<meta name="msapplication-square70x70logo" content="{{asset('web/images/favicon/mstile-70x70.png')}}"/>
	<meta name="msapplication-square150x150logo" content="{{asset('web/images/favicon/mstile-150x150.png')}}"/>
	<meta name="msapplication-wide310x150logo" content="{{asset('web/images/favicon/mstile-310x150.png')}}"/>
	<meta name="msapplication-square310x310logo" content="{{asset('web/images/favicon/mstile-310x310.png')}}"/>
	@include('partials.css')
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body
	class="m-page--fluid m--skin- m-content--skin-dark m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div id="app" class="m-grid m-grid--hor m-grid--root m-page">
	<vue-progress-bar></vue-progress-bar>
@include('partials.header')

<!-- begin::Body -->

	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		@include('partials.aside')
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
			<div class="m-content">
				@yield('left-aside')
				@yield('content')
			</div>
		</div>
	</div>

	<!-- end:: Body -->
	<!-- begin::Footer -->

@include('partials.footer')

<!-- end::Footer -->
	<offcanvas></offcanvas>
</div>
<!-- end:: Page -->

<!-- begin::Quick Sidebar -->
@include("partials.quick-sidebar")

<!-- end::Quick Sidebar -->

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
	<i class="la la-arrow-up"></i>
</div>

<!-- end::Scroll Top -->

<!-- begin::Quick Nav -->
{{--@include('partials.quick-nav')--}}

@include("partials.js")
</body>
<!-- end::Body -->
</html>