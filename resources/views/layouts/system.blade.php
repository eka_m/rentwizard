<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<!-- begin::Head -->
<head>
	<meta charset="utf-8"/>
	<title>
		{{env("APP_NAME")}} @yield('title')
	</title>
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
	  WebFont.load( {
		  google: { "families": [ "Montserrat:300,400,500,600,700","Open Sans:300,400,500,600,700" ] },
		  active: function () {
			  sessionStorage.fonts = true;
		  }
	  } );
	</script>
	<link rel="apple-touch-icon-precomposed" sizes="57x57"
	      href="{{asset('web/images/favicon/apple-touch-icon-57x57.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="114x114"
	      href="{{asset('web/images/favicon/apple-touch-icon-114x114.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="72x72"
	      href="{{asset('web/images/favicon/apple-touch-icon-72x72.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="144x144"
	      href="{{asset('web/images/favicon/apple-touch-icon-144x144.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="60x60"
	      href="{{asset('web/images/favicon/apple-touch-icon-60x60.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="120x120"
	      href="{{asset('web/images/favicon/apple-touch-icon-120x120.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="76x76"
	      href="{{asset('web/images/favicon/apple-touch-icon-76x76.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="152x152"
	      href="{{asset('web/images/favicon/apple-touch-icon-152x152.png')}}"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-196x196.png')}}" sizes="196x196"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-96x96.png')}}" sizes="96x96"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-32x32.png')}}" sizes="32x32"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-16x16.png')}}" sizes="16x16"/>
	<link rel="icon" type="image/png" href="{{asset('web/images/favicon/favicon-128.png')}}" sizes="128x128"/>
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF"/>
	<meta name="msapplication-TileImage" content="{{asset('web/images/favicon/mstile-144x144.png')}}"/>
	<meta name="msapplication-square70x70logo" content="{{asset('web/images/favicon/mstile-70x70.png')}}"/>
	<meta name="msapplication-square150x150logo" content="{{asset('web/images/favicon/mstile-150x150.png')}}"/>
	<meta name="msapplication-wide310x150logo" content="{{asset('web/images/favicon/mstile-310x150.png')}}"/>
	<meta name="msapplication-square310x310logo" content="{{asset('web/images/favicon/mstile-310x310.png')}}"/>
	@include('partials.css')
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body
				class="m-page--fluid m--skin- m-content--skin-dark m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div id="app" class="m-grid m-grid--hor m-grid--root m-page">
	<vue-progress-bar></vue-progress-bar>

<!-- begin::Body -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="#"><img src="{{asset("web/images/logo.png")}}" alt=""></a>
		<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
			<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">
				<li class="nav-item">
					<a class="nav-link" href="https://logout:logout@rent-wizard.com/system/dashboard">Выйти</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
			<div class="m-content">
				@yield('content')
			</div>
		</div>
	</div>
</div>
<!-- end:: Page -->


<!-- begin::Quick Nav -->
{{--@include('partials.quick-nav')--}}
@include("partials.system-js")
</body>
<!-- end::Body -->
</html>