<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
					class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

	<!-- BEGIN: Brand -->
	<div class="m-brand">
		<a href="{{route('dashboard')}}" class="m-brand__logo appLogo">
			{{--<img alt="" src="{{asset('web/images/icons/preloader.svg')}}" class="img-fluid d-block m-auto"--}}
			     {{--style="width: 60px;"/>--}}
			<simple-svg filepath="{{asset('web/images/icons/preloader.svg')}}" width="60px"></simple-svg>
			<span class="offline-notification d-inline-block pt-3"><span>&#8226;</span><span
								class="offline-notification-status">{{__('Online')}}</span></span>
		</a>
	</div>

	<!-- END: Brand -->

	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
	     data-menu-vertical="true" m-menu-scrollable="true" m-menu-dropdown-timeout="500">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__item {{setActive('dashboard', 'm-menu__item--active')}}">
				<a href="{{route('dashboard')}}" class="m-menu__link">
					<span class="m-menu__item-here"></span>
					<i class="m-menu__link-icon flaticon-dashboard"></i>
					<span class="m-menu__link-text">{{__('Dashboard')}}</span>
				</a>
			</li>
			<li class="m-menu__item {{setActive('statistics', 'm-menu__item--active')}}">
				<a href="@can('View stats'){{route('statistics')}}@else javascript:void(0) @endif" class="m-menu__link @can('View stats')@else lock @endcan">
					<span class="m-menu__item-here"></span>
					<i class="m-menu__link-icon flaticon-analytics"></i>
					<span class="m-menu__link-text">{{__('Analytics')}}</span>
				</a>
			</li>
			<li class="m-menu__item {{setActive('deals', 'm-menu__item--active')}}">
				<a href="@can('View deals'){{route('deals.index')}}@else javascript:void(0) @endif" class="m-menu__link @can('View deals')@else lock @endcan">
					<span class="m-menu__item-here"></span>
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">{{__('Deals')}}</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom {{setActive('inventory', 'm-menu__item--active')}}"
			    aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1">
				<a href="javascript:;" class="m-menu__link m-menu__toggle @can('View inventory')@else lock @endcan">
					<i class="m-menu__link-icon flaticon-open-box"></i>
					<span class="m-menu__link-text">{{__('Inventory')}}</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true"
						    m-menu-link-redirect="1"><span class="m-menu__link">
								<span class="m-menu__link-text">{{__('Inventory')}}</span></span></li>
						<li class="m-menu__item {{setActive('inventory', 'm-menu__item--active')}}" aria-haspopup="true" m-menu-link-redirect="1">
							<a href="@can('View inventory'){{route('inventory.index')}}@else javascript:void(0) @endif" class="m-menu__link @can('View inventory')@else lock @endcan"><i
												class="m-menu__link-icon flaticon-open-box"></i><span></span></i>
								<span class="m-menu__link-text">{{__('Inventory')}}</span>
							</a>
						</li>
						<li class="m-menu__item {{setActive('attributes', 'm-menu__item--active')}}" aria-haspopup="true" m-menu-link-redirect="1">
							<a href="@can('Create attributes'){{route('attributes.index')}}@else javascript:void(0) @endif" class="m-menu__link @can('Create attributes')@else lock @endcan">
								<span class="m-menu__item-here"></span>
								<i class="m-menu__link-icon flaticon-web"></i>
								<span class="m-menu__link-text">{{__('Attributes')}}</span>
							</a>
						</li>
						<li class="m-menu__item {{setActive('categories', 'm-menu__item--active')}}" aria-haspopup="true" m-menu-link-redirect="1">
							<a href="@can('Create categories'){{route('categories.index')}}@else javascript:void(0) @endif" class="m-menu__link @can('Create categories')@else lock @endcan">
								<span class="m-menu__item-here"></span>
								<i class="m-menu__link-icon flaticon-attachment"></i>
								<span class="m-menu__link-text">{{__('Categories')}}</span>
							</a>
						</li>
					</ul>
				</div>
			</li>

			<li class="m-menu__item {{setActive('clients', 'm-menu__item--active')}} ">
				<a href="@can('View clients'){{route('clients.index')}}@else javascript:void(0) @endcan" class="m-menu__link @can('View clients')@else lock @endcan">
					<span class="m-menu__item-here"></span>
					<i class="m-menu__link-icon flaticon-users"></i>
					<span class="m-menu__link-text">
						{{__('Clients')}}
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom {{setActive('settings', 'm-menu__item--active')}} "
			    aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1">
				<a href="javascript:;" class="m-menu__link m-menu__toggle @hasrole('root') @else lock @endhasrole">
					<i class="m-menu__link-icon flaticon-settings"></i>
					<span class="m-menu__link-text">{{__('Settings')}}</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true"
						    m-menu-link-redirect="1"><span class="m-menu__link">
								<span class="m-menu__link-text">{{__('Settings')}}</span></span></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
							<a href="@can('Edit settings'){{route('settings.main')}} @else javascript:void(0) @endcan" class=" m-menu__link @can('Edit settings'){{route('settings.main')}} @else lock @endcan"><i
												class="m-menu__link-icon flaticon-settings"></i><span></span></i>
								<span class="m-menu__link-text">{{__('Main')}}</span>
							</a>
						</li>
						@can('View users')
							<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
								<a href="{{route('users.index')}}" class="m-menu__link "><i
													class="m-menu__link-icon fa fa-user-shield"></i><span></span></i>
									<span class="m-menu__link-text">{{__("Users and Roles")}}</span>
								</a>
							</li>
						@endcan

						{{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
						{{--<a href="{{route('logs')}}" class="m-menu__link "><i class="m-menu__link-icon flaticon-folder-1"></i><span></span></i>--}}
						{{--<span class="m-menu__link-text">{{__('Logs')}}</span>--}}
						{{--</a>--}}
						{{--</li>--}}
					</ul>
				</div>
			</li>
		</ul>
	</div>

	<!-- END: Aside Menu -->
</div>
<div class="m-aside-menu-overlay"></div>

<!-- END: Left Aside -->