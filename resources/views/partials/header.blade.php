<header id="m_header" class="m-grid__item m-header" m-minimize-offset="200" m-minimize-mobile-offset="200">
	<div class="m-container m-container--fluid m-container--full-height">
		<div class="m-stack m-stack--ver m-stack--desktop">

			<!-- BEGIN: Brand -->
			<div class="m-stack__item m-brand ">
				<div class="m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-stack__item--middle m-brand__logo">
						<a href="{{route('homepage')}}" class="m-brand__logo-wrapper">
							<img alt="" src="{{asset('web/images/icons/preloader.svg')}}" class="img-fluid" style="width: 60px;"/>
						</a>
					</div>
					<div class="m-stack__item m-stack__item--middle m-brand__tools">

						<!-- BEGIN: Responsive Aside Left Menu Toggler -->
						<a href="javascript:;" id="m_aside_left_offcanvas_toggle"
						   class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>

						<!-- END -->

						<!-- BEGIN: Responsive Header Menu Toggler -->
						<a id="m_aside_header_menu_mobile_toggle" href="javascript:;"
						   class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>

						<!-- END -->

						<!-- BEGIN: Topbar Toggler -->
						<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;"
						   class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
							<i class="flaticon-more"></i>
						</a>

						<!-- BEGIN: Topbar Toggler -->
					</div>
				</div>
			</div>

			<!-- END: Brand -->
			<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

				<!-- BEGIN: Horizontal Menu -->
				<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark "
				        id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
				<div id="m_header_menu"
				     class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-dark m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
					<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
						<li class="m-menu__item "><a href="javascript:;" class="m-menu__link m-menu__toggle">
								<span class="m-menu__link-text text-danger font-weight-bold">PIN: {{website()->prettypin}}</span></a>
						</li>
						<li class="m-menu__item "><a href="javascript:;" class="m-menu__link m-menu__toggle">
								<span class="m-menu__link-text text-accent font-weight-bold"><clock></clock></span></a>
						</li>
					</ul>
					</ul>
				</div>

				<!-- END: Horizontal Menu -->

				<!-- BEGIN: Topbar -->
				<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
					{{--<div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-dark m-header-search m-header-search--expandable m-header-search--skin-dark"--}}
					{{--id="m_quicksearch" m-quicksearch-mode="default">--}}

					{{--@include('partials.searchbar')--}}
					{{--</div>--}}
					<div class="m-stack__item m-topbar__nav-wrapper">
						<ul class="m-topbar__nav m-nav m-nav--inline">
							<notifications-bar :prop-notifications="{{\App\Models\App::getNotifications(20)}}"></notifications-bar>
							@include('partials.quick-actions-bar')
							<li class="m-nav__item">
								<a href="{{route('deals.create')}}" class="m-nav__link m-dropdown__toggle">
									<span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
									<span class="m-nav__link-icon "><span class="m-nav__link-icon-wrapper m--bg-brand"><i
															class="flaticon-share text-white"></i></span></span>
								</a>
							</li>
							<li class="m-nav__item"></li>
							<li class="m-nav__item"></li>
							@include('partials.user-bar')
							<li class="m-nav__item"></li>
							<li class="m-nav__item"></li>
							<li class="m-nav__item"></li>
						</ul>
					</div>
				</div>

				<!-- END: Topbar -->
			</div>
		</div>
	</div>
</header>
