
<style>
	#arrows {
		-webkit-animation: rotating 2s linear infinite;
		-moz-animation: rotating 2s linear infinite;
		-ms-animation: rotating 2s linear infinite;
		-o-animation: rotating 2s linear infinite;
		animation: rotating 2s linear infinite;
		transform-origin: 50% 50%;
		-webkit-transform-origin: 50% 50%;
		-moz-transform-origin: 50% 50%;
		-o-transform-origin: 50% 50%;
		transform-origin: 50% 50%;
	}
	@keyframes rotating {
		to {
			-ms-transform: rotate(360deg);
			-moz-transform: rotate(360deg);
			-webkit-transform: rotate(360deg);
			-o-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
	.cls-1 {
		fill: url(#linear-gradient);
	}

	.cls-2 {
		fill: url(#linear-gradient-2);
	}

	.cls-3 {
		fill: url(#linear-gradient-3);
	}

	.cls-4 {
		fill: url(#linear-gradient-4);
	}
</style>
<svg width="100%" height="100%" id="logo-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
     viewBox="0 0 411.52 399.87">
	<defs>

		<linearGradient id="linear-gradient" y1="399.69" x2="1" y2="399.69"
		                gradientTransform="matrix(387.75, 0, 0, -387.75, 93.36, 155263.17)"
		                gradientUnits="userSpaceOnUse">
			<stop offset="0" stop-color="#43e97b"/>
			<stop offset="1" stop-color="#38f9d7"/>
		</linearGradient>
		<linearGradient id="linear-gradient-2" x1="0" y1="399.69" x2="1" y2="399.69"
		                gradientTransform="matrix(387.75, 0, 0, -387.75, 117.13, 155101.41)"
		                xlink:href="#linear-gradient"/>
		<linearGradient id="linear-gradient-3" x1="0" y1="399.36" x2="1" y2="399.36"
		                gradientTransform="matrix(106.55, 0, 0, -106.55, 158.62, 42764.25)"
		                xlink:href="#linear-gradient"/>
		<linearGradient id="linear-gradient-4" x1="0" y1="399.6" x2="1" y2="399.6"
		                gradientTransform="matrix(172.22, 0, 0, -172.22, 266.1, 69033.29)"
		                xlink:href="#linear-gradient"/>
	</defs>
	<title>preloadernew</title>
	<g id="arrows">
		<path id="arr-bottom" class="cls-1"
		      d="M304.28,399.87c85.59.12,148.92-60.43,176.84-156.44-24.45,84.05-101.84,127-176.79,126.87C226.53,370.19,167,309,144.71,221.69l10.16-1.81-38.16-58.12-23.35,68.3,14-1.1C131.61,331.09,214.1,399.74,304.28,399.87"
		      transform="translate(-93.36 0)"/>
		<path id="arr-top" class="cls-2"
		      d="M294,0C208.37-.12,145.05,60.43,117.13,156.44c24.44-84,101.83-127,176.79-126.87,77.8.11,137.32,61.35,159.62,148.61L443.38,180l38.16,58.12,23.34-68.3-14,1.09C466.64,68.78,384.15.13,294,0"
		      transform="translate(-93.36 0)"/>
	</g>
	<g id="letters">
		<path id="letter-r" class="cls-3"
		      d="M199.94,206.71h5.56q8.72,0,13.38-3.6t4.66-10.37q0-6.77-4.66-10.37t-13.38-3.61h-5.56Zm64.77,62.67H228.05L199.94,225.8v43.58H170.49V156.07h45.83a45,45,0,0,1,16.53,2.78,31.11,31.11,0,0,1,18.56,18.71,38.63,38.63,0,0,1,2.33,13.53q0,12.91-6.23,21T229.1,223Z"
		      transform="translate(-93.36 0)"/>
		<path id="letter-w" class="cls-4"
		      d="M296.9,156.07l19.54,73.19,24.05-73.19h23.44l24,73.19,19.54-73.19h30.81L404.5,269.38H374.45l-22.24-65.67L330,269.38H299.91L266.1,156.07Z"
		      transform="translate(-93.36 0)"/>
	</g>
</svg>
