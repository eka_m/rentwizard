<li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
	<a href="#" class="m-nav__link m-dropdown__toggle">
		<span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
		<span class="m-nav__link-icon"><span class="m-nav__link-icon-wrapper"><i class="flaticon-add"></i></span></span>
	</a>
	<div class="m-dropdown__wrapper">
		<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
		<div class="m-dropdown__inner">
			<div class="m-dropdown__header m--align-center m--bg-brand text-white">
				<span class="m-dropdown__header-title">{{__('Add')}}</span>
				<span class="m-dropdown__header-subtitle">{{__('Quick actions')}}</span>
			</div>
			<div class="m-dropdown__body m-dropdown__body--paddingless">
				<div class="m-dropdown__content">
					<div class="m-scrollable" data-scrollable="false" data-height="380" data-mobile-height="200">
						<div class="m-nav-grid m-nav-grid--skin-light">
							<div class="m-nav-grid__row">
								<a href="{{route('inventory.create')}}" class="m-nav-grid__item">
									<i class="m-nav-grid__icon flaticon-open-box"></i>
									<span class="m-nav-grid__text">{{__('New')}} {{mb_strtolower(__('Item'))}}</span>
								</a>
								<a href="{{route('clients.create')}}" class="m-nav-grid__item">
									<i class="m-nav-grid__icon flaticon-user-add"></i>
									<span class="m-nav-grid__text">{{__('New')}} {{mb_strtolower(__('Client'))}}</span>
								</a>
							</div>
							<div class="m-nav-grid__row">
								<a href="{{route('attributes.create')}}" class="m-nav-grid__item">
									<i class="m-nav-grid__icon flaticon-web"></i>
									<span class="m-nav-grid__text">{{__('New')}} {{mb_strtolower(__('Attribute'))}}</span>
								</a>
								<a href="{{route('categories.create')}}" class="m-nav-grid__item">
									<i class="m-nav-grid__icon flaticon-attachment"></i>
									<span class="m-nav-grid__text">{{__('New')}} {{mb_strtolower(__('Category'))}}</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</li>
