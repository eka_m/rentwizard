<li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
    m-dropdown-toggle="click">
	<a href="#" class="m-nav__link m-dropdown__toggle d-inline-flex align-items-center">
		<span class="m-topbar__userpic m--hide" style="border-radius: 50%;">
				@if(auth("employee")->user()->avatar())
				<img src="/{{auth("employee")->user()->avatar()}}" alt="{{auth("employee")->user()->name}}"
				     style="width: 50px;">

			@else
				<img src="{{ Avatar::create(auth("employee")->user()->name)->toBase64() }}"
				     alt="{{auth("employee")->user()->name}}" style="width: 50px;"/>
			@endif
		</span>
		<span class="m-nav__link-icon m-topbar__usericon"
		      style="border-radius: 50%; width: 50px; height: 50px; overflow: hidden;">
		@if(auth("employee")->user()->avatar())
				<img src="/{{auth("employee")->user()->avatar()}}" alt="{{auth("employee")->user()->name}}"
				     style="width: 50px;">

			@else
				<img src="{{ Avatar::create(auth("employee")->user()->name)->toBase64() }}"
				     alt="{{auth("employee")->user()->name}}" style="width: 50px;"/>

			@endif
		</span>
		<span class="m-topbar__username m--hide">{{auth("employee")->user()->name}}</span>
	</a>
	<div class="m-dropdown__wrapper">
		<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
		<div class="m-dropdown__inner">
			<div class="m-dropdown__header m--align-center">
				<div class="m-card-user m-card-user--skin-light">
					<div class="m-card-user__pic">
						@if(auth("employee")->user()->avatar())
							<img src="/{{auth("employee")->user()->avatar()}}" alt="{{auth("employee")->user()->name}}">
						@else
							<img src="{{ Avatar::create(auth("employee")->user()->name)->toBase64() }}"
							     alt="{{auth("employee")->user()->name}}"/>
						@endif
					</div>
					<div class="m-card-user__details text-white">
						<span class="m-card-user__name m--font-weight-500"> {{auth("employee")->user()->name}}</span>
						<a href="javascript:void(0)" class="m-card-user__email m--font-weight-300 m-link"> {{auth("employee")->user()->email}}</a>
						<span class="badge badge-info">
							{{__(auth("employee")->user()->roles->first()->name)}}
						</span>
					</div>
				</div>
			</div>
			<div class="m-dropdown__body">
				<div class="m-dropdown__content">
					<ul class="m-nav m-nav--skin-light">
						<li class="m-nav__item">
							<a href="{{route('user.profile')}}" class="m-nav__link">
								<i class="m-nav__link-icon flaticon-profile-1"></i>
								<span class="m-nav__link-title">
									<span class="m-nav__link-wrap">
										<span class="m-nav__link-text">{{__('Profile')}}</span>
										{{--<span class="m-nav__link-badge"><span class="m-badge m-badge--success">2</span></span>--}}
									</span>
								</span>
							</a>
						</li>
						<li class="m-nav__separator m-nav__separator--fit">
						</li>
						<li class="m-nav__item w-100">
							<a class="btn btn-dark m-btn m-btn--custom m-btn--label-brand m-btn--bolder text-white"
							   style="cursor:pointer;" onclick="$(this).find('form').submit();">
								{{__('Logout')}}
								<form action="{{route('tenant.logout')}}" method="POST" style="display:none">
									@csrf
								</form>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</li>

