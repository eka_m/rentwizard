@extends('layouts.base')
@section('title', ' | '. __('Settings') . ' | '. __('Main'))
@section('content')
	<div class="m-content">
		<form action="{{route('settings.save')}}" method="post">
			@method('PUT')
			@csrf
			<div class="row">
				<div class="col-md-6 offset-md-3">
					@fxportlet(["class" => "shadow-none", "title" => __('Main'), "icon" => "flaticon-settings"])
					@slot('actions')
						<clock class="text-white h3 font-weight-bold mb-0"></clock>
						@endslot
					@slot('body')
						@card
						<div class="row pt-5 px-4">
							<label class="col-xl-3 col-lg-3 col-form-label">{{__('Timezone')}}</label>
							<div class="form-group col-md-9">
								<select name="timezone" class="selectpicker form-control timezoneSelect" data-live-search="true">
									@foreach( \App\Helpers\Time::timeZoneList() as $code => $name)
										<option value="{{$code}}" @if(setting('timezone') == $code) selected @endif>{{$name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="row px-4">
							<label class="col-xl-3 col-lg-3 col-form-label">{{__('Сurrency')}}</label>
							<div class="form-group col-md-9">
								<select name="currency[code]" class="selectpicker form-control" data-live-search="true">
									@foreach( currency()->all() as $code => $name)
										@if($code == 'AMD') @continue @endif
										<option value="{{$code}}"
										        @if(setting('currency.code') == $code) selected
										        @endif data-content="{{mb_convert_case($name,MB_CASE_TITLE,'UTF-8')}} <span class='m-badge m-badge--wide m-badge--success'>{!! \App\Helpers\Currency::getSymbol($code) !!} </span>">{{mb_convert_case($name,MB_CASE_TITLE,'UTF-8')}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="row mt-5">
							<div class="col-12">
								<button type="submit"
								        class="btn btn-accent m-btn m-btn--icon m-btn--wide m-btn--md pull-right">
																<span>
																	<i class="la la-check"></i>
																	<span>{{__('Save')}}</span>
																</span>
								</button>
							</div>
						</div>
						@endcard
					@endslot
					@endfxportlet
				</div>
			</div>
		</form>
	</div>
@endsection