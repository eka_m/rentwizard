<div class="settings__nav">
	<a href="{{route('settings.main')}}" class="btn btn-brand settings__nav__item">
				<span>
					<span class="d-block"><i class="flaticon-settings text-white"></i></span>
					<span class="d-block">{{__('Main')}}</span>
				</span>
	</a>
	<a href="{{route('users.index')}}" class="btn btn-brand settings__nav__item">
				<span>
					<span class="d-block"><i class="fa fa-user-shield"></i></span>
					<span class="d-block"> {{__("Users and Roles")}}</span>
				</span>
	</a>
	<a href="{{route('logs')}}" class="btn btn-brand settings__nav__item">
				<span>
					<span class="d-block"><i class="flaticon-folder-1 text-white"></i></span>
					<span class="d-block">{{__('Logs')}}</span>
				</span>
	</a>
</div>