@extends('layouts.base')
@section('title', ' | '. __('Settings'))
@section('content')
	<div class="m-content">
		@include('settings.nav')
	</div>
@endsection