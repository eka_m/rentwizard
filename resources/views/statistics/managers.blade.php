@extends('layouts.base')
@section('title', ' | Статистика менеджера')
@section('content')
	<div class="m-content">
		@include('statistics.nav')
		@if($managers->isEmpty())
			<div class="row">
				<div class="col-12" style="min-height: 300px;">
					<div class="card shadow-sm h-100">
						<div class="card-body h-100 d-flex align-items-center justify-content-center">
							<p class="text-muted">{{__('First add at least one manager')}}</p>
						</div>
					</div>
				</div>
			</div>

		@else
			<manager-profit prop-managers="{{$managers}}"></manager-profit>
		@endif
	</div>
@endsection