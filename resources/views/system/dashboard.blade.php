@extends("layouts.system")
@section("title", " | Системная панель")
@section("css")
	<style>
		table td, table th {
			text-align: center;
		}
	</style>
@endsection
@section("content")
	<div class="m-content">
		@include("system.table")
	</div>
@endsection