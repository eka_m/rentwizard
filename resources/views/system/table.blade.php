@card(["class" => "d-flex align-items-center justify-content-center"])
<table class="table table-bordered table-responsive">
	<thead>
	<tr>
		<th>#</th>
		<th>Пользователь</th>
		<th>Сервис</th>
		<th>Компания</th>
		<th>Дата регистрации</th>
		<th>Дата активации</th>
		<th>Сделки</th>
		<th>Инвентарь</th>
		<th>Клиенты</th>

	</tr>
	</thead>
	<tbody>
	@foreach($customers as $item)
		<tr>
			<td>{{$item->id}}</td>
			<td>{{$item->name}}</td>
			<td>{{$item->websites[0]['pin']}}</td>
			<td>{{$item->company}}</td>
			<td>{{$item->created_at}}</td>
			<td>{{$item->email_verified_at}}</td>
			<td><span class="m-badge m-badge--wide m-badge--primary font-weight-bold">{{$item->info['count_of_deals']}}</span></td>
			<td><span class="m-badge m-badge--wide m-badge--primary font-weight-bold">{{$item->info['count_of_items']}}</span></td>
			<td><span class="m-badge m-badge--wide m-badge--primary font-weight-bold">{{$item->info['count_of_clients']}}</span></td>
		</tr>
	@endforeach
	</tbody>
</table>
@endcard