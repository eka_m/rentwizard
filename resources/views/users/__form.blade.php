<div class="row">
	<div class="col-md-6 offset-md-3">
		<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg  m-portlet--responsive-mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-wrapper">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								<span class="m-portlet__head-icon"><i class="fa fa-user-plus"></i></span>
								{{__("New")}} {{  mb_strtolower(__("User")) }}
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="form-group m-form__group  {{$errors->has('name') ? 'has-danger' : ''}}">
					<label class="form-control-label">{{__('Fullname')}}</label>
					<input type="text" class="form-control m-input {{$errors->has('name') ? 'form-control-danger' : ''}}"
					       name="name" placeholder="{{__('Fullname')}}" value="{{old('name',$user->name)}}" required>
					@if ($errors->has('name'))
						<div class="form-control-feedback">
							{{ $errors->first('email') }}
						</div>
					@endif
				</div>
				<div class="form-group m-form__group  {{$errors->has('email') ? 'has-danger' : ''}}">
					<label class="form-control-label">{{__('E-mail')}}</label>
					<input class="form-control m-input {{$errors->has('email') ? 'form-control-danger' : ''}}" name="email"
					       type="email" placeholder="{{__('E-mail')}}" value="{{old('email',$user->email)}}" required>
					@if ($errors->has('email'))
						<div class="form-control-feedback">
							{{ $errors->first('email') }}
						</div>
					@endif
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group m-form__group">
							<label class="form-control-label">{{__('Role')}}</label>
							<select name="role" class="form-control m-input form-control selectPicker">
								@foreach($roles as $role)
									<option value="{{$role->name}}"
									        @hasRole($role) selected @endhasRole >{{__(ucfirst($role->name))}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group m-form__group">
							<label class="form-control-label">{{__('Percentage from the deal')}}</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" class="form-control m-input" name="percent" placeholder="0"
								       value="{{old('percent', $user->percent) ?: 0}}" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i>%</i></span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot text-right">
				<a href="{{ url()->previous() }}"
				   class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
					<span><i class="la la-arrow-left"></i><span>Назад</span></span>
				</a>
				<div class="btn-group">
					<button type="submit" class="btn btn-brand  m-btn m-btn--icon m-btn--wide m-btn--md">
						<span><i class="la la-check"></i><span>Сохранить</span></span>
					</button>
					{{--<button type="button" class="btn btn-brand  dropdown-toggle dropdown-toggle-split m-btn m-btn--md"--}}
					{{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
					{{--</button>--}}
					{{--<div class="dropdown-menu dropdown-menu-right">--}}
					{{--<a class="dropdown-item" href="#"><i class="la la-plus"></i> Save & New</a>--}}
					{{--<a class="dropdown-item" href="#"><i class="la la-copy"></i> Save & Duplicate</a>--}}
					{{--<a class="dropdown-item" href="#"><i class="la la-undo"></i> Save & Close</a>--}}
					{{--<div class="dropdown-divider"></div>--}}
					{{--<a class="dropdown-item" href="#"><i class="la la-close"></i> Cancel</a>--}}
					{{--</div>--}}
				</div>
			</div>
		</div>

	</div>
</div>
