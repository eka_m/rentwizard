@extends('settings.settings')
@section('title', ' | ' .__('Users and Roles'))

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-md-6">
				<div class="m-portlet m-portlet--responsive-mobile m-portlet--brand m-portlet--head-solid-bg">
					<div class="m-portlet__head">
						<div class="m-portlet__head-wrapper">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon"><i class="flaticon-users"></i></span>
									<h3 class="m-portlet__head-text">
										{{__('Users')}}
									</h3>
								</div>
							</div>
							<div class="m-portlet__head-tools">
								<ul class="m-portlet__nav">
									<li class="m-portlet__nav-item">
										<a href="{{route('users.create')}}"
										   data-toggle="m-tooltip"
										   data-placement="top"
										   data-html="true"
										   data-skin="light"
										   data-original-title='{{__('Add')}} '
										   class="m-portlet__nav-link m-portlet__nav-link--icon">
											<i class="la la-plus"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<ul class="list-group">
							@foreach($users as $user)
								<li class="list-group-item d-flex align-items-center justify-content-between mb-1">
									<span>{{$user->name}}</span>
									<div class="text-right">
										<a href="{{route('users.edit', $user->id)}}"
										   class="m-portlet__nav-link btn btn-sm m-btn--hover-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
											<i class="la la-edit"></i>
										</a>
											<delete-btn
															class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
															url="{{route('users.destroy', $user->id)}}"
															confirm="{{__('Are you sure?')}}">
												<i class="la la-trash"></i>
											</delete-btn>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<roles-permissions :prop-roles="{{$roles}}" :prop-permissions="{{$permissions}}"/>
			</div>
		</div>
	</div>
@endsection