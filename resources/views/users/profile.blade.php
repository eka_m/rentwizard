@extends('layouts.base')
@section('title', ' | ' . __('Profile'))

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-xl-3 col-lg-4">
				<div class="m-portlet m-portlet--full-height  ">
					<div class="m-portlet__body">
						<div class="m-card-profile">
							<div class="m-card-profile__title m--hide">
								{{__('Profile')}}
							</div>
							<div class="m-card-profile__picd mb-3">
								<div class="m-card-profile__pic-wrapper p-0  m-auto"
								     style="width: 150px; height: 150px; overflow: hidden;">
									@php
										$options = [
										"size"=> "240,240",
										"ratio" => '1:1',
										"maxFileSize"=>"250",
										"instantEdit" => true,
										"buttonConfirmLabel" => __('Confirm'),
										"buttonCancelLabel" => __('Reject'),
										"label" => __('Drag photo here'),
										"service" => route('async.user.avatar'),
										"push" => true,
										"uploadBase64" => true
										];
									@endphp
									<slim-cropper :options='{{json_encode($options)}}' style="border-radius: 50%;">
										<input type="file" name="photo" class="d-none"/>
										@if($user->avatar())
											<img src="/{{$user->avatar()}}" alt="{{$user->name}}">
										@endif
									</slim-cropper>
								</div>
							</div>
							<div class="m-card-profile__details">
								<span class="m-card-profile__name">{{$user->name}}</span>
								<a href="" class="m-card-profile__email m-link">{{$user->email}}</a>
							</div>
						</div>
						<div class="m-separator"></div>
						<h4 class="text-center">{{__('Earnings')}}</h4>
						<div class="m-widget1 m-widget1--paddingless">
							<div class="m-widget1__item">
								<div class="row m-row--no-padding align-items-center">
									<div class="col">
										<h3 class="m-widget1__title">{{__('Today')}}</h3>
										{{--<span class="m-widget1__desc">{{__('Today')}}</span>--}}
									</div>
									<div class="col m--align-right">
										<span class="m-widget1__number m--font-brand">+ {{collect(\App\Helpers\Calculate::managerProfit( $today ))->sum( 'manager_profit' )}}</span>
									</div>
								</div>
							</div>
							<div class="m-widget1__item">
								<div class="row m-row--no-padding align-items-center">
									<div class="col">
										<h3 class="m-widget1__title">{{__('Week')}}</h3>
										<span class="m-widget1__desc">{{__('This Week')}}</span>
									</div>
									<div class="col m--align-right">
										<span class="m-widget1__number m--font-brand">+ {{collect(\App\Helpers\Calculate::managerProfit( $week ))->sum( 'manager_profit' )}}</span>
									</div>
								</div>
							</div>
							<div class="m-widget1__item">
								<div class="row m-row--no-padding align-items-center">
									<div class="col">
										<h3 class="m-widget1__title">{{__('Month')}}</h3>
										<span class="m-widget1__desc">{{__('This Month')}}</span>
									</div>
									<div class="col m--align-right">
										<span class="m-widget1__number m--font-success">{{collect(\App\Helpers\Calculate::managerProfit( $month ))->sum( 'manager_profit' )}}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-lg-8">
				@fxportlet(["class" => "shadow-none", "title" => __("Settings")])
				@slot('actions')
					<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--right" role="tablist">
						<li class="nav-item m-tabs__item">
							<a class="nav-link m-tabs__link active" data-toggle="tab" href="#usermain" role="tab">
								{{__('Main')}}
							</a>
						</li>
						<li class="nav-item m-tabs__item">
							<a class="nav-link m-tabs__link" data-toggle="tab" href="#resetpass" role="tab">
								{{__('Reset Password')}}
							</a>
						</li>
					</ul>
				@endslot
				@slot('body')
					<div class="tab-content">
						<div class="tab-pane fade active show" id="usermain">
							<div class="row">
								<div class="col-md-12">
									@card
									<form action="{{route('user.save.profile')}}" method="post" class="form validate">
										@method('PUT')
										@csrf
										<div class="row">
											<div class="col-md-8 offset-md-2 py-5">
												<div class="row">
													<label for="name" class="col-xl-3 col-lg-3 col-form-label">{{__('Fullname')}}</label>
													<div class="form-group col-md-9">
														<input id="name" type="text" name="name" class="form-control"
														       value="{{old('name', $user->name)}}"
														       placeholder="{{__('Fullname')}}" required>
													</div>
												</div>
												<div class="row">
													<label for="email" class="col-xl-3 col-lg-3 col-form-label">{{__('E-mail')}}</label>
													<div class="form-group col-md-9">
														<input id="email" type="text" name="email" class="form-control"
														       value="{{old('email', $user->email)}}"
														       placeholder="{{__('E-mail')}}" required>
													</div>
												</div>
												<div class="row">
													<label for="email" class="col-xl-3 col-lg-3 col-form-label">{{__('Language')}}</label>
													<div class="form-group col-md-9">
														<select name="locale" class="selectpicker form-control">
															@foreach(["RU" => "Русский", "EN" => "English"  ] as $code => $name)
																<option value="{{$code}}" @if($user->locale == $code) selected @endif>{{$name}}</option>
															@endforeach
														</select>
													</div>
												</div>
												<div class="row mt-5">
													<div class="col-12">
														<button type="submit"
														        class="btn btn-accent m-btn m-btn--icon m-btn--wide m-btn--md pull-right">
																<span>
																	<i class="la la-check"></i>
																	<span>{{__('Save')}}</span>
																</span>
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									@endcard
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="resetpass">
							<div class="row">
								<div class="col-md-12">
									@card
									<form action="{{route('user.reset', $user->id)}}" method="post" class="form validate">
										@method('PUT')
										@csrf
										<div class="row">
											<div class="col-md-8 offset-md-2 py-5">
												<div class="row">
													<label for="name" class="col-xl-3 col-lg-3 col-form-label">{{__('Password')}}</label>
													<div class="form-group col-md-9">
														<input class="form-control m-input"
														       name="password"
														       type="password" placeholder="{{__('Password')}}" data-rule-equalto="#password_confirmation" id="password"  required>
													</div>

												</div>
												<div class="row">
													<label for="name" class="col-xl-3 col-lg-3 col-form-label">{{__('Confirm Password')}}</label>
													<div class="form-group col-md-9">
														<input class="form-control m-input" name="password_confirmation"  id="password_confirmation" type="password"
														       placeholder="{{__('Confirm Password')}}" required>
													</div>

												</div>
												<div class="row mt-5">
													<div class="col-12">
														<button type="submit"
														        class="btn btn-accent m-btn m-btn--icon m-btn--wide m-btn--md pull-right">
																<span>
																	<i class="la la-check"></i>
																	<span>{{__('Save')}}</span>
																</span>
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									@endcard
								</div>
							</div>
						</div>
					</div>
				@endslot
				@endfxportlet
			</div>
		</div>
	</div>
@endsection