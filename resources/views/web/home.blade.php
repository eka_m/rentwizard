@extends('layouts.app')
@section('content')

	@include("web.partials.slider")
	@include("web.partials.features")
	@include("web.partials.opportunities")
	@include("web.partials.overview")

@endsection