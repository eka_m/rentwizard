<!--Application Section-->
<section class="application-section" id="features">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Title Column-->
			<div class="title-column col-lg-5 col-md-12 col-sm-12">
				<div class="inner-column">
					<!--Sec Title-->
					<div class="sec-title">
						<h2>{!! __("Smart service to automate <br> rental business") !!}</h2>
						<div class="separater"></div>
						<div class="text">{{__("Rent Wizard is suitable for all types of rental services")}}.</div>
					</div>
					{{--<a href="#" class="theme-btn btn-style-seven">Solution Price</a>--}}
				</div>
			</div>

			<!--Blocks Column-->
			<div class="blocks-column col-lg-7 col-md-12 col-sm-12">
				<div class="inner-column">
					<div class="row clearfix">
						<div class="column col-lg-6 col-md-6 col-sm-12">

							<!--Feature Block-->
							<div class="feature-block">
								<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
									<div class="icon-box">
										<img src="{{asset('web/images/illustrations/security.svg')}}" width="50px" alt="">
									</div>
									<h3>{{__("Security")}}</h3>
									<div class="text">{!! __("We guarantee complete confidentiality, safety and integrity of data") !!}.</div>
								</div>
							</div>

							<!--Feature Block-->
							<div class="feature-block">
								<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
									<div class="icon-box">
										<img src="{{asset('web/images/illustrations/fingersnap.svg')}}" width="50px" alt="">
									</div>
									<h3>{{__("Simplicity")}}</h3>
									<div class="text">{{__("Convenient control panel that even an inexperienced user can figure out")}}.</div>
								</div>
							</div>

						</div>

						<div class="column col-lg-6 col-md-6 col-sm-12">

							<!--Feature Block Two-->
							<div class="feature-block-two">
								<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
									<div class="icon-box">
										<img src="{{asset('web/images/illustrations/robot.svg')}}" width="50px" alt="">
									</div>
									<h3>{{__("Automation")}}</h3>
									<div class="text">{{__("We have automated routine tasks so that you don’t waste your precious time on it")}}.</div>
								</div>
							</div>

							<!--Feature Block Two-->
							<div class="feature-block-two">
								<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
									<div class="icon-box">
										<img src="{{asset('web/images/illustrations/controls.svg')}}" width="50px" alt="">
									</div>
									<h3>{{__("Fine tuning")}}</h3>
									<div class="text">{{__("Ability to customize to any need to ensure maximum control over the business")}}.</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<!--End Application Section-->