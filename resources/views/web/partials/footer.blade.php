<!--Main Footer-->
<footer class="main-footer alternate"  id="contacts">
	<!--Widgets Section-->
	<div class="auto-container">

		<!--Widgets Section-->
		<div class="widgets-section">
			<div class="row clearfix">

				<!--Column-->
				<div class="big-column col-lg-7 col-md-12 col-sm-12">
					<div class="row clearfix">

						<!--Footer Column-->
						<div class="footer-column col-lg-7 col-md-7 col-sm-12">
							<div class="footer-widget awesome-widget">
								<h2>Rent Wizard</h2>
								<div class="text">
									{{__("Rent Wizard is designed for administrators and owners of rental points and rental of any inventory. The service automates the accounting of goods issued by customers, maintains a history of payments and settlements, generating all the necessary reports for the manager.")}}
								</div>
							</div>
						</div>

						<!--Footer Column-->
						<div class="footer-column col-lg-5 col-md-5 col-sm-12">
							{{--<div class="footer-widget links-widget">--}}
								{{--<h2>Resources</h2>--}}
								{{--<ul>--}}
									{{--<li><a href="#">Learning Hub</a></li>--}}
									{{--<li><a href="#">Video Tutorials</a></li>--}}
									{{--<li><a href="#">Quick Start Guide</a></li>--}}
									{{--<li><a href="#">SEO Software Reviews</a></li>--}}
									{{--<li><a href="#">Update Stories</a></li>--}}
								{{--</ul>--}}
							{{--</div>--}}
						</div>

					</div>
				</div>

				<!--Column-->
				<div class="big-column col-lg-5 col-md-12 col-sm-12">
					<div class="row clearfix">

						<!--Footer Column-->
						<div class="footer-column col-lg-5 col-md-5 col-sm-12">
							<div class="footer-widget links-widget">
								<h2>{{__("Go to")}}</h2>
								<ul>
									<li class="current"><a href="{{!request()->is("/") ? route("homepage"): ''}}/#home">{{__("Home")}}</a></li>
									<li><a href="{{!request()->is("/") ? route("homepage"): ''}}/#features">{{__("Features")}}</a></li>
									<li><a href="{{!request()->is("/") ? route("homepage"): ''}}/#opportunities">{{__("Opportunities")}}</a></li>
									<li><a href="{{!request()->is("/") ? route("homepage"): ''}}/#contacts">{{__("Contacts")}}</a></li>
								</ul>
							</div>
						</div>

						<!--Footer Column-->
						<div class="footer-column col-lg-7 col-md-7 col-sm-12">
							<div class="footer-widget social-widget">
								<h2>{{__("Contacts")}}</h2>
								<ul class="contact-list">
									<li> <i class="fa flaticon-location-pin"></i> 9000 Regency Parkway, Suite 400 Cary,NC, 2756</li>
									<li><i class="fa flaticon-envelope"></i> hello@rent-wizard.com</li>
									{{--<li><span>Phone:</span>+24 135 3657</li>--}}
									{{--<li><span>Fax:</span>+24 135 5479</li>--}}
								</ul>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

		<!--Footer Bottom-->
		<div class="footer-bottom">
			<div class="row clearfix">
				<div class="social-column col-lg-8 col-md-8 col-sm-12">
					{{--<ul class="social-icon-one">--}}
					{{--<li>Follow Us:</li>--}}
					{{--<li><a href="#"><span class="icon fa fa-facebook"></span></a></li>--}}
					{{--<li><a href="#"><span class="icon fa fa-twitter"></span></a></li>--}}
					{{--<li><a href="#"><span class="icon fa fa-whatsapp"></span></a></li>--}}
					{{--<li><a href="#"><span class="icon fa fa-skype"></span></a></li>--}}
					{{--<li><a href="#"><span class="icon fa fa-google"></span></a></li>--}}
					{{--</ul>--}}
				</div>
				<div class="copyright-column col-lg-4 col-md-4 col-sm-12">
					<div class="copyright">Copyright &copy; {{date("Y")}}, Rent Wizard
						{{--<a target="_blank"href="https://themeforest.net/user/yogsthemes/portfolio"></a>--}}
					</div>
				</div>
			</div>
		</div>

	</div>
</footer>
<!--End Main Footer-->