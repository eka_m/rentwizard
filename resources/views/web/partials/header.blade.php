<header class="main-header header-style-two">
	<!--Header-Upper-->
	<div class="header-upper">
		<div class="auto-container">
			<div class="clearfix">

				<div class="pull-left logo-box">
					<div class="logo">
						<a href="{{route('homepage')}}">
							@if(isset($logowhite))
								<img src="{{asset('web/images/logowhite.svg')}}" width="170px" height="51px" alt="" title="">
							@else
								<img src="{{asset('web/images/logo.svg')}}" width="170px" height="51px" alt="" title="">
							@endif
						</a>
					</div>
				</div>

				<div class="nav-outer clearfix">

					<!-- Main Menu -->
					<nav class="main-menu navbar-expand-md">
						<div class="navbar-header">
							{{--<div class="btn-group" role="group">--}}
							<button class="navbar-toggler btn-success btn-sm" type="button" data-toggle="collapse"
							        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
							        aria-expanded="false" aria-label="Toggle navigation">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							{{--<button class="btn btn-danger d-inline d-sm-none">--}}
							{{--<i class="fa flaticon-user"></i>--}}
							{{--</button>--}}
							{{--</div>--}}
						</div>

						<div class="navbar-collapse collapse scroll-nav clearfix" id="navbarSupportedContent">
							<ul class="navigation clearfix">
								<li class="current"><a href="{{!request()->is("/") ? route("homepage"): ''}}/#home">{{__("Home")}}</a>
								</li>
								<li><a href="{{!request()->is("/") ? route("homepage"): ''}}/#features">{{__("Features")}}</a></li>
								<li><a href="{{!request()->is("/") ? route("homepage"): ''}}/#opportunities">{{__("Opportunities")}}</a>
								</li>
								<li><a href="{{!request()->is("/") ? route("homepage"): ''}}/#contacts">{{__("Contacts")}}</a></li>
							</ul>
						</div>

					</nav>


					<!--User Box-->
					<div id="user-box" class="button-box d-flex">
						@include('web.partials.user-box')
						@include('web.partials.language-toggler')
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--End Header Upper-->

</header>