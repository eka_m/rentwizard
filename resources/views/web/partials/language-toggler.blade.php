<div class="language-toogler order-1 order-md-2 ml-md-2">
	<button class="btn p-0">
		<img src="{{asset("web/images/icons/flags/blank.gif")}}" class="flag flag-{{app()->getLocale()}}" alt="Russioan Federation">
	</button>
	<ul class="language-list">
		@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
			<li>
				<a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
					{{ ucfirst($properties['native']) }}
				</a>
			</li>
		@endforeach
	</ul>
</div>