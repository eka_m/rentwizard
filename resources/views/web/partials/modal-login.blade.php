<div class="modal fade modallogin" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div class="modal-header py-5 px-5">
				<h5>{{strtoupper(__("Sign in"))}}</h5>
			</div>
			<div class="modal-body px-4">
				<div class="commitment-form">
					<form method="POST" action="{{ route('login') }}" id="modalLoginForm">
						@csrf
						<div class="form-group row mb-0">
							<div class="col-md-8 pr-md-1">
								<label for="email" class="form-label text-md-right">{{__("E-mail")}} {{strtolower(__('Address'))}}</label>
								<input id="email" type="email" class="form-control"
								       name="email" value="{{ old('email') }}" placeholder="{{__("E-mail")}} {{strtolower(__('Address'))}}" required autofocus>
							</div>
							<div class="col-md-4 pl-md-0 mt-4 mt-sm-0">
								<label for="pin" class="form-label text-md-right">{{ __('Company pin') }}</label>
								<input id="pin" type="text" class="input-pin form-control"
								       name="pin"
								       value="{{ old('pin') }}" size="7" maxlength="7" placeholder="xxx-xxx">
								<small class="form-text text-muted text-right">{{__('Optional')}}.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="password" class="form-label text-md-right">{{ __('Password') }}</label>
							<input id="password" type="password"
							       class="form-control" name="password" placeholder="{{__('Password')}}" required>
						</div>

						<div class="form-group ">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="remember"
								       id="remember" {{ old('remember') ? 'checked' : '' }}>
								<label class="form-check-label" for="remember">
									{{ __('Remember Me') }}
								</label>
							</div>
						</div>

						<div class="form-group">
							<button id="modalloginsubmit" type="submit" class="btn btn-success btn-lg w-100">
								{{ __('Login') }}
							</button>
							<div class="clearfix mt-3"></div>
							<a href="{{ route('password.request') }}">
								{{ __('Forgot Your Password?') }}
							</a>
							<div class="pull-right">
								<span>{{ __("Don't have account?") }}</span>
								<a href="{{ route('register') }}">
									{{ __("Create") }}
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>