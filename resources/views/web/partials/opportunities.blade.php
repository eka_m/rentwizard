<!--Feature Section-->
<section class="feature-section" id="opportunities">
	<div class="auto-container">

		<!--Sec Title-->
		<div class="sec-title centered">
			<h2>{{__("Key Features")}}</h2>
			<div class="separater"></div>
			<div class="text">{{__("Check out some key features")}}.</div>
		</div>

		<!--Feature Block Three-->
		<div class="feature-block-three">
			<div class="inner-box">
				<div class="row clearfix">
					<!--Content Column-->
					<div class="content-column col-lg-6 col-md-6 col-sm-12">
						<div class="inner-column">
							<div class="sec-title">
								<h2>{{__("Detailed analytics")}}</h2>
								<div class="separater"></div>
							</div>
							<div class="text">
								{{__("We provide you with a detailed report on profits, statistics of individual inventory items, statistics and history of transactions with separate clients")}}
								.
							</div>
							{{--<a href="#" class="theme-btn btn-style-seven">See More</a>--}}
						</div>
					</div>
					<!--Image Column-->
					<div class="image-column col-lg-6 col-md-6 col-sm-12">
						<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="{{asset('web/images/illustrations/analitics.svg')}}" alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--Feature Block Three-->
		<div class="feature-block-three alternate">
			<div class="inner-box">
				<div class="row clearfix">

					<!--Image Column-->
					<div class="image-column pull-left col-lg-6 col-md-6 col-sm-12">
						<div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="{{asset('web/images/illustrations/calculate.svg')}}" alt=""/>
							</div>
						</div>
					</div>

					<!--Content Column-->
					<div class="content-column pull-right col-lg-6 col-md-6 col-sm-12">
						<div class="inner-column">
							<div class="sec-title">
								<h2>{{__("Price calculator")}}</h2>
								<div class="separater"></div>
							</div>
							<div
								class="text">
								{{__("The system will calculate the transaction prices for you taking into account the range of dates and discounts and provide you with the amount in your currency")}}
								.
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<!--Feature Block Three-->
		<div class="feature-block-three">
			<div class="inner-box">
				<div class="row clearfix">
					<!--Content Column-->
					<div class="content-column col-lg-6 col-md-6 col-sm-12">
						<div class="inner-column">
							<div class="sec-title">
								<h2>{{__("Employee management")}}</h2>
								<div class="separater"></div>
							</div>
							<div class="text">
								{{__("Add employees. Ensure or limit the rights of employees to specific actions in the system. Adjust the percentage of sales for employees. Find out which of the employees brings more income to the company.")}}
							</div>
							{{--<a href="#" class="theme-btn btn-style-seven">See More</a>--}}
						</div>
					</div>
					<!--Image Column-->
					<div class="image-column col-lg-6 col-md-6 col-sm-12">
						<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="{{asset('web/images/illustrations/employee.svg')}}" alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
<!--End Feature Section-->