<div class="tabs-content">

	<!--Tab / Active Tab-->
	<div class="tab active-tab" id="prod-modern">
		<div class="content">
			<div class="row clearfix">
				<div class="content-column col-lg-7 col-md-7 col-sm-12">
					<div class="inner-column pt-5">
						<h3>Modern Tecnology</h3>
						<div class="text">There are many variations of passages of Lorem Ipsume available, but the
							majority. Many museu are transforming thanks to echnology. They are adding interactivity and
							magic to the visitorer experience. Estimote Beacons have helped the Guggenheim in New York to
							be at the forefront of this process. The club uses Estimote Beacons to transform the
							in-stadium experience for its guests.
						</div>
					</div>
				</div>
				<!--Image Column-->
				<div class="image-column col-lg-5 col-md-5 col-sm-12">
					<div class="inner-column mock-container">
						<div class="image">
							<img src="{{asset('web/images/illustrations/imac.png')}}" alt=""/>
							<div class="mock mock-1">
								<div class="mock-inner">
									<img src="{{asset("web/images/mockup/stat-1.jpg")}}" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Tab-->
	<div class="tab " id="prod-friendly">
		<div class="content">
			<div class="row clearfix">
				<!--Content Column-->
				<div class="content-column col-lg-7 col-md-7 col-sm-12">
					<div class="inner-column">
						<div class="icon-box">
													<span class="icon"><img src="{{asset('web/images/icons/technology-icon.png')}}"
													                        alt=""/></span>
						</div>
						<h3>Friendly Use</h3>
						<div class="text">There are many variations of passages of Lorem Ipsume available, but the
							majority. Many museu are transforming thanks to echnology. They are adding interactivity and
							magic to the visitorer experience. Estimote Beacons have helped the Guggenheim in New York to
							be at the forefront of this process. The club uses Estimote Beacons to transform the
							in-stadium experience for its guests.
						</div>
						<a href="#" class="theme-btn btn-style-seven">See More</a>
					</div>
				</div>
				<!--Image Column-->
				<div class="image-column col-lg-5 col-md-5 col-sm-12">
					<div class="inner-column">
						<div class="image">
							<img src="{{asset('web/images/resource/technology.png')}}" alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Tab-->
	<div class="tab" id="prod-face">
		<div class="content">
			<div class="row clearfix">
				<!--Content Column-->
				<div class="content-column col-lg-7 col-md-7 col-sm-12">
					<div class="inner-column">
						<div class="icon-box">
													<span class="icon"><img src="{{asset('web/images/icons/technology-icon.png')}}"
													                        alt=""/></span>
						</div>
						<h3>Face ID</h3>
						<div class="text">There are many variations of passages of Lorem Ipsume available, but the
							majority. Many museu are transforming thanks to echnology. They are adding interactivity and
							magic to the visitorer experience. Estimote Beacons have helped the Guggenheim in New York to
							be at the forefront of this process. The club uses Estimote Beacons to transform the
							in-stadium experience for its guests.
						</div>
						<a href="#" class="theme-btn btn-style-seven">See More</a>
					</div>
				</div>
				<!--Image Column-->
				<div class="image-column col-lg-5 col-md-5 col-sm-12">
					<div class="inner-column">
						<div class="image">
							<img src="{{asset('web/images/resource/technology.png')}}" alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Tab-->
	<div class="tab" id="prod-woreless">
		<div class="content">
			<div class="row clearfix">
				<!--Content Column-->
				<div class="content-column col-lg-7 col-md-7 col-sm-12">
					<div class="inner-column">
						<div class="icon-box">
													<span class="icon"><img src="{{asset('web/images/icons/technology-icon.png')}}"
													                        alt=""/></span>
						</div>
						<h3>Wireless Charging</h3>
						<div class="text">There are many variations of passages of Lorem Ipsume available, but the
							majority. Many museu are transforming thanks to echnology. They are adding interactivity and
							magic to the visitorer experience. Estimote Beacons have helped the Guggenheim in New York to
							be at the forefront of this process. The club uses Estimote Beacons to transform the
							in-stadium experience for its guests.
						</div>
						<a href="#" class="theme-btn btn-style-seven">See More</a>
					</div>
				</div>
				<!--Image Column-->
				<div class="image-column col-lg-5 col-md-5 col-sm-12">
					<div class="inner-column">
						<div class="image">
							<img src="{{asset('web/images/resource/technology.png')}}" alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Tab-->
	<div class="tab" id="prod-sensor">
		<div class="content">
			<div class="row clearfix">
				<!--Content Column-->
				<div class="content-column col-lg-7 col-md-7 col-sm-12">
					<div class="inner-column">
						<div class="icon-box">
													<span class="icon"><img src="{{asset('web/images/icons/technology-icon.png')}}"
													                        alt=""/></span>
						</div>
						<h3>New Sensor</h3>
						<div class="text">There are many variations of passages of Lorem Ipsume available, but the
							majority. Many museu are transforming thanks to echnology. They are adding interactivity and
							magic to the visitorer experience. Estimote Beacons have helped the Guggenheim in New York to
							be at the forefront of this process. The club uses Estimote Beacons to transform the
							in-stadium experience for its guests.
						</div>
						<a href="#" class="theme-btn btn-style-seven">See More</a>
					</div>
				</div>
				<!--Image Column-->
				<div class="image-column col-lg-5 col-md-5 col-sm-12">
					<div class="inner-column">
						<div class="image">
							<img src="{{asset('web/images/resource/technology.png')}}" alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>