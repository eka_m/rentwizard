<!--Faq Section-->
<section class="overview-section">
	<div class="auto-container">
		<!--Sec Title-->
		<div class="sec-title centered">
			<h2>{{__("Take A Quick Overview")}}</h2>
			<div class="separater"></div>
			{{--<div class="text">--}}
			{{--{{__("Screenshots of the control panel")}}--}}
			{{--</div>--}}
		</div>
		<!--Overview Tabs-->
		<div class="overview-tabs tabs-box">
			<div class="row clearfix">
				<!--Column-->
				<div class="col-lg-3 offset-lg-1 col-md-12 col-sm-12 d-flex justify-content-center">
					<!--Tab Btns-->
					<ul class="tab-btns tab-buttons clearfix">
						<li data-video="/web/videos/stats.webm" class="tab-btn active-btn video-link">@lang("Analytics")</li>
						<li data-video="/web/videos/deals.webm" class="tab-btn video-link">@lang("Deals table")</li>
						<li data-video="/web/videos/inventory.webm" class="tab-btn video-link" >@lang("Inventory table")</li>
						<li data-video="/web/videos/clients.webm" class="tab-btn video-link">@lang("Clients table")</li>
					</ul>
				</div>
				<!--Column-->
				<div class="col-lg-8 col-md-12 col-sm-12 d-flex justify-content-center ">
					<div class="overview-mock">
						<div class="overview-bg">
							<img src="{{asset('web/images/illustrations/imac.png')}}" alt=""/>
						</div>
						<div class="overview-player">
							<video id="mock" width="100%" src="/web/videos/stats.webm" autoplay muted loop>
							</video>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--End Faq Section-->