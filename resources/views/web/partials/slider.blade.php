<!--Main Slider-->
<section class="main-slider" id="home"
         style="background-image:url({{asset('web/images/main-slider/image-2.png')}})">
	<div class="main-slider-carousel owl-carousel owl-theme">

		<div class="slide">
			<div class="auto-container">
				<div class="clearfix">
					<div class="content">
						<div class="title">{{__("One platform for all rental business owners")}}.</div>
						<h2>{!! __("Manage all of you stuff using <br> one app Solution") !!}.</h2>
						<div class="text">{{__("We offer you the best way manage rental deals")}}.</div>
						<div class="link-box">
							<a href="{{route("register")}}" class="theme-btn btn-success btn-style-two">{{__("Start right now")}}!</a>
							<a href="/#features"  class="theme-btn btn-style-six sc-nav">{{__("Learn More")}}</a>
						</div>
					</div>
					<div class="image-box">
						<div class="image">
							<img src="{{asset('web/images/illustrations/main.svg')}}" alt="" title="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--End Main Slider-->