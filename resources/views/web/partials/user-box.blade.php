<div class="order-2 order-md-1">
	@guest
		<button type="button" class="btn btn-danger px-3" data-toggle="modal" data-target="#modalLogin">
			<span class="d-none d-sm-inline">{{__("Login / Register")}}</span>
			<i class="fa flaticon-user d-inline d-sm-none"></i>
		</button>
	@else
		<div id="userDropDown" class="dropdown">
			<button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
			        aria-expanded="false">
				<span class="d-none d-sm-inline">{{ Auth::user()->name }}</span>
				<span
					class="d-inline d-sm-none">{!!   Avatar::create(Auth::user()->name)->setDimension(22)->setBackground("transparent")->setFontSize(18)->toSvg() !!} </span>
			</button>
			<div class="dropdown-menu w-100">
				<h6 class="dropdown-header">{{__('Accounts')}}</h6>
				@foreach(Auth::user()->websites as $website)
					<a class="dropdown-item" href="{{route("website.switch", $website->pin)}}">
						<span class="text-danger">(PIN: {{$website->prettypin}})</span>
					</a>
				@endforeach
				@if(Auth::user()->id == 1)
					<a class="dropdown-item" href="{{route('system.dashboard')}}">
						<span class="text-danger">Панель</span>
					</a>
					@endif
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="{{ route('logout') }}"
				   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					{{ __('Logout') }}
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</div>
		</div>
	@endguest
</div>
