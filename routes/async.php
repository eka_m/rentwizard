<?php
/*
** ASYNC ROUTES ______________________________________________________________________________________________________________________
*/

/* INVENTORY ____________________________________________________________________________________________________________________*/
Route::post( '/inventory', 'InventoryController@getInventory' )->name( 'inventory.all' );
Route::post( '/inventory/search', 'InventoryController@asyncSearch' )->name( 'inventory.search' );
Route::post( '/inventory/chekForActiveDeals', 'InventoryController@chekForActiveDeals' )->name( 'inventory.isactivedeal' );
/* END INVENTORY ________________________________________________________________________________________________________________ */

/* CLIENTS _______________________________________________________________________________________________________________________*/
Route::post( '/clients', 'ClientsController@getClients' )->name( 'clients.all' );
Route::get( '/clients/search/{field}', 'ClientsController@asyncSearch' )->name( 'clients.search' );
Route::get( '/clients/{id}{year}/statistics', 'ClientsController@clientDealsStatistics' )->name( 'clients.statistics' );
Route::get( '/clients/{id}/deals', 'ClientsController@getClientDeals' )->name( 'clients.deals' );
/* END CLIENTS ____________________________________________________________________________________________________________________*/

/* DEALS  _________________________________________________________________________________________________________________________*/
Route::post( '/deals', 'DealsController@getDeals' )->name( 'deals.all' );
Route::get( '/deals/attach/{id}', 'DealsController@attachInventory' )->name( 'deals.attach.item' );
Route::get( '/deals/detach/{id}', 'DealsController@detachInventory' )->name( 'deals.detach.item' );
Route::put( '/deals/close/{id}', 'DealsController@closeDeal' )->name( 'deals.close' );
/* END DEALS  _______________________________________________________________________________________________________________________*/

/* STATISTICS  _______________________________________________________________________________________________________________________*/
Route::post( '/statistics/calendar', 'StatisticsController@calendar' )->name( 'statistics.calendar' );
Route::get( '/statistics/year/{year}/{type?}/{manager?}', 'StatisticsController@profit' )->name( 'statistics.year' );

Route::get( '/inventory/stats/{id}/{year}', 'InventoryController@getProfitStatistics' );
/* END STATISTICS  ___________________________________________________________________________________________________________________*/

/* UPLOADS ___________________________________________________________________________________________________________________________ */
Route::post( '/upload/{input?}/{folder?}/{disk?}', 'FilesController@upload' )->name( 'upload' );
Route::post( '/uploadtemp/{input?}/{collection?}/{temporary?}', 'FilesController@uploadTMP' )->name( 'upload.tmp' );
/* END UPLOADS _______________________________________________________________________________________________________________________*/

/* CATEGORIES ____________________________________________________________________________________________________________________________________*/
Route::resource( '/categories', 'CategoriesController' );
/* END CATEGORIES ________________________________________________________________________________________________________________________________¬*/

/* ROLES AND PERMISSIONS  _________________________________________________________________________________________________________________________*/
Route::put( '/attachPermissionToRole/{role}', 'RolesAndPermissionsController@attachPermissionToRole' )->name( 'permission.to.role' );
/* END ROLES AND PERMISSIONS _______________________________________________________________________________________________________________________*/

/* USERS  _________________________________________________________________________________________________________________________*/
Route::post( '/user/avatar', 'UsersController@avatar' )->name( 'user.avatar' );
Route::delete( '/user/avatar', 'UsersController@removeAvatar' )->name( 'user.avatar.remove' );
/* END USERS  _________________________________________________________________________________________________________________________*/


/* DASHBOARD  _________________________________________________________________________________________________________________________*/
Route::get( '/recentCustomers', "DashboardController@recentCustomers" )->name( "recentCustomers" );
Route::get( '/todaysDeals', "DashboardController@todaysDeals" )->name( "todaysDeals" );
/* END DASHBOARD  _________________________________________________________________________________________________________________________*/


/* NOTIFICATIONS ___________________________________________________________________________________________________________________________________ */
Route::put( '/notifications/markAsRead', 'NotificationsController@markAsRead' )->name( 'notifications.markAsRead' );
/* END NOTIFICATIONS ___________________________________________________________________________________________________________________________________ */
