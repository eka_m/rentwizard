<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
Broadcast::channel( 'online', function ( $user ) {
		return [
			'user' => $user,
			"tenant" => website()
		];
} );

Broadcast::channel( 'inventory.{id}.{tenant}', function ( $user, $id, $tenant ) {
	return (int)$user->id === (int)$id && website() && (int)website()->id === (int)$tenant;
} );
Broadcast::channel( 'app.notification.{id}.{tenant}', function ( $user, $id, $tenant ) {
	return (int)$user->id === (int)$id && website() && (int)website()->id === (int)$tenant;
} );

