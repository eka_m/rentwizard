<?php
Route::get( '/dashboard', 'DashboardController@show' )->name( 'dashboard' );
Route::put( '/dashboard/online', "DashboardController@online" )->name( "users.online" );
Route::get( '/', 'StatisticsController@show' );
Route::get( '/home', 'StatisticsController@show' );
Route::get( '/inventory/stat/{id}', 'InventoryController@inventoryStat' );
Route::get('/export/{model}', 'ExportsController@export')->name('export');


/* SETTINGS ___________________________________________________________________________________________*/
Route::get( '/settings', 'SettingsController@index' )->name( 'settings' );
Route::get( '/settings/main', 'SettingsController@main' )->name( 'settings.main' );
Route::put( '/settings/main', 'SettingsController@save' )->name( 'settings.save' );
Route::get( '/logs', 'SettingsController@logs' )->name( 'logs' );
Route::delete( '/log/clear/{name}', 'SettingsController@clearLog' )->name( 'log.clear' );
/* END SETTINGS ___________________________________________________________________________________________*/


Route::group( [ 'middleware' => [ 'route.log' ] ], function () {

	/*INVENTORY*/
	Route::resource( '/inventory', 'InventoryController' );

	/*END INVENTORY*/

	/*CLIENTS*/
	Route::resource( '/clients', 'ClientsController' );
	/*END CLIENTS*/

	/*DEALS*/
	Route::resource( '/deals', 'DealsController' );
	/*END DEALS*/

	/*STATISTICS*/
	Route::get( '/statistics', 'StatisticsController@show' )->name( 'statistics' );
	Route::get( '/statistics/calendar', 'StatisticsController@showCalendar' )->name( 'statistics.calendar' );
	Route::get( '/statistics/manager', 'StatisticsController@showManager' )->name( 'statistics.manager' );
	/*END STATISTICS*/

	/* USERS */
	Route::get( '/user/profile', 'UsersController@profile' )->name( 'user.profile' );
	Route::put( '/user/profile', 'UsersController@saveProfile' )->name( 'user.save.profile' );
	Route::put( '/user/reset/{id}', 'UsersController@resetPassword' )->name( 'user.reset' );
	Route::resource( '/users', 'UsersController' );
	/* END USERS*/

	/* ATTRIBUTES */
	Route::resource( '/attributes', 'AttributesController' );
	/* END ATTRIBUTES */

	/* CATEGORIES */
	Route::resource( '/categories', 'CategoriesController' );
	/* END CATEGORIES */

	/* ROLES AND PERMISSIONS  _________________________________________________________________________________________________________________________*/
//	Route::resource( '/rolespermissions', 'RolesAndPermissionsController' );
	/* END ROLES AND PERMISSIONS _______________________________________________________________________________________________________________________*/
} );

