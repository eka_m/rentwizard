<?php
//Route::get( '/generateSystemPass', function () {
////	$pass = md5(sprintf(
////		'%s.%d',
////		config('app.key'),
////		"system"
////	));
//	$pass = md5(sprintf(
//		'%d.%s.%s.%s',
//		1,
//		"system",
//		"2019-03-22 08:44:46",
//		"rentwizard"
//	));
//	dd($pass);
//} );

Route::get( "/website/switch/{id}", "Web\WebsiteController@switch" )->name( "website.switch" )->middleware( "auth" );
Route::group( [ "domain" => env( "APP_FQDN" ), 'prefix' => LaravelLocalization::setLocale() ], function () {
	Auth::routes( [ "verify" => true ] );
	Route::get( '/', 'Web\HomeController@index' )->name( 'homepage' );
	Route::get( '/home', 'Web\HomeController@index' )->name( 'homepage' );
	Route::get( '/setLocale/{locale}', "Web\HomeController@setLocale" )->name( "setLocale" );
	Route::get( '/system/dashboard', "Web\SystemController@index" )->name( "system.dashboard" );
} );
Route::group( [ "domain" => "{tenant}." . env( "APP_FQDN" ), "middleware" => [ "tenant.host" ] ], function () {
	Route::get( "/login", "Auth\EmployeeLoginController@login" )->name( "tenant.login" );
	Route::post( "/logout", "Auth\EmployeeLoginController@logout" )->name( "tenant.logout" );


	Route::middleware( [ "auth:employee", "localization" ] )->group( function () {
		Route::get( '/showinv', 'InventoryController@showInventory' )->name( 'showinv' );

		Route::middleware( [ 'phptojs', 'shareviewdata' ] )->group( function () {
			require base_path( 'routes/rent.php' );
		} );
		Route::group( [
			'prefix' => 'async',
			'as' => 'async.',
		], function () {
			require base_path( 'routes/async.php' );
		} );
		Route::get( '/media/{path}', '\Hyn\Tenancy\Controllers\MediaController' )
			->where( 'path', '.+' )
			->name( 'tenant.media' );
	} );
} );
