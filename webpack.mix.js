let mix = require( 'laravel-mix' );
const webpack = require( 'webpack' );

mix.webpackConfig( {

	plugins: [
		new webpack.ContextReplacementPlugin(
			/moment[\/\\]locale/,
			// A regular expression matching files that should be included
			/(en-gb|ru|az)\.js/
		)
	]
} );

mix.js('resources/js/scripts/custom.js', 'public/js/custom.js');
mix.js('resources/js/app.js', 'public/js');
mix.sass('resources/sass/app.scss', 'public/css');
mix.js('public/web/js/app.js', 'public/web/js/app.min.js');
mix.js( "resources/js/scripts/system.js", "public/web/js/system.min.js" );
// mix.sass('public/web/css/app.scss', 'public/web/css/app.min.css').options({
// 	processCssUrls: false
// });
mix.webpackConfig( {
	output: {
		publicPath: '/'
	}
} );
